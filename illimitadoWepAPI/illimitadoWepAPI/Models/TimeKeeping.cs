﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class TimeKeeping
    {
        public class Daily
        {
            public string SchedDate { get; set; }
            public string SchedDay { get; set; }
            public string SchedIN { get; set; }
            public string SchedOUT { get; set; }
            public string TotalTimeHour { get; set; }
            public string SchedType { get; set; }
            public string DateFormat { get; set; }
            public string EmpID { get; set; }
            public string ForApproval { get; set; }
            public string SeriesID { get; set; }
            public string Name { get; set; }

        }
        public class GetDailyResult
        {
            public List<Daily> Daily;
            public string myreturn;
        }

        public class DailyParameters
        {
            public string NTID { get; set; }
            public string Start_Date { get; set; }
            public string End_Date { get; set; }
        }


        public class ForBreaks
        {
            public string NTID { get; set; }
            public string ReasonID{ get; set; }
            public string StartOrEnd { get; set; }
        }

        public class UpadateFaceID
        {
            public string NTID { get; set; }
            public string FaceID { get; set; }
  
        }


        public class ListAllActivityByRange
        {
            public string EmpID { get; set; }
            public string StartTIme { get; set; }
            public string StartEnd { get; set; }
            public string ReasonID { get; set; }
            public string ReasonDesc { get; set; }
            public string IsPaid { get; set; }
            public string SchedDate { get; set; }
            public string HourWork { get; set; }
            public string ActivityStatus { get; set; }
        }
        public class ListAllActivityResult
        {
            public List<ListAllActivityByRange> ListAllActivity;
            public string myreturn;
        }


        public class ListAllReasonBreakTypes
        {
            public List<ListAllBreakReasonTypes> ListAllResonType;
            public string myreturn;
        }


        public class ListAllBreakReasonTypes
        {
            public string ReasonID { get; set; }
            public string ReasonDesc { get; set; }

        }



    }
}