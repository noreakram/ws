﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models;
using System.Xml.Serialization;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using illimitadoWepAPI.MyClass;
using static illimitadoWepAPI.Models.UserProfile;
using System.Net.Http.Headers;
using System.Web.Hosting;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using ExpertXls.ExcelLib;

namespace illimitadoWepAPI.Controllers
{
    public class ProfileController : ApiController
    {
        // GET api/<controller>
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/<controller>/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<controller>
        public void Post([FromBody]string value)
        {
        }

        // PUT api/<controller>/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
        }


//        {
//"NTID": "i143",
//"CARDNAME": "123456",
//"BIRTHDAY": "123456",
//"PLACEOFBIRTH": "123456",
//"NATIONALITY": "123456",
//"NATCOUNTRY": "123456",
//"MOBILE": "123456",
//"PERMZIPCODE": "123456",
//"PERMHOUSENUMBER": "123456",
//"PERMBRGY": "123456",
//"PERMUNI": "123456",
//"PERMCITY": "123456",
//"PERMREGION": "123456",
//"PERMCOUNTRY": "123456",
//"PRESZIPCODE": "123456",
//"PRESHOUSENUMBER": "123456",
//"PRESHOUSEADDRESS": "123456",
//"PRESBRGY": "123456",
//"PRESMUNI": "123456",
//"PRESCITY": "123456",
//"PRESCOUNTRY": "123456",
//"EMAILADD": "123456",
//"EMPLOYERNAME": "123456",
//"TIN": "123456",
//"CIVILSTATUS": "123456",
//"MOTHSUFFIX": "123456",
//"MOTHFIRSTNAME": "123456",
//"MOTHLASTNAME": "123456",
//"OTHERDETAILS": "123456"
//}






        [HttpPost]
        [Route("InsertUserProfile")]
        public string UserProfile(UserProfile uProfile)
        {


            string dataDir = HostingEnvironment.MapPath("~/Files/");
            FileStream stream = new FileStream(dataDir + "Basic.xls", FileMode.Open);
            //ExcelWriter writer = new ExcelWriter(stream);


            string filename;
            filename = uProfile.NTID + "_" + DateTime.Now.Month.ToString().PadLeft(2, '0') + DateTime.Now.Day.ToString().PadLeft(2, '0') + DateTime.Now.Year.ToString().Substring(2, 2) + "_CRT-UPDATEMAY2017.xls";
            using (var fileStream = new FileStream(dataDir + filename, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
            }
            stream.Close();

            ExcelWorkbookFormat workbookFormat = ExcelWorkbookFormat.Xls_2003;
            string testDocFile = dataDir + "Basic.xls";
            ExcelWorkbook workbook = new ExcelWorkbook(testDocFile);
            ExcelWorksheet firstWorksheet = workbook.Worksheets[1];
            firstWorksheet["B19"].Text = uProfile.LASTNAME;
            firstWorksheet["C19"].Text = uProfile.FIRSTNAME;
            firstWorksheet["D19"].Text = uProfile.MIDDLENAME;
            firstWorksheet["E19"].Text = uProfile.SUFFIX;
            firstWorksheet["F19"].Text = uProfile.CARDNAME;
            firstWorksheet["G19"].Text = Convert.ToDateTime(uProfile.BIRTHDAY).ToString("MM-dd-yyyy");
            firstWorksheet["H19"].Text = uProfile.PLACEOFBIRTH;
            firstWorksheet["I19"].Text = uProfile.NATIONALITY;
            firstWorksheet["J19"].Text = uProfile.NATCOUNTRY;
            firstWorksheet["K19"].Text = uProfile.GENDER;
            firstWorksheet["L19"].Text = uProfile.MOBILE;
            firstWorksheet["M19"].Text = uProfile.TELPHONE;
            firstWorksheet["N19"].Text = uProfile.PRESHOUSENUMBER;
            firstWorksheet["O19"].Text = uProfile.PRESHOUSEADDRESS;
            firstWorksheet["P19"].Text = uProfile.PRESCOUNTRY;
            firstWorksheet["Q19"].Text = uProfile.PRESREGION;
            firstWorksheet["R19"].Text = uProfile.PRESPROVINCE;
            firstWorksheet["S19"].Text = uProfile.PRESCITY;
            firstWorksheet["T19"].Text = uProfile.PRESZIPCODE;
            firstWorksheet["U19"].Text = uProfile.PERMHOUSENUMBER;
            firstWorksheet["V19"].Text = uProfile.PERMHOUSEADDRESS;
            firstWorksheet["W19"].Text = uProfile.PERMCOUNTRY;
            firstWorksheet["X19"].Text = uProfile.PERMREGION;
            firstWorksheet["Y19"].Text = uProfile.PERMPROVINCE;
            firstWorksheet["Z19"].Text = uProfile.PERMCITY;
            firstWorksheet["AA19"].Text = uProfile.PERMZIPCODE;
            firstWorksheet["AB19"].Text = uProfile.EMAILADD;
            firstWorksheet["AC19"].Text = uProfile.EMPLOYERNAME;
            firstWorksheet["AD19"].Text = uProfile.TIN;
            firstWorksheet["AE19"].Text = uProfile.CIVILSTATUS;
            firstWorksheet["AF19"].Text = uProfile.MOTHLASTNAME;
            firstWorksheet["AG19"].Text = uProfile.MOTHFIRSTNAME;
            firstWorksheet["AH19"].Text = uProfile.MOTHMIDDLENAME;
            firstWorksheet["AI19"].Text = uProfile.MOTHSUFFIX;
            firstWorksheet["AJ19"].Text = uProfile.NUMBEROFPADS;
            firstWorksheet["AK19"].Text = uProfile.OTHERDETAILS;


                     
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = uProfile.NTID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CardNum", mytype = SqlDbType.NVarChar, Value = uProfile.CARDNAME });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = uProfile.BIRTHDAY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@POB", mytype = SqlDbType.NVarChar, Value = uProfile.PLACEOFBIRTH });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Natioanality", mytype = SqlDbType.NVarChar, Value = uProfile.NATIONALITY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = uProfile.NATCOUNTRY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = uProfile.GENDER });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Mobile", mytype = SqlDbType.NVarChar, Value = uProfile.MOBILE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Landline", mytype = SqlDbType.NVarChar, Value = uProfile.TELPHONE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PERMZIPCODE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSENUMBER });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PERMHOUSEADDRESS });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PERMBRGY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PERMUNI });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_City", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCITY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PERMREGION });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Per_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PERMCOUNTRY });


            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Zipcode", mytype = SqlDbType.NVarChar, Value = uProfile.PRESZIPCODE });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_HouseNum", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSENUMBER });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_St_VIllage_Sub", mytype = SqlDbType.NVarChar, Value = uProfile.PRESHOUSEADDRESS });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Brgy", mytype = SqlDbType.NVarChar, Value = uProfile.PRESBRGY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Muni", mytype = SqlDbType.NVarChar, Value = uProfile.PRESMUNI });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_City", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCITY });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Region", mytype = SqlDbType.NVarChar, Value = uProfile.PRESREGION });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pre_Country", mytype = SqlDbType.NVarChar, Value = uProfile.PRESCOUNTRY });


            Connection.myparameters.Add(new myParameters { ParameterName = "@email", mytype = SqlDbType.NVarChar, Value = uProfile.EMAILADD });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmployerName", mytype = SqlDbType.NVarChar, Value = uProfile.EMPLOYERNAME });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = uProfile.TIN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CivilStatus", mytype = SqlDbType.NVarChar, Value = uProfile.CIVILSTATUS });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMaidenLName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHSUFFIX });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MothersFname", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHFIRSTNAME });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MothersMName", mytype = SqlDbType.NVarChar, Value = uProfile.MOTHLASTNAME });
            Connection.myparameters.Add(new myParameters { ParameterName = "@OtherDetails", mytype = SqlDbType.NVarChar, Value = uProfile.OTHERDETAILS });

            // DataTable EmployeeDt = new DataTable();
            String ID = Connection.ExecuteScalar("sp_Insert_EmployeeProfile_FromMobile");

            System.Web.HttpResponse httpResponse = System.Web.HttpContext.Current.Response;

            httpResponse.Clear();
            httpResponse.ContentType = "Application/x-msexcel";
            httpResponse.AddHeader("Content-Disposition", String.Format("attachment; filename={0}", filename));
            string newDIR = HostingEnvironment.MapPath("~/sFTP/");
            workbook.Save(newDIR + filename);
            return "Success";

            //return ID;

        }

        [Route("GetImages")]
        public  String Base64Image(UserProfile_Images ProfileImages)
       // public HttpResponseMessage DisplaySignature(string id)
        {
            string file_path;
            //if (id != "")
            //{
                file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\" + ProfileImages.EmpID + "\\", ProfileImages.EmpID + "_" + ProfileImages.ImageType + ".png");
            //}
            //else
            //{
            //    file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\Signature\\", "signaturenotfound.png");
            //}
            MemoryStream ms = new MemoryStream(File.ReadAllBytes(file_path));
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            byte[] imageBytes = ms.ToArray();
            string base64String = Convert.ToBase64String(imageBytes);

            //response.Content = new StreamContent(ms);
            //response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
            //return response;

            


            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = ProfileImages.EmpID});
            Connection.myparameters.Add(new myParameters { ParameterName = "@ImgType", mytype = SqlDbType.NVarChar, Value = ProfileImages.ImageType});
            Connection.myparameters.Add(new myParameters { ParameterName = "@Base", mytype = SqlDbType.NVarChar, Value = base64String });


            String ID = Connection.ExecuteScalar("sp_Insert_Base64_Mobile");

            //return ID;

            return base64String;

        }

        [HttpPost]
        [Route("Save_OR_reimbursement")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String InsertOR_ReImbursement(OR_Reimbursement ProfileImages)
        // public HttpResponseMessage DisplaySignature(string id)
        {
            string file_path;
            //if (id != "")
            //{
            file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\" + ProfileImages.EmpID + "\\", ProfileImages.EmpID + "_" + "OR.png");
            //}
            //else
            //{
            //    file_path = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath + "ProfilePhotos\\Signature\\", "signaturenotfound.png");
            //}
            MemoryStream ms = new MemoryStream(File.ReadAllBytes(file_path));
            HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.OK);
            byte[] imageBytes = ms.ToArray();
            string base64String = Convert.ToBase64String(imageBytes);

            //response.Content = new StreamContent(ms);
            //response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue("image/png");
            //return response;




            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = ProfileImages.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BenefitType", mytype = SqlDbType.NVarChar, Value = ProfileImages.BenefitType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ORDateTime", mytype = SqlDbType.NVarChar, Value = ProfileImages.ORDateTIme });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ORAmount", mytype = SqlDbType.NVarChar, Value = ProfileImages.ORAmount });
            Connection.myparameters.Add(new myParameters { ParameterName = "@VendorName", mytype = SqlDbType.NVarChar, Value = ProfileImages.VendorName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@VendorTIN", mytype = SqlDbType.NVarChar, Value = ProfileImages.VendorTin });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Base", mytype = SqlDbType.NVarChar, Value = base64String });
   

            String ID = Connection.ExecuteScalar("sp_Insert_OR_Reimbursement_Mobile");

            //return ID;

            return base64String;

        }






        [HttpPost]
        [Route("UpdateEmployeePersonalInfo")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String UpdateEmployeeMaster(UpdateEmployeePersonalInfo EmployeeMaster)

        {
  


            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@HouseNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HouseNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@StreetName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.StreetName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Barangay", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Barangay });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Town", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Town });
            Connection.myparameters.Add(new myParameters { ParameterName = "@City", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.City });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Region });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ZipCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BloodType", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BloodType });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Gender", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Gender });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DOB", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DOB });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Citizenship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Citizenship });
            Connection.myparameters.Add(new myParameters { ParameterName = "@NameofOrganization", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.NameofOrganization });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MobileAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileAreaCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MobileNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.MobileNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@HomeAreaCode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeAreaCode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@HomeNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.HomeNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PersonalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PersonalEmail });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgName", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgName });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgNumber", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgNumber });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgRelationship", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgRelationship });
            Connection.myparameters.Add(new myParameters { ParameterName = "@OptionalEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.OptionalEmail });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ContactNo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ContactNo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Province", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Province });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgFname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgFname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgMname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgMname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmrgLname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmrgLname });




            String ID = Connection.ExecuteScalar("sp_UpdatePersonalInfo_Mobile");

            return ID;



        }

        [HttpPost]
        [Route("UpdateEmployeeInfo")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String UpdateEmployeeInfo(UpdateEmployeeInfo EmployeeMaster)

        {



            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@First_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.First_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Middle_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Middle_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Last_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Last_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Ntlogin", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Ntlogin });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Alias", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Alias });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Deptcode", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Deptcode });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Deptname", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Deptname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BUnit", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BUnit });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BUnithead", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BUnithead });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Workstation", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Workstation });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Supervisor", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Supervisor });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DTransFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DTransFrom });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DTransTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DTransTo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LocFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LocFrom });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LocTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.LocTo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CCChangeFrom", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CCChangeFrom });
            Connection.myparameters.Add(new myParameters { ParameterName = "@CCChangeTo", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.CCChangeTo });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Email", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Email });
            Connection.myparameters.Add(new myParameters { ParameterName = "@WorkEmail", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.WorkEmail });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Extension", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Extension });
            Connection.myparameters.Add(new myParameters { ParameterName = "@JoinDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JoinDate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ProdStart", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ProdStart });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Tenure", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Tenure });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TenureMnth", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TenureMnth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Batch", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Batch });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Class", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Class });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Skill", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Skill });
            Connection.myparameters.Add(new myParameters { ParameterName = "@JobDesc", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.JobDesc });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Promotion", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Promotion });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Status });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Emp_Name", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Emp_Name });
            Connection.myparameters.Add(new myParameters { ParameterName = "@approved", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.approved });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Employee_Level", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Employee_Level });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Employee_Category", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Employee_Category });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BusinessSegment", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BusinessSegment });
            Connection.myparameters.Add(new myParameters { ParameterName = "@IsApprove", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.IsApprove });
            Connection.myparameters.Add(new myParameters { ParameterName = "@ResignedDate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.ResignedDate });




            String ID = Connection.ExecuteScalar("sp_UpdateEmployeeInfo_Mobile");

            return ID;



        }


        [HttpPost]
        [Route("UpdateEmpConfidentialInfo")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String UpdateEmpConfidentialInfo(UpdateEmployeeConfiInfo EmployeeMaster)

        {



            Connection Connection = new Connection();
            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TaxStatus", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TaxStatus });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SSS", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.SSS });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TIN", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TIN });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PhilHealth", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PhilHealth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Pagibig", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Pagibig });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DriversLicense", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DriversLicense });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Passport", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Passport });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Unified", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Unified });
            Connection.myparameters.Add(new myParameters { ParameterName = "@Postal", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.Postal });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BirthCertificate", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BirthCertificate });
            Connection.myparameters.Add(new myParameters { ParameterName = "@SssImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.SssImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@TinImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.TinImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PhilImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PhilImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PagImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PagImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@DriverImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.DriverImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PassportImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PassportImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@UmidImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.UmidImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@PostalImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.PostalImage });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BirthImage", mytype = SqlDbType.NVarChar, Value = EmployeeMaster.BirthImage });





            String ID = Connection.ExecuteScalar("sp_UpdateEmployeeConfidentialInfo_Mobile");

            return ID;



        }




        ///

        //{
        //"LOGINID": "i143"
        //}
        [HttpPost]
        [Route("DisplayCompleteProfile")]
        public Master_User_Info_Personal_Confi_Info DisplayCompleteProfile(Login userToSave)
        {
            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@empId", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
         
            DataRow dr;

            dr = Connection.GetSingleRow("sp_getCompleteProfile_Mobile");

            Master_User_Info_Personal_Confi_Info userprofile = new Master_User_Info_Personal_Confi_Info
            {
                SeriesID = dr["SeriesID"].ToString(),
                EmpID = dr["EmpID"].ToString(),
                First_Name = dr["First_Name"].ToString(),
                Middle_Name = dr["Middle_Name"].ToString(),
                Last_Name = dr["Last_Name"].ToString(),
                NTID = dr["NTID"].ToString(),
                EmpName = dr["EmpName"].ToString(),
                EmpType = dr["EmpType"].ToString(),
                EmpLevel = dr["EmpLevel"].ToString(),
                EmpStatus = dr["EmpStatus"].ToString(),
                UserType = dr["UserType"].ToString(),
                Gender = dr["Gender"].ToString(),
                DateJoined = dr["DateJoined"].ToString(),
                ContractEndDate = dr["ContractEndDate"].ToString(),
                LeaveBal = dr["LeaveBal"].ToString(),
                LeaveBalCTO = dr["LeaveBalCTO"].ToString(),
                Country = dr["Country"].ToString(),
                JobDesc = dr["JobDesc"].ToString(),
                DeptCode = dr["DeptCode"].ToString(),
                DeptName = dr["DeptName"].ToString(),
                MngrID = dr["MngrID"].ToString(),
                MngrName = dr["MngrName"].ToString(),
                MngrNTID = dr["MngrNTID"].ToString(),
                BusinessUnit = dr["BusinessUnit"].ToString(),
                BusinessUnitHead = dr["BusinessUnitHead"].ToString(),
                BusinessSegment = dr["BusinessSegment"].ToString(),
                AccessLevel = dr["AccessLevel"].ToString(),
                DOB = dr["DOB"].ToString(),
                Alias = dr["Alias"].ToString(),
                ExtensionNumber = dr["ExtensionNumber"].ToString(),
                Skill = dr["Skill"].ToString(),
                Batch = dr["Batch"].ToString(),
                Class = dr["Class"].ToString(),
                Workstation = dr["Workstation"].ToString(),
                ProductionStartDate = dr["ProductionStartDate"].ToString(),
                ModifiedBy = dr["ModifiedBy"].ToString(),
                DateModified = dr["DateModified"].ToString(),
                EmpPayHrsCat = dr["EmpPayHrsCat"].ToString(),
                Salary = dr["Salary"].ToString(),
                DailyRate = dr["DailyRate"].ToString(),
                EcolaWageOrder = dr["EcolaWageOrder"].ToString(),
                EcolaRate = dr["EcolaRate"].ToString(),
                Transportation = dr["Transportation"].ToString(),
                Rice = dr["Rice"].ToString(),
                Communication = dr["Communication"].ToString(),
                EcolaRegion = dr["EcolaRegion"].ToString(),
                Branch = dr["Branch"].ToString(),
                Wallet = dr["Wallet"].ToString(),
                FaceID = dr["FaceID"].ToString(),
                DTransFrom = dr["DTransFrom"].ToString(),
                DTransTo = dr["DTransTo"].ToString(),
                LocFrom = dr["LocFrom"].ToString(),
                LocTo = dr["LocTo"].ToString(),
                CCChangeFrom = dr["CCChangeFrom"].ToString(),
                CCChangeTo = dr["CCChangeTo"].ToString(),
                Email = dr["Email"].ToString(),
                WorkEmail = dr["WorkEmail"].ToString(),
                Extension = dr["Extension"].ToString(),
                JoinDate = dr["JoinDate"].ToString(),
                ProdStart = dr["ProdStart"].ToString(),
                Tenure = dr["Tenure"].ToString(),
                TenureMnth = dr["TenureMnth"].ToString(),
                Batch1 = dr["Batch"].ToString(),
                Class1 = dr["Class"].ToString(),
                Skill1 = dr["Skill"].ToString(),
                JobDesc1 = dr["JobDesc"].ToString(),
                Promotion = dr["Promotion"].ToString(),
                Status = dr["Status"].ToString(),
                Emp_Name = dr["Emp_Name"].ToString(),
                approved = dr["approved"].ToString(),
                Employee_Level = dr["Employee_Level"].ToString(),
                Employee_Category = dr["Employee_Category"].ToString(),
                BusinessSegment1 = dr["BusinessSegment"].ToString(),
                IsApprove = dr["IsApprove"].ToString(),
                ResignedDate = dr["ResignedDate"].ToString(),
                HouseNumber = dr["HouseNumber"].ToString(),
                StreetName = dr["StreetName"].ToString(),
                Barangay = dr["Barangay"].ToString(),
                Town = dr["Town"].ToString(),
                City = dr["City"].ToString(),
                Region = dr["Region"].ToString(),
                ZipCode = dr["ZipCode"].ToString(),
                BloodType = dr["BloodType"].ToString(),
                Gender1 = dr["Gender"].ToString(),
                DOB1 = dr["DOB"].ToString(),
                Citizenship = dr["Citizenship"].ToString(),
                NameofOrganization = dr["NameofOrganization"].ToString(),
                MobileAreaCode = dr["MobileAreaCode"].ToString(),
                MobileNumber = dr["MobileNumber"].ToString(),
                HomeAreaCode = dr["HomeAreaCode"].ToString(),
                HomeNumber = dr["HomeNumber"].ToString(),
                PersonalEmail = dr["PersonalEmail"].ToString(),
                EmrgName = dr["EmrgName"].ToString(),
                EmrgNumber = dr["EmrgNumber"].ToString(),
                EmrgRelationship = dr["EmrgRelationship"].ToString(),
                OptionalEmail = dr["OptionalEmail"].ToString(),
                ContactNo = dr["ContactNo"].ToString(),
                Province = dr["Province"].ToString(),
                TaxStatus = dr["TaxStatus"].ToString(),
                SSS = dr["SSS"].ToString(),
                TIN = dr["TIN"].ToString(),
                PhilHealth = dr["PhilHealth"].ToString(),
                Pagibig = dr["Pagibig"].ToString(),
                DriversLicense = dr["DriversLicense"].ToString(),
                Passport = dr["Passport"].ToString(),
                Unified = dr["Unified"].ToString(),
                Postal = dr["Postal"].ToString(),
                BirthCertificate = dr["BirthCertificate"].ToString(),

 

            };
            return userprofile;

        }



        //public EmployerHistory DisplayEmploymentHistory(Login userToSave)
        //{
        //    Connection Connection = new Connection();

        //    Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

        //    DataRow dr;

        //    dr = Connection.GetSingleRow("sp_ShowEmployerHistory_Mobile");

        //    EmployerHistory EmployerHis = new EmployerHistory
        //    {
        //        Company_Name = dr["CompanyName"].ToString(),
        //        Work_From = dr["joinedFrom"].ToString(),
        //        Work_To = dr["joinedTo"].ToString()
        //    };
        //    return EmployerHis;

        //}

        //{     
        //"LoginID": "i004"
        //}
        [HttpPost]
        [Route("DisplayEmployerHistory")]
        public EmployerList SDisplayEmploymentHistory(Login userToSave)
        {
            EmployerList GetEmployerLists = new EmployerList();
            try
            {
                Connection Connection = new Connection();
                List<EmployerHistory> ListEmployers = new List<EmployerHistory>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });

                ds = Connection.GetDataset("sp_ShowEmployerHistory_Mobile");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        EmployerHistory AvailableLeaves = new EmployerHistory
                        {


                            Company_Name = row["CompanyName"].ToString(),
                            Work_From = row["joinedFrom"].ToString(),
                            Work_To = row["joinedTo"].ToString()


                        };
                        ListEmployers.Add(AvailableLeaves);
                    }
                    GetEmployerLists.EmployerListing = ListEmployers;
                    GetEmployerLists.myreturn = "Success";
                }
                else
                {
                    GetEmployerLists.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetEmployerLists.myreturn = ex.Message;
            }
            return GetEmployerLists;
        }





        [HttpPost]
        [Route("InsertEmployeeDependents")]
        //{
        // "EmpID": "jeya",
        // "BenefitType": "BenefitType",
        //  "ORDateTIme": "BenefitType",
        // "ORAmount": "BenefitType",
        // "VendorName": "BenefitType",
        //  "VendorTin": "BenefitType"
        //}
        public String InsertEmployeeDependents(DependentHistory DependentDetails)

        {



            Connection Connection = new Connection();

            Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = DependentDetails.EmpID });
            Connection.myparameters.Add(new myParameters { ParameterName = "@RELATION", mytype = SqlDbType.NVarChar, Value = DependentDetails.Relationship });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FIRST", mytype = SqlDbType.NVarChar, Value = DependentDetails.First });
            Connection.myparameters.Add(new myParameters { ParameterName = "@MID", mytype = SqlDbType.NVarChar, Value = DependentDetails.Mid });
            Connection.myparameters.Add(new myParameters { ParameterName = "@LAST", mytype = SqlDbType.NVarChar, Value = DependentDetails.Lastname });
            Connection.myparameters.Add(new myParameters { ParameterName = "@BIRTH", mytype = SqlDbType.NVarChar, Value = DependentDetails.Birth });
            Connection.myparameters.Add(new myParameters { ParameterName = "@COMMENT", mytype = SqlDbType.NVarChar, Value = DependentDetails.Comments });
            Connection.myparameters.Add(new myParameters { ParameterName = "@FILE", mytype = SqlDbType.NVarChar, Value = DependentDetails.DocFilename });
            Connection.myparameters.Add(new myParameters { ParameterName = "@EXTENSION", mytype = SqlDbType.NVarChar, Value = DependentDetails.Extension });

            String ID = Connection.ExecuteScalar("sp_InsertDependentV2");

            return ID;



        }














        //{     
        //"LoginID": "i004"
        //}
        [HttpPost]
        [Route("DisplayDependents")]
        public DependentList DisplayDependents(Login userToSave)
        {
            DependentList GetDependentLists = new DependentList();
            try
            {
                Connection Connection = new Connection();
                List<DependentHistory> ListDependents = new List<DependentHistory>();
                DataSet ds = new DataSet();
                System.Data.DataTable dtLEAVES = new System.Data.DataTable();

                Connection.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = userToSave.LoginID });
               
                ds = Connection.GetDataset("sp_ShowDependentList");
                dtLEAVES = ds.Tables[0];
                if (dtLEAVES.Rows.Count > 0)
                {
                    foreach (DataRow row in dtLEAVES.Rows)
                    {
                        DependentHistory AvailableLeaves = new DependentHistory
                        {

                            FullName = row["FullName"].ToString(),
                            DOB = row["DateOfBirth"].ToString(),
                            Relationship = row["Relation"].ToString()


                        };
                        ListDependents.Add(AvailableLeaves);
                    }
                    GetDependentLists.DependentListing = ListDependents;
                    GetDependentLists.myreturn = "Success";
                }
                else
                {
                    GetDependentLists.myreturn = "No Data Available";
                }
            }
            catch (Exception ex)
            {
                GetDependentLists.myreturn = ex.Message;
            }
            return GetDependentLists;
        }


    }
}