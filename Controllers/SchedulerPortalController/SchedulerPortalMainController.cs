﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using illimitadoWepAPI.Models.SchedulerPortalModels;
using illimitadoWepAPI.SchedulerPortalClass;
using System.Data;
using System.Security.Claims;
using MimeKit;

namespace illimitadoWepAPI.Controllers.SchedulerPortalController
{
   
    public class SchedulerPortalMainController : ApiController
    {
        [HttpPost]
        [Route("test_getaccinfo")]
        public test_accinfo test_getaccinfo()
        {
            try
            {
                Connection con = new Connection();
                DataTable DT2 = con.GetDataTable("sp_test_getaccinfo");
                test_accinfo t_ai = new test_accinfo();
                List<test_accinfolist> t_ail = new List<test_accinfolist>();
                foreach (DataRow row in DT2.Rows)
                {
                    test_accinfolist _accinfolist = new test_accinfolist()
                    {
                        _sid = row["SID"].ToString(),
                        _sfn = row["FirstName"].ToString(),
                        _smi = row["MI"].ToString(),
                        _sln = row["LastName"].ToString()
                    };
                    t_ail.Add(_accinfolist);
                }
                t_ai.test_accinfolist = t_ail;
                t_ai._myret = "Success";
                return t_ai;
            }
            catch (Exception e)
            {
                test_accinfo t_ai = new test_accinfo();
                List<test_accinfolist> t_ail = new List<test_accinfolist>();
                t_ai.test_accinfolist = t_ail;
                t_ai._myret = e.ToString();
                return t_ai;
            }
        }
        [HttpPost]
        [Route("test_getsched")]
        public test_sched test_getsched(test_getsched tgs)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@sid", mytype = SqlDbType.VarChar, Value = tgs._sid });
                con.myparameters.Add(new myParameters { ParameterName = "@schedstart", mytype = SqlDbType.VarChar, Value = tgs._startdate });
                con.myparameters.Add(new myParameters { ParameterName = "@schedend", mytype = SqlDbType.VarChar, Value = tgs._enddate });
                con.myparameters.Add(new myParameters { ParameterName = "@format", mytype = SqlDbType.VarChar, Value = tgs._format });
                DataTable DT2 = con.GetDataTable("sp_test_getsched");
                test_sched t_s = new test_sched();
                List<test_schedlist> t_sl = new List<test_schedlist>();
                foreach (DataRow row in DT2.Rows)
                {
                    test_schedlist _schedlist = new test_schedlist()
                    {
                        _sdate = row["Date"].ToString(),
                        _day = row["Day"].ToString(),
                        _stimein = row["TimeIn"].ToString(),
                        _stimeout = row["TimeOut"].ToString()
                    };
                    t_sl.Add(_schedlist);
                }
                t_s.test_schedlist = t_sl;
                t_s._myret = "Success";
                return t_s;
            }
            catch (Exception e)
            {
                test_sched t_s = new test_sched();
                List<test_schedlist> t_sl = new List<test_schedlist>();
                t_s.test_schedlist = t_sl;
                t_s._myret = e.ToString();
                return t_s;
            }
        }
        [HttpPost]
        [Route("test_getann")]
        public test_ann test_getann()
        {
            try
            {
                Connection con = new Connection();
                DataTable DT2 = con.GetDataTable("sp_test_getann");
                test_ann t_a = new test_ann();
                List<test_annlist> t_al = new List<test_annlist>();
                foreach (DataRow row in DT2.Rows)
                {
                    test_annlist _annlist = new test_annlist()
                    {
                        _annid = row["ID"].ToString(),
                        _anntitle = row["Title"].ToString(),
                        _annmess = row["Message"].ToString(),
                        _anndatetime = row["DateTimeCreated"].ToString(),
                        _anncreatedby = row["CreatedBy"].ToString()
                    };
                    t_al.Add(_annlist);
                }
                t_a.test_annlist = t_al;
                t_a._myret = "Success";
                return t_a;
            }
            catch (Exception e)
            {
                test_ann t_a = new test_ann();
                List<test_annlist> t_al = new List<test_annlist>();
                t_a.test_annlist = t_al;
                t_a._myret = e.ToString();
                return t_a;
            }
        }
        [HttpPost]
        [Route("test_setann")]
        public string test_setann(test_annlist t_al)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@title", mytype = SqlDbType.VarChar, Value = t_al._anntitle });
                con.myparameters.Add(new myParameters { ParameterName = "@message", mytype = SqlDbType.VarChar, Value = t_al._annmess });
                con.myparameters.Add(new myParameters { ParameterName = "@accid", mytype = SqlDbType.VarChar, Value = t_al._anncreatedby });
                con.ExecuteNonQuery("sp_test_setann");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("test_tito")]
        public string test_tito(test_tito tito)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@sid", mytype = SqlDbType.VarChar, Value = tito._sid });
                con.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.VarChar, Value = tito._status });
                con.ExecuteNonQuery("sp_test_tito");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("SchedulerPortalLogin")] //Para sa login
        public string schedulerLogin(schedulerLogin schedulerLogin)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = schedulerLogin._username });
                con.myparameters.Add(new myParameters { ParameterName = "@USERPASS", mytype = SqlDbType.NVarChar, Value = schedulerLogin._password });
                return con.ExecuteScalar("sp_SP_Login");
            }
            catch (Exception e)
            {
                return "Failed";
            }
        }

        [HttpPost]
        [Route("SchedulerPortalEmpInformation")] //Para sa viewing sa Profile
        public employeeInfo employeeInfo(employeeInfo employeeInfo)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = employeeInfo._UID });
            employeeInfo empInfo = new employeeInfo();
            List<employeeInfoList> list = new List<employeeInfoList>();
            //DataSet ds = new DataSet();
            DataTable dt = con.GetDataTable("sp_SP_EmployeeInfo");

            foreach (DataRow row in dt.Rows)
            {
                employeeInfoList param = new employeeInfoList()
                {
                    _firstName = row["First_Name"].ToString(),
                    _middleName = row["Middle_Name"].ToString(),
                    _lastName = row["Last_Name"].ToString(),
                    _title = row["Title"].ToString(),
                    _role = row["Role"].ToString(),
                    _area = row["Area"].ToString(),
                    _branch = row["Branch"].ToString(),
                    _department = row["Department"].ToString(),
                    _subdepartment = row["SubDepartment"].ToString(),
                    _userType = row["UserType"].ToString(),
                    _email = row["Email"].ToString(),
                    _phoneNumber = row["Phone_Number"].ToString(),
                    _autoapprove = Convert.ToBoolean(row["Auto_Approve"].ToString())
                };
                list.Add(param);
            }
            empInfo.employeeInfoList = list;
            empInfo._myreturn = "Success";
            return empInfo;
        }

        [HttpPost]
        [Route("SchedulerPortalChangePassword")] //Para sa changepass sa profile
        public string schedulerChangePassword(changePassword changePassword)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = changePassword._UID });
                con.myparameters.Add(new myParameters { ParameterName = "@OLDPASS", mytype = SqlDbType.NVarChar, Value = changePassword._oldPassword });
                con.myparameters.Add(new myParameters { ParameterName = "@NEWPASS", mytype = SqlDbType.NVarChar, Value = changePassword._newPassword });
                return con.ExecuteScalar("sp_SP_ChangePass");
            }
            catch (Exception e)
            {
                return "Failed";
            }
        }
        [HttpPost]
        [Route("SchedulerPortalSubjects")] //Listahan ng Subjects sa Labels
        public subjects schedulerSubjects()
        {
            try
            {
                Connection con = new Connection();
                subjects subjects = new subjects();
                List<subjectlist> list = new List<subjectlist>();
                //DataSet ds = new DataSet();
                DataTable dt = con.GetDataTable("sp_SP_ShowSubjects");

                foreach (DataRow row in dt.Rows)
                {
                    subjectlist param = new subjectlist()
                    {
                        _SID = row["SID"].ToString(),
                        _Subject = row["Subject"].ToString(),
                        _Status = Convert.ToBoolean(row["Status"].ToString()),
                        _DefaultName = row["DefaultName"].ToString(),
                        _ModifiedBy = row["ModifiedBy"].ToString(),
                        _ModifiedDate = row["ModifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subjects.subjectlist = list;
                subjects._myreturn = "Success";
                return subjects;
            }
            catch (Exception e)
            {
                subjects subjects = new subjects();
                List<subjectlist> list = new List<subjectlist>();
                subjects.subjectlist = list;
                subjects._myreturn = e.ToString();
                return subjects;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalSubSubjects")] //Listahan ng SubSubjects sa labels, pasahan ng value 0 yung parameter para magdisplay lahat ng subsubjects else selected subsubjects lang lalabas
        public subsubjects schedulerSubSubjects(subjectlist subject)
        {
            try
            {
                Connection con = new Connection();
                subsubjects subsubjects = new subsubjects();
                List<subsubjectlist> list = new List<subsubjectlist>();
                //DataSet ds = new DataSet();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subject._SID });
                DataTable dt = con.GetDataTable("sp_SP_SelectShowSubSubjects");

                foreach (DataRow row in dt.Rows)
                {
                    subsubjectlist param = new subsubjectlist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _SID = row["SID"].ToString(),
                        _Subject = row["SubSubject"].ToString(),
                        _Status = Convert.ToBoolean(row["Status"].ToString()),
                        _ModifiedBy = row["ModifiedBy"].ToString(),
                        _ModifiedDate = row["ModifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                subsubjects subsubjects = new subsubjects();
                List<subsubjectlist> list = new List<subsubjectlist>();
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }
        [HttpPost]
        [Route("SchedulerPortalUpdateSubject")] // insert and update ng Subject. pag pinasahan ng 0 yung @SUBJECTID magiinsert siya ng panibago else iuupdate niya. yung @DEFAULTNAME original na name nung Subject
        public string UpdateSubject(subjectlist subjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectlist._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTNAME", mytype = SqlDbType.NVarChar, Value = subjectlist._Subject });
                con.myparameters.Add(new myParameters { ParameterName = "@DEFAULTNAME", mytype = SqlDbType.NVarChar, Value = subjectlist._DefaultName });
                con.ExecuteScalar("sp_SP_InsertUpdateSubject");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("SchedulerPortalUpdateSubSubject")] //Insert and Update ng SubSubjects, pasahan ng 0 yung @SUBSUBJECTID para mag insert ng panibagong subsubject else iuupdate niya yung selected subsubject
        public string UpdateSubSubject(subsubjectlist subsubjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._RowID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTNAME", mytype = SqlDbType.NVarChar, Value = subsubjectlist._Subject });
                con.ExecuteScalar("sp_SP_InsertUpdateSubSubjectList");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("SchedulerEmpInfoUpdate")] 
        public string EmpInfoUpdate(createuserv2 EIU)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = EIU._userid });
                con.myparameters.Add(new myParameters { ParameterName = "@PCLIENTID", mytype = SqlDbType.NVarChar, Value = EIU.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = EIU._email });
                con.myparameters.Add(new myParameters { ParameterName = "@PHONE", mytype = SqlDbType.NVarChar, Value = EIU._phone });
                con.ExecuteScalar("sp_SP_UpdateEmailPhone");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("SchedulerPortalSubjectStatus")] //Toggle ng checkbox for Subject = Active/Inactive
        public string SubjectStatus(subjectlist subjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectlist._SID });
                con.ExecuteNonQuery("sp_SP_ActiveInactiveSubject");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("SchedulerPortalSubSubjectStatus")] //Toggle ng checkbox for SubSubject = Active/Inactive
        public string SubSubjectStatus(subsubjectlist subsubjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._RowID });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = subsubjectlist.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = subsubjectlist.SCN });
                con.ExecuteNonQuery("sp_SP_ActiveInactiveSubSubjectListV2");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("SchedulerPortalGridRequestList")] //Items sa table sa gitna sa may To Do. yung apat na columns, pasahan ng status either: "New", "Pending", "Approved"
        public gridrequestlist GridRequestList(gridsearch gr)
        {
            try
            {
                Connection con = new Connection();
                
                con.myparameters.Add(new myParameters { ParameterName = "@STATUS", mytype = SqlDbType.NVarChar, Value = gr._status });
                string BRANCH = "";
                BRANCH = gr._branch.Replace("\"", "");
                BRANCH = BRANCH.Replace("[", "");
                BRANCH = BRANCH.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = BRANCH });
                string DEPARTMENT = "";
                DEPARTMENT = gr._department.Replace("\"", "");
                DEPARTMENT = DEPARTMENT.Replace("[", "");
                DEPARTMENT = DEPARTMENT.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = DEPARTMENT });
                string SUBDEPARTMENT = "";
                SUBDEPARTMENT = gr._subdepartment.Replace("\"", "");
                SUBDEPARTMENT = SUBDEPARTMENT.Replace("[", "");
                SUBDEPARTMENT = SUBDEPARTMENT.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = SUBDEPARTMENT });
                string REQUESTOR = "";
                REQUESTOR = gr._requestor.Replace("\"", "");
                REQUESTOR = REQUESTOR.Replace("[", "");
                REQUESTOR = REQUESTOR.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@REQUESTOR", mytype = SqlDbType.NVarChar, Value = REQUESTOR });
                con.myparameters.Add(new myParameters { ParameterName = "@DATEFROM", mytype = SqlDbType.NVarChar, Value = gr._datefrom });
                con.myparameters.Add(new myParameters { ParameterName = "@DATETO", mytype = SqlDbType.NVarChar, Value = gr._dateto });
                DataTable dt = con.GetDataTable("sp_SP_ViewRequests");
                gridrequestlist gridrequest = new gridrequestlist();
                List<gridrequest> list = new List<gridrequest>();
                foreach (DataRow row in dt.Rows)
                {
                    gridrequest param = new gridrequest()
                    {
                        _requestid = row["SeriesID"].ToString(),
                        _requestor = row["Requestor"].ToString(),
                        _company = row["Company"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString()
                    };
                    list.Add(param);
                }
                gridrequest.gridrequest = list;
                gridrequest._myreturn = "Success";
                return gridrequest;
            }
            catch (Exception e)
            {
                gridrequestlist gridrequest = new gridrequestlist();
                List<gridrequest> list = new List<gridrequest>();
                gridrequest.gridrequest = list;
                gridrequest._myreturn = e.ToString();
                return gridrequest;
            }
        }
        [HttpPost]
        [Route("SchedulerPortalRequestInfoLists")] // Data para sa resibo sa kanan sa To Do. Papasahan ng ID nung sinelect na item sa may table sa gitna ng To Do tsaka ng status either: "New", "Pending", "Approved"
        public requestinfolist RequestInfoList(gridrequest gr)
        {
            try
            {
                Connection con = new Connection();
                requestinfolist requestinfolist = new requestinfolist();
                List<requestinfo> requestinfo = new List<requestinfo>();
                con.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = gr._requestid });
                DataTable dt = con.GetDataTable("sp_SP_SelectRequests");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    requestinfo ri = new requestinfo();
                    ri._daterequested = dt.Rows[i]["RequestDate"].ToString();
                    ri._requestedtime = dt.Rows[i]["RequestTime"].ToString();
                    ri._requestorname = dt.Rows[i]["RequestorName"].ToString();
                    ri._requestorarea = dt.Rows[i]["RequestorArea"].ToString();
                    ri._requestorbranch = dt.Rows[i]["RequestorBranch"].ToString();
                    ri._department = dt.Rows[i]["Department"].ToString();
                    ri._subdepartment = dt.Rows[i]["SubDepartment"].ToString();
                    ri._companyname = dt.Rows[i]["CompanyName"].ToString();
                    ri._industry = dt.Rows[i]["Industry"].ToString();
                    ri._noofemployees = dt.Rows[i]["NoOfEmployees"].ToString();
                    ri._hris = dt.Rows[i]["HRIS"].ToString();
                    ri._unitbldg = dt.Rows[i]["UnitBldg"].ToString();
                    ri._city = dt.Rows[i]["Municipality_City"].ToString();
                    Connection con2 = new Connection();
                    List<contactinfo> contactinfo = new List<contactinfo>();
                    con2.myparameters.Add(new myParameters { ParameterName = "@ID", mytype = SqlDbType.NVarChar, Value = gr._requestid });
                    DataTable dt2 = con2.GetDataTable("sp_SP_SelectRequests2");
                    for (int ii = 0; ii < dt2.Rows.Count; ii++)
                    {
                        contactinfo ci = new contactinfo();
                        ci._contactperson = dt2.Rows[ii]["ContactPerson"].ToString();
                        ci._title = dt2.Rows[ii]["Title"].ToString();
                        ci._phonenumber = dt2.Rows[ii]["ContactNumber"].ToString();
                        ci._email = dt2.Rows[ii]["EmailAddress"].ToString();
                        contactinfo.Add(ci);
                    }
                    ri.contactinfo = contactinfo;
                    requestinfo.Add(ri);
                }
                requestinfolist.requestinfo = requestinfo;
                requestinfolist._myreturn = "Success";
                return requestinfolist;
            }
            catch (Exception e)
            {
                requestinfolist requestinfolist = new requestinfolist();
                List<requestinfo> requestinfo = new List<requestinfo>();
                requestinfolist.requestinfo = requestinfo;
                requestinfolist._myreturn = e.ToString();
                return requestinfolist;
            }
        }
        [HttpPost]
        [Route("SchedulerPortalListSimulator")] 
        // Para sa display ng dropdowns, mga parameter values: 
        // 0 = lahat ng SubSbujects; 
        // 1 = lahat ng subsubject ng Title, 
        // 2 = lahat ng subsubject ng Role, 
        // 3 = lahat ng subsubject ng Area, 
        // 4 = lahat ng subsubject ng Branch, 
        // 5 = lahat ng subsubject ng Department, 
        // 6 = lahat ng subsubject ng SubDepartment
        public subsubjects ListSimulator(subjectlist sl)
        {
            try
            {
                Connection con = new Connection();
                subsubjects subsubjects = new subsubjects();
                List<subsubjectlist> list = new List<subsubjectlist>();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = sl._SID });
                DataTable dt = con.GetDataTable("sp_SP_SubSubjectSimulator");

                foreach (DataRow row in dt.Rows)
                {
                    subsubjectlist param = new subsubjectlist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _Subject = row["SubSubject"].ToString()
                    };
                    list.Add(param);
                }
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                subsubjects subsubjects = new subsubjects();
                List<subsubjectlist> list = new List<subsubjectlist>();
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }
        [HttpPost]
        [Route("SchedulerPortalRequestStatusChange")] 
        //Pang Pend,Approve,Deny ng mga request galing sa Scheduler Mobile App. Pasahan ng status either: "Pending", "Approved", "Denied"
        //@REASON = ginagamit kapag nag Pending tsaka Denied
        public string RequestStatusChange(requestinfo ri)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = ri._modifiedby });
                con.myparameters.Add(new myParameters { ParameterName = "@REQUESTID", mytype = SqlDbType.NVarChar, Value = ri._requestid });
                con.myparameters.Add(new myParameters { ParameterName = "@STATUS", mytype = SqlDbType.NVarChar, Value = ri._status });
                con.myparameters.Add(new myParameters { ParameterName = "@REASON", mytype = SqlDbType.NVarChar, Value = ri._reason });
                con.ExecuteNonQuery("sp_SP_RequestStatusChange");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("SchedulerPortalSubjectLabels")]
        public subjectlabellist SubjectLabelList()
        {
            try
            {
                Connection con = new Connection();
                DataTable dt = con.GetDataTable("sp_SP_LabelSubjects");
                subjectlabellist SubjectLabelList = new subjectlabellist();
                List<subjectlabel> SL = new List<subjectlabel>();
                foreach (DataRow row in dt.Rows)
                {
                    subjectlabel param = new subjectlabel()
                    {
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                    };
                    SL.Add(param);
                }
                SubjectLabelList.subjectlabel = SL;
                SubjectLabelList._myreturn = "Success";
                return SubjectLabelList;
            }
            catch (Exception e)
            {
                subjectlabellist SubjectLabelList = new subjectlabellist();
                List<subjectlabel> SL = new List<subjectlabel>();
                SubjectLabelList.subjectlabel = SL;
                SubjectLabelList._myreturn = e.ToString();
                return SubjectLabelList;
            }
        }
        [HttpPost]
        [Route("SchedulerPortalRequestorList")]
        public requestorlist RequestorList(requestor requestor)
        {
            try
            {
                Connection con = new Connection();
                string USERTYPE = "";
                USERTYPE = requestor._usertype.Replace("\"", "");
                USERTYPE = USERTYPE.Replace("[", "");
                USERTYPE = USERTYPE.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.NVarChar, Value = USERTYPE });
                DataTable dt = con.GetDataTable("sp_SP_RequestorList");
                requestorlist RL = new requestorlist();
                List<requestor> R = new List<requestor>();
                foreach (DataRow row in dt.Rows)
                {
                    requestor req = new requestor()
                    {
                        _userid = row["UserID"].ToString(),
                        _requestorname = row["Requestor"].ToString()
                    };
                    R.Add(req);
                }
                RL.requestor = R;
                RL._myreturn = "Success";
                return RL;
            }
            catch (Exception e)
            {
                requestorlist RL = new requestorlist();
                List<requestor> R = new List<requestor>();
                RL.requestor = R;
                RL._myreturn = e.ToString();
                return RL;
            }
        }
        [HttpPost]
        [Route("SchedulerPortalSearchEmployee")] //Para sa viewing sa User Mgt pag nag search
        public employeeInfo SearchEmployee(employeeInfoList employeeInfo)
        {
            Connection con = new Connection();
            string AREA = "";
            AREA = employeeInfo._area.Replace("\"", "");
            AREA = AREA.Replace("[", "");
            AREA = AREA.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@AREA", mytype = SqlDbType.NVarChar, Value = AREA });
            string BRANCH = "";
            BRANCH = employeeInfo._branch.Replace("\"", "");
            BRANCH = BRANCH.Replace("[", "");
            BRANCH = BRANCH.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = BRANCH });
            string DEPARTMENT = "";
            DEPARTMENT = employeeInfo._department.Replace("\"", "");
            DEPARTMENT = DEPARTMENT.Replace("[", "");
            DEPARTMENT = DEPARTMENT.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = DEPARTMENT });
            string SUBDEPARTMENT = "";
            SUBDEPARTMENT = employeeInfo._subdepartment.Replace("\"", "");
            SUBDEPARTMENT = SUBDEPARTMENT.Replace("[", "");
            SUBDEPARTMENT = SUBDEPARTMENT.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = SUBDEPARTMENT });
            string REQUESTOR = "";
            REQUESTOR = employeeInfo._userid.Replace("\"", "");
            REQUESTOR = REQUESTOR.Replace("[", "");
            REQUESTOR = REQUESTOR.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@REQUESTOR", mytype = SqlDbType.NVarChar, Value = REQUESTOR });
            employeeInfo empInfo = new employeeInfo();
            List<employeeInfoList> list = new List<employeeInfoList>();
            //DataSet ds = new DataSet();
            DataTable dt = con.GetDataTable("sp_SP_SearchEmployee");

            foreach (DataRow row in dt.Rows)
            {
                employeeInfoList param = new employeeInfoList()
                {
                    _userid = row["UserID"].ToString(),
                    _name = row["Name"].ToString(),
                    _area = row["Area"].ToString(),
                    _branch = row["Branch"].ToString(),
                    _department = row["Department"].ToString(),
                    _subdepartment = row["SubDepartment"].ToString(),
                    _userType = row["UserType"].ToString(),
                    _email = row["Email"].ToString(),
                    _phoneNumber = row["Phone_Number"].ToString(),
                    _status = row["Status"].ToString(),
                    _loginid = row["LoginID"].ToString()
                };
                list.Add(param);
            }
            empInfo.employeeInfoList = list;
            empInfo._myreturn = "Success";
            return empInfo;
        }
        [HttpPost]
        [Route("SchedulerPortalSelectEmployee")]
        public employeeInfo SelectEmployee(changePassword cp)
        {
            try
            {
                Connection con = new Connection();
                string USERID = "";
                USERID = cp._UID.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = USERID });
                DataTable dt = con.GetDataTable("sp_SP_SelectEmployee");
                employeeInfo empInfo = new employeeInfo();
                List<employeeInfoList> list = new List<employeeInfoList>();
                //DataSet ds = new DataSet();
                

                foreach (DataRow row in dt.Rows)
                {
                    employeeInfoList param = new employeeInfoList()
                    {
                        _firstName = row["First_Name"].ToString(),
                        _middleName = row["Middle_Name"].ToString(),
                        _lastName = row["Last_Name"].ToString(),
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                        _userType = row["UserType"].ToString(),
                        _email = row["Email"].ToString(),
                        _phoneNumber = row["Phone_Number"].ToString(),
                        _autoapprove = Convert.ToBoolean(row["Auto_Approve"].ToString())
                    };
                    list.Add(param);
                }
                empInfo.employeeInfoList = list;
                empInfo._myreturn = "Success";
                return empInfo;
            }
            catch (Exception e)
            {
                employeeInfo empInfo = new employeeInfo();
                List<employeeInfoList> list = new List<employeeInfoList>();
                empInfo.employeeInfoList = list;
                empInfo._myreturn = e.ToString();
                return empInfo;
            }
        }
        [HttpPost]
        [Route("SchedulerPortalCreateUser")]
        public string CreateUser(createuser cu)
        {
            try
            {
                Connection con = new Connection();
                string USERID = "";
                USERID = cu._userid.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = USERID });
                con.myparameters.Add(new myParameters { ParameterName = "@FNAME", mytype = SqlDbType.NVarChar, Value = cu._fname });
                con.myparameters.Add(new myParameters { ParameterName = "@MNAME", mytype = SqlDbType.NVarChar, Value = cu._mname });
                con.myparameters.Add(new myParameters { ParameterName = "@LNAME", mytype = SqlDbType.NVarChar, Value = cu._lname });
                con.myparameters.Add(new myParameters { ParameterName = "@TITLE", mytype = SqlDbType.NVarChar, Value = cu._title });
                con.myparameters.Add(new myParameters { ParameterName = "@ROLE", mytype = SqlDbType.NVarChar, Value = cu._role });
                con.myparameters.Add(new myParameters { ParameterName = "@AREA", mytype = SqlDbType.NVarChar, Value = cu._area });
                con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = cu._branch });
                con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = cu._department });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = cu._subdepartment });
                con.myparameters.Add(new myParameters { ParameterName = "@COMPANY", mytype = SqlDbType.NVarChar, Value = cu._company });
                con.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.NVarChar, Value = cu._usertype });
                con.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = cu._email });
                con.myparameters.Add(new myParameters { ParameterName = "@PHONE", mytype = SqlDbType.NVarChar, Value = cu._phone });
                con.myparameters.Add(new myParameters { ParameterName = "@UPDATETYPE", mytype = SqlDbType.NVarChar, Value = cu._updatetype });
                con.myparameters.Add(new myParameters { ParameterName = "@AUTOAPPROVE", mytype = SqlDbType.Bit, Value = cu._autoapprove });
                con.ExecuteNonQuery("sp_SP_InsertUpdateUser");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("SchedulerPortalUpdateUserType")]
        public string updateUserType(userType userType)
        {
            try
            {
                string USERID = "";
                USERID = userType._userID.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = USERID });
                con.myparameters.Add(new myParameters { ParameterName = "@UserType", mytype = SqlDbType.NVarChar, Value = userType._userType });
                con.ExecuteNonQuery("sp_SP_updateUserType");
                return "Success";
            }
            catch (Exception e)
            {
                return "Failed";
            }
        }
        [HttpPost]
        [Route("SchedulerPortalAuditTrail")]
        public AuditTrailList AuditTrail(AuditParameters AP)
        {
            try
            {
                Connection con = new Connection();
                AuditTrailList AuditTrailList = new AuditTrailList();
                List<AuditTrail> ListReturned = new List<AuditTrail>();
                con.myparameters.Add(new myParameters { ParameterName = "@DATEFROM", mytype = SqlDbType.NVarChar, Value = AP._datefrom });
                con.myparameters.Add(new myParameters { ParameterName = "@DATETO", mytype = SqlDbType.NVarChar, Value = AP._dateto });
                DataTable DT = con.GetDataTable("sp_SP_SearchAuditTrail");
                foreach (DataRow row in DT.Rows)
                {
                    AuditTrail AT = new AuditTrail()
                    {
                        _requestid = row["SeriesID"].ToString() == "" ? null : row["SeriesID"].ToString(),
                        _requestorname = row["Requestor"].ToString() == "" ? null : row["Requestor"].ToString(),
                        _companyname = row["CompanyName"].ToString() == "" ? null : row["CompanyName"].ToString(),
                        _daterequested = row["DateRequested"].ToString() == "" ? null : Convert.ToDateTime(row["DateRequested"]).ToString("yyyy-MM-dd"),
                        _timerequested = row["RequestedTime"].ToString() == "" ? null : row["RequestedTime"].ToString(),
                        _appointmentreason = row["Reason"].ToString() == "" ? null : row["Reason"].ToString(),
                        _status = row["Status"].ToString() == "" ? null : row["Status"].ToString(),
                        _modifiedby = row["ModifiedBy"].ToString() == "" ? null : row["ModifiedBy"].ToString(),
                        _modifiedon = row["ModifiedOn"].ToString() == "" ? null : row["ModifiedOn"].ToString()
                    };
                    ListReturned.Add(AT);
                }
                AuditTrailList.AuditTrail = ListReturned;
                AuditTrailList._myreturn = "Success";
                return AuditTrailList;
            }
            catch (Exception e)
            {
                AuditTrailList AuditTrailList = new AuditTrailList();
                List<AuditTrail> ListReturned = new List<AuditTrail>();
                AuditTrailList.AuditTrail = ListReturned;
                AuditTrailList._myreturn = e.ToString();
                return AuditTrailList;
            }
        }
        [HttpPost]
        [Route("SchedulerPortalDropdownHierarchy")]
        public DropdownHierarchyList DDHL(DropdownHierarchy DDH)
        {
            try
            {
                DropdownHierarchyList DHL = new DropdownHierarchyList();
                List<DropdownHierarchyItems> DHI = new List<DropdownHierarchyItems>();
                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[1] { new DataColumn("Items") });
                foreach (string item in DDH._items)
                {
                    DT.Rows.Add(item);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@SOURCE", mytype = SqlDbType.NVarChar, Value = DDH._source });
                con.myparameters.Add(new myParameters { ParameterName = "@TARGET", mytype = SqlDbType.NVarChar, Value = DDH._target });
                con.myparameters.Add(new myParameters { ParameterName = "@HIERARCHYITEMS", mytype = SqlDbType.Structured, Value = DT });
                DataTable DT2 = con.GetDataTable("sp_SP_DropdownHierarchy");
                foreach (DataRow row in DT2.Rows)
                {
                    DropdownHierarchyItems DI = new DropdownHierarchyItems()
                    {
                        _itemid = row["ItemID"].ToString(),
                        _itemname = row["ItemName"].ToString()
                    };
                    DHI.Add(DI);
                }
                DHL.DDITEMS = DHI;
                DHL._myreturn = "Success";
                return DHL;
            }
            catch (Exception e)
            {
                DropdownHierarchyList DHL = new DropdownHierarchyList();
                List<DropdownHierarchyItems> DHI = new List<DropdownHierarchyItems>();
                DHL.DDITEMS = DHI;
                DHL._myreturn = e.ToString();
                return DHL;
            }
        }
        [HttpPost]
        [Route("SchedulerPortalUserMap")]
        public UserList UserMapList(UserDataMappingItems UDMI)
        {
            try
            {
                UserList UserList = new UserList();
                List<UserMap> ListReturned = new List<UserMap>();
                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[12] { new DataColumn("Fname"), new DataColumn("Mname"), new DataColumn("Lname"), new DataColumn("Title"), new DataColumn("Role"), new DataColumn("Area"), new DataColumn("Branch"), new DataColumn("Department"), new DataColumn("SubDepartment"), new DataColumn("UserType"), new DataColumn("Email"), new DataColumn("PhoneNumber") });
                for (int i = 0; i < UDMI._fname.Length; i++)
                {
                    DT.Rows.Add(UDMI._fname[i], UDMI._mname[i], UDMI._lname[i], UDMI._title[i], UDMI._role[i], UDMI._area[i], UDMI._branch[i], UDMI._department[i], UDMI._subdepartment[i], UDMI._usertype[i], UDMI._email[i], UDMI._phonenumber[i]);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DT });
                DataTable DT2 = con.GetDataTable("sp_SP_UserMapping");
                foreach (DataRow row in DT2.Rows)
                {
                    UserMap UM = new UserMap()
                    {
                        _rowid = row["RowID"].ToString(),
                        _fname = row["FirstName"].ToString(),
                        _mname = row["MiddleName"].ToString(),
                        _lname = row["LastName"].ToString(),
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                        _usertype = row["UserType"].ToString(),
                        _email = row["Email"].ToString(),
                        _phonenumber = row["PhoneNumber"].ToString()
                    };
                    ListReturned.Add(UM);
                }
                UserList.UserMapList = ListReturned;
                UserList._myreturn = "Success";
                return UserList;
            }
            catch (Exception e)
            {
                UserList UserList = new UserList();
                List<UserMap> ListReturned = new List<UserMap>();
                UserList.UserMapList = ListReturned;
                UserList._myreturn = e.ToString();
                return UserList;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalDropdownHierarchyAU")]
        public subsubjects DDHAU(DropdownHierarchy DDHAU)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@SOURCE", mytype = SqlDbType.VarChar, Value = DDHAU._source });
                con.myparameters.Add(new myParameters { ParameterName = "@TARGET", mytype = SqlDbType.VarChar, Value = DDHAU._target });
                con.myparameters.Add(new myParameters { ParameterName = "@HIERARCHYITEMS", mytype = SqlDbType.VarChar, Value = DDHAU._paramater });
                DataTable DT2 = con.GetDataTable("sp_SP_DropdownHierarchy_AddUser");
                subsubjects DHL = new subsubjects();
                List<subsubjectlist> DHI = new List<subsubjectlist>();
                foreach (DataRow row in DT2.Rows)
                {
                    subsubjectlist DI = new subsubjectlist()
                    {
                        _RowID = row["ItemID"].ToString(),
                        _Subject = row["ItemName"].ToString()
                    };
                    DHI.Add(DI);
                }
                DHL.subsubjectlist = DHI;
                DHL._myreturn = "Success";
                return DHL;
            }
            catch (Exception e)
            {
                subsubjects DHL = new subsubjects();
                List<subsubjectlist> DHI = new List<subsubjectlist>();
                DHL.subsubjectlist = DHI;
                DHL._myreturn = e.ToString();
                return DHL;
            }
        }


        //---------------revised
        [HttpPost]
        [Route("SchedulerPortalLoginv2")] //Para sa login
        public string schedulerLoginv2(schedulerLoginv2 schedulerLogin)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = schedulerLogin._username });
                con.myparameters.Add(new myParameters { ParameterName = "@USERPASS", mytype = SqlDbType.NVarChar, Value = schedulerLogin._password });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_CODE", mytype = SqlDbType.NVarChar, Value = schedulerLogin.CN });
                return con.ExecuteScalar("sp_SP_LoginV2");
            }
            catch (Exception e)
            {
                return "Failed";
            }
        }


        [HttpPost]
        [Route("SchedulerPortalSubjectsv2")] //Listahan ng Subjects sa Labels
        public subjectsV2 schedulerSubjectsV2(subjectsV2 sb)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ClientID", mytype = SqlDbType.NVarChar, Value = sb.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClientID", mytype = SqlDbType.NVarChar, Value = sb.SCN }); 
                 subjectsV2 subjects = new subjectsV2();
                List<subjectlistv2> list = new List<subjectlistv2>();
                //DataSet ds = new DataSet();
                DataTable dt = con.GetDataTable("sp_SP_ShowSubjectsV2");

                foreach (DataRow row in dt.Rows)
                {
                    subjectlistv2 param = new subjectlistv2()
                    {
                        _SID = row["SID"].ToString(),
                        _Subject = row["Subject"].ToString(),
                        _Status = Convert.ToBoolean(row["Status"].ToString()),
                        _DefaultName = row["DefaultName"].ToString(),
                        _ModifiedBy = row["ModifiedBy"].ToString(),
                        _ModifiedDate = row["ModifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subjects.subjectlist = list;
                subjects._myreturn = "Success";
                return subjects;
            }
            catch (Exception e)
            {
                subjectsV2 subjects = new subjectsV2();
                List<subjectlistv2> list = new List<subjectlistv2>();
                subjects.subjectlist = list;
                subjects._myreturn = e.ToString();
                return subjects;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalSubSubjectsv2")] //Listahan ng SubSubjects sa labels, pasahan ng value 0 yung parameter para magdisplay lahat ng subsubjects else selected subsubjects lang lalabas
        public subsubjectsv2 schedulerSubSubjects(subjectlistv2 subject)
        {
            try
            {
                Connection con = new Connection();
                subsubjectsv2 subsubjects = new subsubjectsv2();
                List<subsubjectlist> list = new List<subsubjectlist>();
                //DataSet ds = new DataSet();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subject._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientID", mytype = SqlDbType.NVarChar, Value = subject.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClientID", mytype = SqlDbType.NVarChar, Value = subject.SCN });
                DataTable dt = con.GetDataTable("sp_SP_SelectShowSubSubjectsV2");

                foreach (DataRow row in dt.Rows)
                {
                    subsubjectlist param = new subsubjectlist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _SID = row["SID"].ToString(),
                        _Subject = row["SubSubject"].ToString(),
                        _Status = Convert.ToBoolean(row["Status"].ToString()),
                        _ModifiedBy = row["ModifiedBy"].ToString(),
                        _ModifiedDate = row["ModifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                subsubjectsv2 subsubjects = new subsubjectsv2();
                List<subsubjectlist> list = new List<subsubjectlist>();
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }


        [HttpPost]
        [Route("SchedulerPortalSubSubjectsv22")] //Listahan ng SubSubjects sa labels, pasahan ng value 0 yung parameter para magdisplay lahat ng subsubjects else selected subsubjects lang lalabas
        public subsubjectsv2 schedulerSubSubjectss(subjectlistv2 subject)
        {
            try
            {
                Connection con = new Connection();
                subsubjectsv2 subsubjects = new subsubjectsv2();
                List<subsubjectlist> list = new List<subsubjectlist>();
                //DataSet ds = new DataSet();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subject._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientID", mytype = SqlDbType.NVarChar, Value = subject.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClientID", mytype = SqlDbType.NVarChar, Value = subject.SCN });
                DataTable dt = con.GetDataTable("sp_SP_SelectShowSubSubjectsV22");

                foreach (DataRow row in dt.Rows)
                {
                    subsubjectlist param = new subsubjectlist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _SID = row["SID"].ToString(),
                        _Subject = row["SubSubject"].ToString(),
                        _Status = Convert.ToBoolean(row["Status"].ToString()),
                        _ModifiedBy = row["ModifiedBy"].ToString(),
                        _ModifiedDate = row["ModifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                subsubjectsv2 subsubjects = new subsubjectsv2();
                List<subsubjectlist> list = new List<subsubjectlist>();
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalListSimulatorv2")]
        public subsubjectsv2 ListSimulator(subjectlistv2 sl)
        {
            try
            {
                Connection con = new Connection();
                subsubjectsv2 subsubjects = new subsubjectsv2();
                List<subsubjectlist> list = new List<subsubjectlist>();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = sl._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientID", mytype = SqlDbType.NVarChar, Value = sl.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClientID", mytype = SqlDbType.NVarChar, Value = sl.SCN });
                DataTable dt = con.GetDataTable("sp_SP_SubSubjectSimulatorv2");

                foreach (DataRow row in dt.Rows)
                {
                    subsubjectlist param = new subsubjectlist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _Subject = row["SubSubject"].ToString()
                    };
                    list.Add(param);
                }
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                subsubjectsv2 subsubjects = new subsubjectsv2();
                List<subsubjectlist> list = new List<subsubjectlist>();
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }


        [HttpPost]
        [Route("SchedulerPortalSubjectStatusv2")] //Toggle ng checkbox for Subject = Active/Inactive
        public string SubjectStatusv2(subjectlistv2 subjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectlist._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = subjectlist.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = subjectlist.CN });
                con.ExecuteNonQuery("sp_SP_ActiveInactiveSubjectV2");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("SchedulerPortalUpdateSubjectv2")] // insert and update ng Subject. pag pinasahan ng 0 yung @SUBJECTID magiinsert siya ng panibago else iuupdate niya. yung @DEFAULTNAME original na name nung Subject
        public string UpdateSubject(subjectlistv2 subjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectlist._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTNAME", mytype = SqlDbType.NVarChar, Value = subjectlist._Subject });
                con.myparameters.Add(new myParameters { ParameterName = "@DEFAULTNAME", mytype = SqlDbType.NVarChar, Value = subjectlist._DefaultName });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = subjectlist.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = subjectlist.SCN });
                con.ExecuteScalar("sp_SP_InsertUpdateSubjectV2");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("SchedulerPortalUpdateSubSubjectv2")] //Insert and Update ng SubSubjects, pasahan ng 0 yung @SUBSUBJECTID para mag insert ng panibagong subsubject else iuupdate niya yung selected subsubject
        public string UpdateSubSubjectv2(subsubjectlist subsubjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._RowID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTNAME", mytype = SqlDbType.NVarChar, Value = subsubjectlist._Subject });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = subsubjectlist.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = subsubjectlist.SCN });
                con.ExecuteScalar("sp_SP_InsertUpdateSubSubjectListV2");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("SchedulerPortalRequestorListv2")]
        public requestorlistv2 RequestorList(requestorv2 requestor)
        {
            try
            {
                Connection con = new Connection();
                string USERTYPE = "";
                USERTYPE = requestor._usertype.Replace("\"", "");
                USERTYPE = USERTYPE.Replace("[", "");
                USERTYPE = USERTYPE.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.NVarChar, Value = USERTYPE });
                con.myparameters.Add(new myParameters { ParameterName = "@ClientID", mytype = SqlDbType.NVarChar, Value = requestor.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClientID", mytype = SqlDbType.NVarChar, Value = requestor.SCN });
                DataTable dt = con.GetDataTable("sp_SP_RequestorListv2");
                requestorlistv2 RL = new requestorlistv2();
                List<requestorv2> R = new List<requestorv2>();
                foreach (DataRow row in dt.Rows)
                {
                    requestorv2 req = new requestorv2()
                    {
                        _userid = row["UserID"].ToString(),
                        _requestorname = row["Requestor"].ToString()
                    };
                    R.Add(req);
                }
                RL.requestor = R;
                RL._myreturn = "Success";
                return RL;
            }
            catch (Exception e)
            {
                requestorlistv2 RL = new requestorlistv2();
                List<requestorv2> R = new List<requestorv2>();
                RL.requestor = R;
                RL._myreturn = e.ToString();
                return RL;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalSearchEmployeev2")] //Para sa viewing sa User Mgt pag nag search
        public employeeInfov2 SearchEmployee(employeeInfoListv2 employeeInfo)
        {
            Connection con = new Connection();
            string AREA = "";
            AREA = employeeInfo._area.Replace("\"", "");
            AREA = AREA.Replace("[", "");
            AREA = AREA.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@AREA", mytype = SqlDbType.NVarChar, Value = AREA });
            string BRANCH = "";
            BRANCH = employeeInfo._branch.Replace("\"", "");
            BRANCH = BRANCH.Replace("[", "");
            BRANCH = BRANCH.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = BRANCH });
            string DEPARTMENT = "";
            DEPARTMENT = employeeInfo._department.Replace("\"", "");
            DEPARTMENT = DEPARTMENT.Replace("[", "");
            DEPARTMENT = DEPARTMENT.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = DEPARTMENT });
            string SUBDEPARTMENT = "";
            SUBDEPARTMENT = employeeInfo._subdepartment.Replace("\"", "");
            SUBDEPARTMENT = SUBDEPARTMENT.Replace("[", "");
            SUBDEPARTMENT = SUBDEPARTMENT.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = SUBDEPARTMENT });
            string REQUESTOR = "";
            REQUESTOR = employeeInfo._userid.Replace("\"", "");
            REQUESTOR = REQUESTOR.Replace("[", "");
            REQUESTOR = REQUESTOR.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@REQUESTOR", mytype = SqlDbType.NVarChar, Value = REQUESTOR });
            con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = employeeInfo.CN });
            employeeInfov2 empInfo = new employeeInfov2();
            List<employeeInfoListv2> list = new List<employeeInfoListv2>();
            //DataSet ds = new DataSet();
            DataTable dt = con.GetDataTable("sp_SP_SearchEmployeeV2");

            foreach (DataRow row in dt.Rows)
            {
                employeeInfoListv2 param = new employeeInfoListv2()
                {
                    _userid = row["UserID"].ToString(),
                    _name = row["Name"].ToString(),
                    _area = row["Area"].ToString(),
                    _branch = row["Branch"].ToString(),
                    _department = row["Department"].ToString(),
                    _subdepartment = row["SubDepartment"].ToString(),
                    _userType = row["UserType"].ToString(),
                    _email = row["Email"].ToString(),
                    _phoneNumber = row["Phone_Number"].ToString(),
                    _status = row["Status"].ToString(),
                    _loginid = row["LoginID"].ToString()
                };
                list.Add(param);
            }
            empInfo.employeeInfoList = list;
            empInfo._myreturn = "Success";
            return empInfo;
        }

        [HttpPost]
        [Route("SchedulerPortalSearchEmployeev3")] //Para sa viewing sa User Mgt pag nag search
        public employeeInfov2 SearchEmployee3(employeeInfoListv2 employeeInfo)
        {
            Connection con = new Connection();
            string AREA = "";
            AREA = employeeInfo._area.Replace("\"", "");
            AREA = AREA.Replace("[", "");
            AREA = AREA.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@AREA", mytype = SqlDbType.NVarChar, Value = AREA });
            string BRANCH = "";
            BRANCH = employeeInfo._branch.Replace("\"", "");
            BRANCH = BRANCH.Replace("[", "");
            BRANCH = BRANCH.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = BRANCH });
            string DEPARTMENT = "";
            DEPARTMENT = employeeInfo._department.Replace("\"", "");
            DEPARTMENT = DEPARTMENT.Replace("[", "");
            DEPARTMENT = DEPARTMENT.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = DEPARTMENT });
            string SUBDEPARTMENT = "";
            SUBDEPARTMENT = employeeInfo._subdepartment.Replace("\"", "");
            SUBDEPARTMENT = SUBDEPARTMENT.Replace("[", "");
            SUBDEPARTMENT = SUBDEPARTMENT.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = SUBDEPARTMENT });
            string REQUESTOR = "";
            REQUESTOR = employeeInfo._userid.Replace("\"", "");
            REQUESTOR = REQUESTOR.Replace("[", "");
            REQUESTOR = REQUESTOR.Replace("]", "");
            con.myparameters.Add(new myParameters { ParameterName = "@REQUESTOR", mytype = SqlDbType.NVarChar, Value = REQUESTOR });
            con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = employeeInfo.CN });
            con.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = employeeInfo.SCN });
            employeeInfov2 empInfo = new employeeInfov2();
            List<employeeInfoListv2> list = new List<employeeInfoListv2>();
            //DataSet ds = new DataSet();
            DataTable dt = con.GetDataTable("sp_SP_SearchEmployeeV3");

            foreach (DataRow row in dt.Rows)
            {
                employeeInfoListv2 param = new employeeInfoListv2()
                {
                    _userid = row["UserID"].ToString(),
                    _name = row["Name"].ToString(),
                    _area = row["Area"].ToString(),
                    _branch = row["Branch"].ToString(),
                    _department = row["Department"].ToString(),
                    _subdepartment = row["SubDepartment"].ToString(),
                    _userType = row["UserType"].ToString(),
                    _email = row["Email"].ToString(),
                    _phoneNumber = row["Phone_Number"].ToString(),
                    _status = row["Status"].ToString(),
                    _loginid = row["LoginID"].ToString()
                };
                list.Add(param);
            }
            empInfo.employeeInfoList = list;
            empInfo._myreturn = "Success";
            return empInfo;
        }

        [HttpPost]
        [Route("SchedulerPortalCreateUserv2")]
        public string CreateUsev2r(createuserv2 cu)
        {
            try
            {
                Connection con = new Connection();
                string USERID = "";
                USERID = cu._userid.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[1] { new DataColumn("Items") });
                foreach (string item in cu._usertype)
                {
                    DT.Rows.Add(item);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = USERID });
                con.myparameters.Add(new myParameters { ParameterName = "@FNAME", mytype = SqlDbType.NVarChar, Value = cu._fname });
                con.myparameters.Add(new myParameters { ParameterName = "@MNAME", mytype = SqlDbType.NVarChar, Value = cu._mname });
                con.myparameters.Add(new myParameters { ParameterName = "@LNAME", mytype = SqlDbType.NVarChar, Value = cu._lname });
                con.myparameters.Add(new myParameters { ParameterName = "@TITLE", mytype = SqlDbType.NVarChar, Value = cu._title });
                con.myparameters.Add(new myParameters { ParameterName = "@ROLE", mytype = SqlDbType.NVarChar, Value = cu._role });
                con.myparameters.Add(new myParameters { ParameterName = "@AREA", mytype = SqlDbType.NVarChar, Value = cu._area });
                con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = cu._branch });
                con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = cu._department });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = cu._subdepartment });
                con.myparameters.Add(new myParameters { ParameterName = "@COMPANY", mytype = SqlDbType.NVarChar, Value = cu._company });
                con.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.Structured, Value = DT });
                con.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = cu._email });
                con.myparameters.Add(new myParameters { ParameterName = "@PHONE", mytype = SqlDbType.NVarChar, Value = cu._phone });
                con.myparameters.Add(new myParameters { ParameterName = "@UPDATETYPE", mytype = SqlDbType.NVarChar, Value = cu._updatetype });
                con.myparameters.Add(new myParameters { ParameterName = "@AUTOAPPROVE", mytype = SqlDbType.Bit, Value = cu._autoapprove });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = cu.CN });
                con.ExecuteNonQuery("sp_SP_InsertUpdateUserV2");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }


        [HttpPost]
        [Route("SchedulerPortalSubjectLabelsv2")]
        public subjectlabellistv2 SubjectLabelList(subjectlabelv2 sb)
        {
            try
            {
                Connection con = new Connection();
                subjectlabellistv2 SubjectLabelList = new subjectlabellistv2();
                List<subjectlabelv2> SL = new List<subjectlabelv2>();
                con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = sb.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClientID", mytype = SqlDbType.NVarChar, Value = sb.SCN });
                DataTable dt = con.GetDataTable("sp_SP_LabelSubjectsV2");
                foreach (DataRow row in dt.Rows)
                {
                    subjectlabelv2 param = new subjectlabelv2()
                    {
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                    };
                    SL.Add(param);
                }
                SubjectLabelList.subjectlabel = SL;
                SubjectLabelList._myreturn = "Success";
                return SubjectLabelList;
            }
            catch (Exception e)
            {
                subjectlabellistv2 SubjectLabelList = new subjectlabellistv2();
                List<subjectlabelv2> SL = new List<subjectlabelv2>();
                SubjectLabelList.subjectlabel = SL;
                SubjectLabelList._myreturn = e.ToString();
                return SubjectLabelList;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalSelectEmployeev3")]
        public employeeInfov2 SelectEmployee3(changePasswordv2 cp)
        {
            try
            {
                Connection con = new Connection();
                string USERID = "";
                USERID = cp._UID.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = USERID });
                con.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.NVarChar, Value = cp.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClient_ID", mytype = SqlDbType.NVarChar, Value = cp.SCN });
                DataTable dt = con.GetDataTable("sp_SP_SelectEmployeeV3");
                employeeInfov2 empInfo = new employeeInfov2();
                List<employeeInfoListv2> list = new List<employeeInfoListv2>();
                //DataSet ds = new DataSet();


                foreach (DataRow row in dt.Rows)
                {
                    employeeInfoListv2 param = new employeeInfoListv2()
                    {
                        _firstName = row["First_Name"].ToString(),
                        _middleName = row["Middle_Name"].ToString(),
                        _lastName = row["Last_Name"].ToString(),
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                        _userType = row["UserType"].ToString(),
                        _email = row["Email"].ToString(),
                        _phoneNumber = row["Phone_Number"].ToString(),
                        _autoapprove = Convert.ToBoolean(row["Auto_Approve"].ToString()),
                        _loginid = row["LoginID"].ToString()
                    };
                    list.Add(param);
                }
                empInfo.employeeInfoList = list;
                empInfo._myreturn = "Success";
                return empInfo;
            }
            catch (Exception e)
            {
                employeeInfov2 empInfo = new employeeInfov2();
                List<employeeInfoListv2> list = new List<employeeInfoListv2>();
                empInfo.employeeInfoList = list;
                empInfo._myreturn = e.ToString();
                return empInfo;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalSelectEmployeev2")]
        public employeeInfov2 SelectEmployee(changePasswordv2 cp)
        {
            try
            {
                Connection con = new Connection();
                string USERID = "";
                USERID = cp._UID.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = USERID });
                con.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.NVarChar, Value = cp.CN });
                DataTable dt = con.GetDataTable("sp_SP_SelectEmployeeV2");
                employeeInfov2 empInfo = new employeeInfov2();
                List<employeeInfoListv2> list = new List<employeeInfoListv2>();
                //DataSet ds = new DataSet();


                foreach (DataRow row in dt.Rows)
                {
                    employeeInfoListv2 param = new employeeInfoListv2()
                    {
                        _firstName = row["First_Name"].ToString(),
                        _middleName = row["Middle_Name"].ToString(),
                        _lastName = row["Last_Name"].ToString(),
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                        _userType = row["UserType"].ToString(),
                        _email = row["Email"].ToString(),
                        _phoneNumber = row["Phone_Number"].ToString(),
                        _autoapprove = Convert.ToBoolean(row["Auto_Approve"].ToString()),
                        _loginid = row["LoginID"].ToString()
                    };
                    list.Add(param);
                }
                empInfo.employeeInfoList = list;
                empInfo._myreturn = "Success";
                return empInfo;
            }
            catch (Exception e)
            {
                employeeInfov2 empInfo = new employeeInfov2();
                List<employeeInfoListv2> list = new List<employeeInfoListv2>();
                empInfo.employeeInfoList = list;
                empInfo._myreturn = e.ToString();
                return empInfo;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalDropdownHierarchyv2")]
        public DropdownHierarchyListv2 DDHL(DropdownHierarchyv2 DDH)
        {
            try
            {
                DropdownHierarchyListv2 DHL = new DropdownHierarchyListv2();
                List<DropdownHierarchyItemsv2> DHI = new List<DropdownHierarchyItemsv2>();
                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[1] { new DataColumn("Items") });
                foreach (string item in DDH._items)
                {
                    DT.Rows.Add(item);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@SOURCE", mytype = SqlDbType.NVarChar, Value = DDH._source });
                con.myparameters.Add(new myParameters { ParameterName = "@TARGET", mytype = SqlDbType.NVarChar, Value = DDH._target });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = DDH.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = DDH.SCN });
                con.myparameters.Add(new myParameters { ParameterName = "@HIERARCHYITEMS", mytype = SqlDbType.Structured, Value = DT });
                DataTable DT2 = con.GetDataTable("sp_SP_DropdownHierarchyV2");
                foreach (DataRow row in DT2.Rows)
                {
                    DropdownHierarchyItemsv2 DI = new DropdownHierarchyItemsv2()
                    {
                        _itemid = row["ItemID"].ToString(),
                        _itemname = row["ItemName"].ToString()
                    };
                    DHI.Add(DI);
                }
                DHL.DDITEMS = DHI;
                DHL._myreturn = "Success";
                return DHL;
            }
            catch (Exception e)
            {
                DropdownHierarchyListv2 DHL = new DropdownHierarchyListv2();
                List<DropdownHierarchyItemsv2> DHI = new List<DropdownHierarchyItemsv2>();
                DHL.DDITEMS = DHI;
                DHL._myreturn = e.ToString();
                return DHL;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalUserMapv2")]
        public UserListv2 UserMapListv2(UserDataMappingItemsv2 UDMI)
        {
            try
            {
                UserListv2 UserList = new UserListv2();
                List<UserMapv2> ListReturned = new List<UserMapv2>();
                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[13] { new DataColumn("Fname"), new DataColumn("Mname"), new DataColumn("Lname"), new DataColumn("Title"), new DataColumn("Role"), new DataColumn("Area"), new DataColumn("Branch"), new DataColumn("Department"), new DataColumn("SubDepartment"), new DataColumn("UserType"), new DataColumn("Email"), new DataColumn("PhoneNumber"), new DataColumn("PClient_ID") });
                for (int i = 0; i < UDMI._fname.Length; i++)
                {
                    DT.Rows.Add(UDMI._fname[i], UDMI._mname[i], UDMI._lname[i], UDMI._title[i], UDMI._role[i], UDMI._area[i], UDMI._branch[i], UDMI._department[i], UDMI._subdepartment[i], UDMI._usertype[i], UDMI._email[i], UDMI._phonenumber[i], UDMI.CN);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@IMPORT", mytype = SqlDbType.Structured, Value = DT });
                DataTable DT2 = con.GetDataTable("sp_SP_UserMappingV2");
                foreach (DataRow row in DT2.Rows)
                {
                    UserMapv2 UM = new UserMapv2()
                    {
                        _rowid = row["RowID"].ToString(),
                        _fname = row["FirstName"].ToString(),
                        _mname = row["MiddleName"].ToString(),
                        _lname = row["LastName"].ToString(),
                        _title = row["Title"].ToString(),
                        _role = row["Role"].ToString(),
                        _area = row["Area"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString(),
                        _subdepartment = row["SubDepartment"].ToString(),
                        _usertype = row["UserType"].ToString(),
                        _email = row["Email"].ToString(),
                        _phonenumber = row["PhoneNumber"].ToString(),
                        CN = row["ClientID"].ToString()
                    };
                    ListReturned.Add(UM);
                }
                UserList.UserMapList = ListReturned;
                UserList._myreturn = "Success";
                return UserList;
            }
            catch (Exception e)
            {
                UserListv2 UserList = new UserListv2();
                List<UserMapv2> ListReturned = new List<UserMapv2>();
                UserList.UserMapList = ListReturned;
                UserList._myreturn = e.ToString();
                return UserList;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalEmpInformationv2")] //Para sa viewing sa Profile
        public employeeInfov2 employeeInfo(employeeInfov2 employeeInfo)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = employeeInfo._UID });
            con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = employeeInfo.CN });
            employeeInfov2 empInfo = new employeeInfov2();
            List<employeeInfoListv2> list = new List<employeeInfoListv2>();
            //DataSet ds = new DataSet();
            DataTable dt = con.GetDataTable("sp_SP_EmployeeInfoV2");

            foreach (DataRow row in dt.Rows)
            {
                employeeInfoListv2 param = new employeeInfoListv2()
                {
                    _firstName = row["First_Name"].ToString(),
                    _middleName = row["Middle_Name"].ToString(),
                    _lastName = row["Last_Name"].ToString(),
                    _title = row["Title"].ToString(),
                    _role = row["Role"].ToString(),
                    _area = row["Area"].ToString(),
                    _branch = row["Branch"].ToString(),
                    _department = row["Department"].ToString(),
                    _subdepartment = row["SubDepartment"].ToString(),
                    _userType = row["UserType"].ToString(),
                    _email = row["Email"].ToString(),
                    _phoneNumber = row["Phone_Number"].ToString(),
                    _autoapprove = Convert.ToBoolean(row["Auto_Approve"].ToString())
                };
                list.Add(param);
            }
            empInfo.employeeInfoList = list;
            empInfo._myreturn = "Success";
            return empInfo;
        }

        [HttpPost]
        [Route("SchedulerPortalEmpInformationv3")] //Para sa viewing sa Profile
        public employeeInfov2 employeeInfo3(employeeInfov3 employeeInfo)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = employeeInfo._UID });
            con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = employeeInfo.CN });
            con.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = employeeInfo.SCN });

            employeeInfov2 empInfo = new employeeInfov2();
            List<employeeInfoListv2> list = new List<employeeInfoListv2>();
            //DataSet ds = new DataSet();
            DataTable dt = con.GetDataTable("sp_SP_EmployeeInfoV3");

            foreach (DataRow row in dt.Rows)
            {
                employeeInfoListv2 param = new employeeInfoListv2()
                {
                    _firstName = row["First_Name"].ToString(),
                    _middleName = row["Middle_Name"].ToString(),
                    _lastName = row["Last_Name"].ToString(),
                    _title = row["Title"].ToString(),
                    _role = row["Role"].ToString(),
                    _area = row["Area"].ToString(),
                    _branch = row["Branch"].ToString(),
                    _department = row["Department"].ToString(),
                    _subdepartment = row["SubDepartment"].ToString(),
                    _userType = row["UserType"].ToString(),
                    _email = row["Email"].ToString(),
                    _phoneNumber = row["Phone_Number"].ToString(),
                    _autoapprove = Convert.ToBoolean(row["Auto_Approve"].ToString())
                };
                list.Add(param);
            }
            empInfo.employeeInfoList = list;
            empInfo._myreturn = "Success";
            return empInfo;
        }

        [HttpPost]
        [Route("SchedulerPortalUpdateUserType3")]
        public string updateUserType3(userType3 userType)
        {
            try
            {
                string USERID = "";
                USERID = userType._userID.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = USERID });
                con.myparameters.Add(new myParameters { ParameterName = "@UserType", mytype = SqlDbType.NVarChar, Value = userType._userType });
                con.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.NVarChar, Value = userType.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SClient_ID", mytype = SqlDbType.NVarChar, Value = userType.SCN });
                con.ExecuteNonQuery("sp_SP_updateUserTypeV2");
                return "Success";
            }
            catch (Exception e)
            {
                return "Failed";
            }
        }


        [HttpPost]
        [Route("SchedulerPortalGridRequestListv2")] //Items sa table sa gitna sa may To Do. yung apat na columns, pasahan ng status either: "New", "Pending", "Approved"
        public gridrequestlistv2 GridRequestList(gridsearchv2 gr)
        {
            try
            {
                Connection con = new Connection();

                con.myparameters.Add(new myParameters { ParameterName = "@STATUS", mytype = SqlDbType.NVarChar, Value = gr._status });
                string BRANCH = "";
                BRANCH = gr._branch.Replace("\"", "");
                BRANCH = BRANCH.Replace("[", "");
                BRANCH = BRANCH.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = BRANCH });
                string DEPARTMENT = "";
                DEPARTMENT = gr._department.Replace("\"", "");
                DEPARTMENT = DEPARTMENT.Replace("[", "");
                DEPARTMENT = DEPARTMENT.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = DEPARTMENT });
                string SUBDEPARTMENT = "";
                SUBDEPARTMENT = gr._subdepartment.Replace("\"", "");
                SUBDEPARTMENT = SUBDEPARTMENT.Replace("[", "");
                SUBDEPARTMENT = SUBDEPARTMENT.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = SUBDEPARTMENT });
                string REQUESTOR = "";
                REQUESTOR = gr._requestor.Replace("\"", "");
                REQUESTOR = REQUESTOR.Replace("[", "");
                REQUESTOR = REQUESTOR.Replace("]", "");
                con.myparameters.Add(new myParameters { ParameterName = "@REQUESTOR", mytype = SqlDbType.NVarChar, Value = REQUESTOR });
                con.myparameters.Add(new myParameters { ParameterName = "@DATEFROM", mytype = SqlDbType.NVarChar, Value = gr._datefrom });
                con.myparameters.Add(new myParameters { ParameterName = "@DATETO", mytype = SqlDbType.NVarChar, Value = gr._dateto });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = gr.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = gr.SCN });
                DataTable dt = con.GetDataTable("sp_SP_ViewRequestsV2");
                gridrequestlistv2 gridrequest = new gridrequestlistv2();
                List<gridrequestv2> list = new List<gridrequestv2>();
                foreach (DataRow row in dt.Rows)
                {
                    gridrequestv2 param = new gridrequestv2()
                    {
                        _requestid = row["SeriesID"].ToString(),
                        _requestor = row["Requestor"].ToString(),
                        _company = row["Company"].ToString(),
                        _branch = row["Branch"].ToString(),
                        _department = row["Department"].ToString()
                    };
                    list.Add(param);
                }
                gridrequest.gridrequest = list;
                gridrequest._myreturn = "Success";
                return gridrequest;
            }
            catch (Exception e)
            {
                gridrequestlistv2 gridrequest = new gridrequestlistv2();
                List<gridrequestv2> list = new List<gridrequestv2>();
                gridrequest.gridrequest = list;
                gridrequest._myreturn = e.ToString();
                return gridrequest;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalAuditTrailv2")]
        public AuditTrailListv2 AuditTrail(AuditParametersv2 AP)
        {
            try
            {
                Connection con = new Connection();
                AuditTrailListv2 AuditTrailList = new AuditTrailListv2();
                List<AuditTrailv2> ListReturned = new List<AuditTrailv2>();
                con.myparameters.Add(new myParameters { ParameterName = "@DATEFROM", mytype = SqlDbType.NVarChar, Value = AP._datefrom });
                con.myparameters.Add(new myParameters { ParameterName = "@DATETO", mytype = SqlDbType.NVarChar, Value = AP._dateto });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = AP.CN });
                DataTable DT = con.GetDataTable("sp_SP_SearchAuditTrailV2");
                foreach (DataRow row in DT.Rows)
                {
                    AuditTrailv2 AT = new AuditTrailv2()
                    {
                        _requestid = row["SeriesID"].ToString() == "" ? null : row["SeriesID"].ToString(),
                        _requestorname = row["Requestor"].ToString() == "" ? null : row["Requestor"].ToString(),
                        _companyname = row["CompanyName"].ToString() == "" ? null : row["CompanyName"].ToString(),
                        _daterequested = row["DateRequested"].ToString() == "" ? null : Convert.ToDateTime(row["DateRequested"]).ToString("yyyy-MM-dd"),
                        _timerequested = row["RequestedTime"].ToString() == "" ? null : row["RequestedTime"].ToString(),
                        _appointmentreason = row["Reason"].ToString() == "" ? null : row["Reason"].ToString(),
                        _status = row["Status"].ToString() == "" ? null : row["Status"].ToString(),
                        _modifiedby = row["ModifiedBy"].ToString() == "" ? null : row["ModifiedBy"].ToString(),
                        _modifiedon = row["ModifiedOn"].ToString() == "" ? null : row["ModifiedOn"].ToString()
                    };
                    ListReturned.Add(AT);
                }
                AuditTrailList.AuditTrail = ListReturned;
                AuditTrailList._myreturn = "Success";
                return AuditTrailList;
            }
            catch (Exception e)
            {
                AuditTrailListv2 AuditTrailList = new AuditTrailListv2();
                List<AuditTrailv2> ListReturned = new List<AuditTrailv2>();
                AuditTrailList.AuditTrail = ListReturned;
                AuditTrailList._myreturn = e.ToString();
                return AuditTrailList;
            }
        }

        [HttpPost]
        [Route("SchedulerPortalGetMaxUserID")]
        public GetMaxUserID MaxUserID()
        {
            GetMaxUserID GMUID = new GetMaxUserID();
            try
            {
                Connection con = new Connection();
                GMUID._maxuid = con.ExecuteScalar("sp_SP_MaxID");
                GMUID._myreturn = "Success";
            }
            catch (Exception e)
            {
                GMUID._myreturn = e.ToString();
            }
            return GMUID;
        }

        //[HttpPost]
        //[Route("ScheduleToDoNotif")]
        //public ScheduleToDoNotiff getNotiff()
        //{
        //    ScheduleToDoNotiff getToDoCount = new ScheduleToDoNotiff();
        //    Connection con = new Connection();
        //    List<getNotiff> countList = new List<getNotiff>();
        //    DataTable dt = con.GetDataTable("sp_getNotif");

        //    foreach (DataRow row in dt.Rows)
        //    {
        //        getNotiff param = new getNotiff()
        //        {
        //            getNotif = row["Notif"].ToString()
        //        };
        //        countList.Add(param);
        //    }
        //    getToDoCount.getNotifCount = countList;
        //    getToDoCount.myreturn = "Success";
        //    return getToDoCount;
        //}

        [HttpPost]
        [Route("ScheduleToDoNotif")]
        public ScheduleToDoNotiff getNotiff(notif n)
        {
            ScheduleToDoNotiff getToDoCount = new ScheduleToDoNotiff();
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@ClientID", mytype = SqlDbType.VarChar, Value = n.ClientID });
            con.myparameters.Add(new myParameters { ParameterName = "@SubClient_ID", mytype = SqlDbType.VarChar, Value = n.SubClientID });
            List<getNotiff> countList = new List<getNotiff>();
            DataTable dt = con.GetDataTable("sp_getNotif");

            foreach (DataRow row in dt.Rows)
            {
                getNotiff param = new getNotiff()
                {
                    getNotif = row["Notif"].ToString()
                };
                countList.Add(param);
            }
            getToDoCount.getNotifCount = countList;
            getToDoCount.myreturn = "Success";
            return getToDoCount;
        }

        [HttpPost]
        [Route("PulongWebLogin")]
        public PulongWebList PulongWeb(PulongWebParams PWP)
        {
            PulongWebList PWL = new PulongWebList();
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.VarChar, Value = PWP.UserID });
                con.myparameters.Add(new myParameters { ParameterName = "@USERPASS", mytype = SqlDbType.VarChar, Value = PWP.Userpass });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_CODE", mytype = SqlDbType.VarChar, Value = PWP.Client_Code });
                DataTable DT = con.GetDataTable("sp_Pulong_LoginV3");
                List<PulongWebReturnDetails> WRD = new List<PulongWebReturnDetails>();
                foreach (DataRow row in DT.Rows)
                {
                    PulongWebReturnDetails RD = new PulongWebReturnDetails()
                    {
                        EmpID = row["EmpID"].ToString(),
                        PClientID = row["PClientID"].ToString(),
                        SubClientID = row["SubClientID"].ToString(),
                        userName = row["userName"].ToString()
                    };
                    WRD.Add(RD);
                }
                PWL.PulongWebReturnDetails = WRD;
                PWL._myreturn = "Success";
            }
            catch (Exception e)
            {
                PWL._myreturn = e.ToString();
            }
            return PWL;
        }

        [HttpPost]
        [Route("SchedulerLogin")]
        public SchedulerList Scheduler(SchedulerParams SP)
        {
            SchedulerList SL = new SchedulerList();
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USEREMAIL", mytype = SqlDbType.VarChar, Value = SP.USEREMAIL });
                con.myparameters.Add(new myParameters { ParameterName = "@PASSWORD", mytype = SqlDbType.VarChar, Value = SP.PASSWORD });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENTCODE", mytype = SqlDbType.VarChar, Value = SP.CLIENTCODE });
                DataTable DT = con.GetDataTable("sp_Scheduler_Login");
                List<SchedulerReturnDetails> SRD = new List<SchedulerReturnDetails>();
                foreach (DataRow row in DT.Rows)
                {
                    SchedulerReturnDetails RD = new SchedulerReturnDetails()
                    {
                        PClientID = row["PClientID"].ToString(),
                        SubClientID = row["SubClientID"].ToString(),
                        UserType = row["UserType"].ToString(),
                        AutoApprove = row["AutoApprove"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        UserID = row["UserID"].ToString()
                        
                    };
                    SRD.Add(RD);
                }
                SL.SchedulerReturnDetails = SRD;
                SL._myreturn = "Success";
            }
            catch (Exception e)
            {
                SL._myreturn = e.ToString();
            }
            return SL;
        }

        [HttpPost]
        [Route("GetCompanyDetails")]
        public List<companylist> getcompanydetails(companyupdateparams prms)
        {
            List<companylist> cl = new List<companylist>();
            DataTable dt = new DataTable();
            try
            {
                Connection con1 = new Connection();
                con1.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.NVarChar, Value = prms.PClient_ID });
                con1.myparameters.Add(new myParameters { ParameterName = "@SubClientCode", mytype = SqlDbType.NVarChar, Value = prms.SubClientCode });
                dt = con1.GetDataTable("sp_GetCompanyDetails");

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    companylist cml = new companylist();
                    cml.SubClient_ID = dt.Rows[0]["SubClient_ID"].ToString();
                    cml.SubClientName = dt.Rows[0]["SubClientName"].ToString();
                    cml.SubClientCode = dt.Rows[0]["SubClientCode"].ToString();
                    cml.DateOfIncorporation = dt.Rows[0]["DateOfIncorporation"].ToString();
                    cml.Floor = dt.Rows[0]["Floor"].ToString();
                    cml.Building = dt.Rows[0]["Building"].ToString();
                    cml.Street = dt.Rows[0]["Street"].ToString();
                    cml.Barangay = dt.Rows[0]["Barangay"].ToString();
                    cml.Municipality = dt.Rows[0]["Municipality"].ToString();
                    cml.Province = dt.Rows[0]["Province"].ToString();
                    cml.Region = dt.Rows[0]["Region"].ToString();
                    cml.Country = dt.Rows[0]["Country"].ToString();
                    cml.ZipCode = dt.Rows[0]["ZipCode"].ToString();
                    cml.CompanyNationality = dt.Rows[0]["CompanyNationality"].ToString();
                    cml.NumberOfEmployees = dt.Rows[0]["NumberOfEmployees"].ToString();
                    cml.CompanyTin = dt.Rows[0]["CompanyTin"].ToString();
                    cml.RepUserID = dt.Rows[0]["UserID"].ToString();
                    cml.RepUserName = dt.Rows[0]["UserName"].ToString();
                    cml.RepFirstName = dt.Rows[0]["First_Name"].ToString();
                    cml.RepMiddleName = dt.Rows[0]["Middle_Name"].ToString();
                    cml.RepLastName = dt.Rows[0]["Last_Name"].ToString();
                    cml.RepTitle = dt.Rows[0]["RepTitle"].ToString();
                    cml.RepEmail = dt.Rows[0]["Email"].ToString();
                    cml.RepPhoneNumber = dt.Rows[0]["Phone_Number"].ToString();
                    cml.message = "Success";
                    cl.Add(cml);
                }
                return cl;
            }
            catch (Exception e)
            {
                companylist cml = new companylist();
                cml.message = e.Message;
                cl.Add(cml);
                return cl;
            }
        }

        [HttpPost]
        [Route("updatePartnerDetails")]
        public string updatePartnerDetails(updatecompanyparams prms)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.NVarChar, Value = prms.PClient_ID });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClient_ID", mytype = SqlDbType.NVarChar, Value = prms.SubClient_ID });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClientName", mytype = SqlDbType.NVarChar, Value = prms.SubClientName });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClientCode", mytype = SqlDbType.NVarChar, Value = prms.SubClientCode });
                con.myparameters.Add(new myParameters { ParameterName = "@DateOfIncorporation", mytype = SqlDbType.NVarChar, Value = prms.DateOfIncorporation });
                con.myparameters.Add(new myParameters { ParameterName = "@Floor", mytype = SqlDbType.NVarChar, Value = prms.Floor });
                con.myparameters.Add(new myParameters { ParameterName = "@Building", mytype = SqlDbType.NVarChar, Value = prms.Building });
                con.myparameters.Add(new myParameters { ParameterName = "@Street", mytype = SqlDbType.NVarChar, Value = prms.Street });
                con.myparameters.Add(new myParameters { ParameterName = "@Barangay", mytype = SqlDbType.NVarChar, Value = prms.Barangay });
                con.myparameters.Add(new myParameters { ParameterName = "@Municipality", mytype = SqlDbType.NVarChar, Value = prms.Municipality });
                con.myparameters.Add(new myParameters { ParameterName = "@Province", mytype = SqlDbType.NVarChar, Value = prms.Province });
                con.myparameters.Add(new myParameters { ParameterName = "@Region", mytype = SqlDbType.NVarChar, Value = prms.Region });
                con.myparameters.Add(new myParameters { ParameterName = "@Country", mytype = SqlDbType.NVarChar, Value = prms.Country });
                con.myparameters.Add(new myParameters { ParameterName = "@ZipCode", mytype = SqlDbType.NVarChar, Value = prms.ZipCode });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyNationality", mytype = SqlDbType.NVarChar, Value = prms.CompanyNationality });
                con.myparameters.Add(new myParameters { ParameterName = "@NumberOfEmployees", mytype = SqlDbType.NVarChar, Value = prms.NumberOfEmployees });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyTin", mytype = SqlDbType.NVarChar, Value = prms.CompanyTin });
                con.myparameters.Add(new myParameters { ParameterName = "@RepUserName", mytype = SqlDbType.NVarChar, Value = prms.RepUserName });
                con.myparameters.Add(new myParameters { ParameterName = "@RepFirstName", mytype = SqlDbType.NVarChar, Value = prms.RepFirstName });
                con.myparameters.Add(new myParameters { ParameterName = "@RepMiddleName", mytype = SqlDbType.NVarChar, Value = prms.RepMiddleName });
                con.myparameters.Add(new myParameters { ParameterName = "@RepLastName", mytype = SqlDbType.NVarChar, Value = prms.RepLastName });
                con.myparameters.Add(new myParameters { ParameterName = "@RepTitle", mytype = SqlDbType.NVarChar, Value = prms.RepTitle });
                con.myparameters.Add(new myParameters { ParameterName = "@RepEmail", mytype = SqlDbType.NVarChar, Value = prms.RepEmail });
                con.myparameters.Add(new myParameters { ParameterName = "@RepPhoneNumber", mytype = SqlDbType.NVarChar, Value = prms.RepPhoneNumber });
                con.myparameters.Add(new myParameters { ParameterName = "@prevUserID", mytype = SqlDbType.NVarChar, Value = prms.prevUserID });
                con.ExecuteNonQuery("sp_updateCompanyDetails");
                return "true";
            }
            catch (Exception e)
            {
                return "" + e;
            }
        }

        [HttpPost]
        [Route("CheckRepUsername")]
        public string checkrepusername(checkrepusername prms)
        {
            try
            {
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.NVarChar, Value = prms.PClient_ID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@RepUserName", mytype = SqlDbType.NVarChar, Value = prms.RepUserName });
                return Connection.ExecuteScalar("sp_checkRepUserName").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        #region PULONG MOBILE
        [HttpPost]
        [Route("GetScheduleForDemo")]
        public GetDemoSchedule DemoSchedule(DemoScheduleParams DSP)
        {
            GetDemoSchedule GDS = new GetDemoSchedule();
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.VarChar, Value = DSP._scheddate });
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.VarChar, Value = DSP._userid });
                DataTable DT = con.GetDataTable("sp_GetScheduleforDemo");
                List<DemoSchedule> LDS = new List<DemoSchedule>();
                foreach (DataRow row in DT.Rows)
                {
                    DemoSchedule DS = new DemoSchedule()
                    {
                        _ID = row["ID"].ToString(),
                        _StartTime = row["StartTime"].ToString(),
                        _EndTime = row["EndTime"].ToString(),
                        _Status = row["Status"].ToString()
                    };
                    LDS.Add(DS);
                }
                GDS.DemoSchedule = LDS;
                GDS._myreturn = "Success";
            }
            catch (Exception e)
            {
                GDS._myreturn = e.ToString();
            }
            return GDS;
        }

        [HttpPost]
        [Route("GetUserSchedule")]
        public GetUserSchedule UserSchedule(DemoScheduleParams DSP)
        {
            GetUserSchedule GUS = new GetUserSchedule();
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.VarChar, Value = DSP._scheddate });
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.VarChar, Value = DSP._userid });
                DataTable DT = con.GetDataTable("sp_GetUserSchedule");
                List<UserSchedule> LUS = new List<UserSchedule>();
                foreach (DataRow row in DT.Rows)
                {
                    UserSchedule US = new UserSchedule()
                    {
                        _SeriesID = row["SeriesID"].ToString(),
                        _CompanyID = row["CompanyID"].ToString(),
                        _StartDate = row["StartDate"].ToString(),
                        _EndDate = row["EndDate"].ToString(),
                        _StartTime = row["StartTime"].ToString(),
                        _EndTime = row["EndTime"].ToString(),
                        _Status = row["Status"].ToString(),
                        _UserID = row["UserID"].ToString()
                    };
                    LUS.Add(US);
                }
                GUS.UserSchedule = LUS;
                GUS._myreturn = "Success";
            }
            catch (Exception e)
            {
                GUS._myreturn = e.ToString();
            }
            return GUS;
        }

        [HttpPost]
        [Route("GetCompanyNames")]
        public GetCompanyNames CompanyNames(GetCompanyNamesParams GCNP)
        {
            GetCompanyNames GCN = new GetCompanyNames();
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = GCNP.CN });
                DataTable DT = con.GetDataTable("sp_GetCompanyList");
                List<Company> Company = new List<Company>();
                foreach (DataRow row in DT.Rows)
                {
                    Company C = new Company()
                    {
                        _CompanyName = row["Company_Names"].ToString()
                    };
                    Company.Add(C);
                }
                GCN.Companies = Company;
                GCN._myreturn = "Success";
            }
            catch (Exception e)
            {
                GCN._myreturn = e.ToString();
            }
            return GCN;
        }
        [HttpPost]
        [Route("GetCompanyNamesv2")]
        public List<string> Companies(GetCompanyNamesParams GCNP)
        {
            Company Company = new Company();
            System.Collections.ArrayList myList = new System.Collections.ArrayList(10);
            //DataTable selprov = new DataTable();
            List<string> aWhich = new List<string>();

            //selprov.Columns.AddRange(new DataColumn[1] { new DataColumn("ProvinceID") });
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = GCNP.CN });
                DataTable DT = con.GetDataTable("sp_GetCompanyList");

                foreach (DataRow row in DT.Rows)
                {
                    aWhich.Add(row["Company_Names"].ToString());
                    //selprov.Rows.Add();
                }
                //return DT;
            }
            catch (Exception e)
            {

            }
            return aWhich;
        }
        [HttpPost]
        [Route("GetCompanyNamesv3")]
        public GetCompanyNamesv2 Companiesv3(GetCompanyNamesParams GCNP)
        {
            GetCompanyNamesv2 thisList = new GetCompanyNamesv2();
            List<Companyv2> companyList = new List<Companyv2>();
            //selprov.Columns.AddRange(new DataColumn[1] { new DataColumn("ProvinceID") });
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyName", mytype = SqlDbType.VarChar, Value = GCNP.CN });
                DataTable DT = new DataTable();
                DT = con.GetDataTable("sp_GetCompanyList");

                if (DT.Rows.Count >= 0)
                {
                    foreach (DataRow row in DT.Rows)
                    {
                        Companyv2 returnList = new Companyv2
                        {
                            companyName = row["Company_Names"].ToString()
                        };
                        companyList.Add(returnList);
                    }
                }
                thisList.CompanyNames = companyList;
                thisList._myreturn = "Sucess";
            }
            catch (Exception e)
            {
                thisList._myreturn = e.ToString();
            }
            return thisList;
        }

        [HttpPost]
        [Route("LoginPulongMobile")]
        public LoginList LoginDetails(LoginParams LP)
        {
            LoginList LGN = new LoginList();
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USEREMAIL", mytype = SqlDbType.VarChar, Value = LP.username });
                con.myparameters.Add(new myParameters { ParameterName = "@PASSWORD", mytype = SqlDbType.VarChar, Value = LP.password });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENTCODE", mytype = SqlDbType.VarChar, Value = LP.PClientCode });
                DataTable DT = con.GetDataTable("sp_LoginRevisedv2");
                List<LoginDetails> LD = new List<LoginDetails>();
                foreach (DataRow row in DT.Rows)
                {
                    LoginDetails LDS = new LoginDetails()
                    {
                        PClientID = row["PClientID"].ToString(),
                        SubClientID = row["SubClientID"].ToString(),
                        UserType = row["UserType"].ToString(),
                        AutoApprove = row["AutoApprove"].ToString(),
                        EmpName = row["EmpName"].ToString(),
                        USERID = row["UserID"].ToString()
                    };
                    LD.Add(LDS);
                }
                LGN.LoginDetails = LD;
                LGN._myreturn = "Success";
            }
            catch (Exception e)
            {
                LGN._myreturn = e.ToString();
            }
            return LGN;
        }
        #endregion

        [HttpPost]
        [Route("SchedulerPortalCreateUserv3")]
        public string CreateUsev3r(createuserv3 cu)
        {
            try
            {
                Connection con = new Connection();
                //string USERID = "";
                //USERID = cu._userid.Replace("\"", "");
                //USERID = USERID.Replace("[", "");
                //USERID = USERID.Replace("]", "");
                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[1] { new DataColumn("Items") });
                foreach (string item in cu._usertype)
                {
                    DT.Rows.Add(item);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = cu.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = cu.SCN });
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = cu._userid });
                con.myparameters.Add(new myParameters { ParameterName = "@FNAME", mytype = SqlDbType.NVarChar, Value = cu._fname });
                con.myparameters.Add(new myParameters { ParameterName = "@MNAME", mytype = SqlDbType.NVarChar, Value = cu._mname });
                con.myparameters.Add(new myParameters { ParameterName = "@LNAME", mytype = SqlDbType.NVarChar, Value = cu._lname });
                con.myparameters.Add(new myParameters { ParameterName = "@TITLE", mytype = SqlDbType.NVarChar, Value = cu._title });
                con.myparameters.Add(new myParameters { ParameterName = "@ROLE", mytype = SqlDbType.NVarChar, Value = cu._role });
                con.myparameters.Add(new myParameters { ParameterName = "@AREA", mytype = SqlDbType.NVarChar, Value = cu._area });
                con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = cu._branch });
                con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = cu._department });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = cu._subdepartment });
                con.myparameters.Add(new myParameters { ParameterName = "@COMPANY", mytype = SqlDbType.NVarChar, Value = cu._company });
                con.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.Structured, Value = DT });
                con.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = cu._email });
                con.myparameters.Add(new myParameters { ParameterName = "@PHONE", mytype = SqlDbType.NVarChar, Value = cu._phone });
                con.myparameters.Add(new myParameters { ParameterName = "@UPDATETYPE", mytype = SqlDbType.NVarChar, Value = cu._updatetype });
                con.myparameters.Add(new myParameters { ParameterName = "@AUTOAPPROVE", mytype = SqlDbType.Bit, Value = cu._autoapprove });
                con.ExecuteNonQuery("sp_SP_InsertUpdateUserV3");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("SchedulerPortalUpdateUserv3")]
        public string UpdateUsev3r(createuserv3 cu)
        {
            try
            {
                Connection con = new Connection();
                string USERID = "";
                USERID = cu._userid.Replace("\"", "");
                USERID = USERID.Replace("[", "");
                USERID = USERID.Replace("]", "");
                //DataTable USERID = new DataTable();
                //USERID.Columns.AddRange(new DataColumn[1] { new DataColumn("Items") });
                //foreach (string item in cu._usertype)
                //{
                //    USERID.Rows.Add(item);
                //}
                DataTable DT = new DataTable();
                DT.Columns.AddRange(new DataColumn[1] { new DataColumn("Items") });
                foreach (string item in cu._usertype)
                {
                    DT.Rows.Add(item);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = cu.CN });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = cu.SCN });
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = USERID });
                con.myparameters.Add(new myParameters { ParameterName = "@FNAME", mytype = SqlDbType.NVarChar, Value = cu._fname });
                con.myparameters.Add(new myParameters { ParameterName = "@MNAME", mytype = SqlDbType.NVarChar, Value = cu._mname });
                con.myparameters.Add(new myParameters { ParameterName = "@LNAME", mytype = SqlDbType.NVarChar, Value = cu._lname });
                con.myparameters.Add(new myParameters { ParameterName = "@TITLE", mytype = SqlDbType.NVarChar, Value = cu._title });
                con.myparameters.Add(new myParameters { ParameterName = "@ROLE", mytype = SqlDbType.NVarChar, Value = cu._role });
                con.myparameters.Add(new myParameters { ParameterName = "@AREA", mytype = SqlDbType.NVarChar, Value = cu._area });
                con.myparameters.Add(new myParameters { ParameterName = "@BRANCH", mytype = SqlDbType.NVarChar, Value = cu._branch });
                con.myparameters.Add(new myParameters { ParameterName = "@DEPARTMENT", mytype = SqlDbType.NVarChar, Value = cu._department });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBDEPARTMENT", mytype = SqlDbType.NVarChar, Value = cu._subdepartment });
                con.myparameters.Add(new myParameters { ParameterName = "@COMPANY", mytype = SqlDbType.NVarChar, Value = cu._company });
                con.myparameters.Add(new myParameters { ParameterName = "@USERTYPE", mytype = SqlDbType.Structured, Value = DT });
                con.myparameters.Add(new myParameters { ParameterName = "@EMAIL", mytype = SqlDbType.NVarChar, Value = cu._email });
                con.myparameters.Add(new myParameters { ParameterName = "@PHONE", mytype = SqlDbType.NVarChar, Value = cu._phone });
                con.myparameters.Add(new myParameters { ParameterName = "@UPDATETYPE", mytype = SqlDbType.NVarChar, Value = cu._updatetype });
                con.myparameters.Add(new myParameters { ParameterName = "@AUTOAPPROVE", mytype = SqlDbType.Bit, Value = cu._autoapprove });
                con.ExecuteNonQuery("sp_SP_updateUsers");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("updateSchedV2")]
        public string updateSchedV2(updateField update)
        {
            try
            {
                DataTable empIds = new DataTable();
                empIds.Columns.AddRange(new DataColumn[1] { new DataColumn("EmpID") });
                for (int i = 0; i < update.ID.Length; i++)
                {
                    empIds.Rows.Add(update.ID[i]);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDULE", mytype = SqlDbType.NVarChar, Value = update.SCHEDULE });
                con.myparameters.Add(new myParameters { ParameterName = "@START", mytype = SqlDbType.NVarChar, Value = update.START });
                con.myparameters.Add(new myParameters { ParameterName = "@FIRSTBREAK", mytype = SqlDbType.NVarChar, Value = update.FIRSTBREAK });
                con.myparameters.Add(new myParameters { ParameterName = "@LUNCH", mytype = SqlDbType.NVarChar, Value = update.LUNCH });
                con.myparameters.Add(new myParameters { ParameterName = "@SECONDBREAK", mytype = SqlDbType.NVarChar, Value = update.SECONDBREAK });
                con.myparameters.Add(new myParameters { ParameterName = "@ENDBREAK", mytype = SqlDbType.NVarChar, Value = update.ENDBREAK });
                con.myparameters.Add(new myParameters { ParameterName = "@ENDOFSHIFT", mytype = SqlDbType.NVarChar, Value = update.ENDOFSHIFT });
                con.myparameters.Add(new myParameters { ParameterName = "@serID", mytype = SqlDbType.Structured, Value = empIds });
                con.myparameters.Add(new myParameters { ParameterName = "@FIRSTBREAKDSP", mytype = SqlDbType.NVarChar, Value = update.FIRSTBREAKDSP });
                con.myparameters.Add(new myParameters { ParameterName = "@SECONDBREAKDSP", mytype = SqlDbType.NVarChar, Value = update.SECONDBREAKDSP });
                con.myparameters.Add(new myParameters { ParameterName = "@ENDBREAKDSP", mytype = SqlDbType.NVarChar, Value = update.ENDBREAKDSP });
                con.myparameters.Add(new myParameters { ParameterName = "@LUNCHBREAKEND", mytype = SqlDbType.NVarChar, Value = update.LUNCHBREAKEND });
                con.myparameters.Add(new myParameters { ParameterName = "@RESTDAY1", mytype = SqlDbType.NVarChar, Value = update.RESTDAY1 });
                con.myparameters.Add(new myParameters { ParameterName = "@RESTDAY2", mytype = SqlDbType.NVarChar, Value = update.RESTDAY2 });
                con.myparameters.Add(new myParameters { ParameterName = "@RESTDAY3", mytype = SqlDbType.NVarChar, Value = update.RESTDAY3 });
                con.myparameters.Add(new myParameters { ParameterName = "@FBREAKTIMEMINUTES", mytype = SqlDbType.NVarChar, Value = update.FBREAKTIMEMINUTES });
                con.myparameters.Add(new myParameters { ParameterName = "@SBREAKTIMEMINUTES", mytype = SqlDbType.NVarChar, Value = update.SBREAKTIMEMINUTES });
                con.myparameters.Add(new myParameters { ParameterName = "@EBREAKTIMEMINUTES", mytype = SqlDbType.NVarChar, Value = update.EBREAKTIMEMINUTES });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENTID", mytype = SqlDbType.NVarChar, Value = update.CLIENTID });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENTNAME", mytype = SqlDbType.NVarChar, Value = update.CLIENTNAME });
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = update.PClientID });
                con.myparameters.Add(new myParameters { ParameterName = "@BREAKTYPE", mytype = SqlDbType.NVarChar, Value = update.BreakType });
                con.ExecuteNonQuery("sp_UpdateSchedV2");
                return "true";
            }
            catch (Exception e)
            {
                return "error" + e;
            }
        }

        [HttpPost]
        [Route("GetUpdateToken")]
        public List<tokenlist> getupdatetoken(tokenparams prms)
        {
            List<tokenlist> tl = new List<tokenlist>();
            DataTable dt = new DataTable();
            try
            {
                Connection con1 = new Connection();
                con1.myparameters.Add(new myParameters { ParameterName = "@Token", mytype = SqlDbType.NVarChar, Value = prms.Token });
                con1.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = prms.UserID });
                con1.myparameters.Add(new myParameters { ParameterName = "@PCLientID", mytype = SqlDbType.NVarChar, Value = prms.PClientID });
                con1.myparameters.Add(new myParameters { ParameterName = "@TokenAction", mytype = SqlDbType.NVarChar, Value = prms.TokenAction });
                dt = con1.GetDataTable("sp_AddGetDelToken");
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        tokenlist MS = new tokenlist();
                        MS.tokenID = dt.Rows[0]["TokenID"].ToString();
                        MS.message = "Success";
                        tl.Add(MS);
                    }
                    return tl;
                }
                else
                {
                    tokenlist MS = new tokenlist();
                    MS.tokenID = "";
                    MS.message = "Updated token";
                    tl.Add(MS);
                    return tl;
                }
            }
            catch (Exception e)
            {
                tokenlist MS = new tokenlist();
                MS.message = e.Message;
                tl.Add(MS);
                return tl;
            }

        }

        [HttpPost]
        [Route("SchedulerCredentialsEmail")]
        public string sendCredentialsEmail(schedulerCredEmail prms)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@repEmail", mytype = SqlDbType.NVarChar, Value = prms.repEmail });
                con.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.NVarChar, Value = prms.PClient_ID });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClientCode", mytype = SqlDbType.NVarChar, Value = prms.SubClientCode });
                DataTable DT = con.GetDataTable("sp_scheduler_CredentialsEmail");

                if (DT.Rows.Count > 0)
                {
                    var message = new MimeMessage();
                    message.From.Add(new MailboxAddress("Pulong Scheduler", "pulong@illimitado.com"));
                    message.To.Add(new MailboxAddress(DT.Rows[0]["Email"].ToString()));
                    //message.To.Add(new MailboxAddress("cibori@getbreathtaking.com"));
                    message.Subject = "Notifications";
                    var messBody = "<label>Welcome to Pulong Scheduler Admin portal. Thank you for using Scheduler.<br>" +
                        "Feel free to visit <a href='http://pulong.illimitado.com.s3-website-ap-southeast-1.amazonaws.com/#/'>http://pulong.illimitado.com</a> to login to your account.<br><br>" +
                        "Here are your account details:<br>" +
                        "<b>Company Name: </b>" + DT.Rows[0]["SubClientName"].ToString() + "<br>" +
                        "<b>Company Alias: </b>" + DT.Rows[0]["SubClientCode"].ToString() + "<br>" +
                        "<b>Username: </b>" + DT.Rows[0]["Username"].ToString() + "<br>" +
                        "<b>Temporary Password: </b>" + DT.Rows[0]["Password"].ToString() + "<br><br>" +
                        "Access <a href='http://pulong.illimitado.com.s3-website-ap-southeast-1.amazonaws.com/#/'>http://pulong.illimitado.com</a> anytime and anywhere with Chrome. Enjoy your account.</label>";
                    message.Body = new TextPart("html") { Text = messBody };

                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect("box1256.bluehost.com", 465, true);
                        client.Authenticate("pulong@illimitado.com", "PuL0ng@1230");
                        client.Send(message);
                        client.Disconnect(true);
                    }
                }
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("setSubClientInactive")]
        public string setsubclientinactive(setSubclientInactive prms)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.NVarChar, Value = prms.PClient_ID });
                con.myparameters.Add(new myParameters { ParameterName = "@SubClient_ID", mytype = SqlDbType.NVarChar, Value = prms.SubClient_ID });
                con.ExecuteNonQuery("sp_setSubClientInactive");
                return prms.message = "inactive subclient";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        #region Brandon Migrate Api for Pulong
        [HttpPost]
        [Route("SD_getbsegment")]
        public List<businessSegments> getBusinessSegment(Client client)
        {
            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = client.ClientID });
            dt = con.GetDataTable("sp_GetBusinessSegment");
            List<businessSegments> final = new List<businessSegments>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                businessSegments temp = new businessSegments();
                temp.businessSegment = dt.Rows[i]["SubSubject"].ToString();
                temp.rowID = dt.Rows[i]["RowID"].ToString();

                final.Add(temp);
            }
            return final;
        }

        [HttpPost]
        [Route("SD_getbUnit")]
        public List<businessUnits> businessUnits(Client client)
        {
            DataTable DeptIds = new DataTable();
            DeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("DeptIds") });
            for (int i = 0; i < client.Department.Length; i++)
            {
                DeptIds.Rows.Add(client.Department[i]);
            }
            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = client.ClientID });
            con.myparameters.Add(new myParameters { ParameterName = "@DeptIds", mytype = SqlDbType.Structured, Value = DeptIds });
            dt = con.GetDataTable("sp_GetBusinessUnit");
            List<businessUnits> final = new List<businessUnits>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                businessUnits temp = new businessUnits();
                temp.businessUnit = dt.Rows[i]["SubSubject"].ToString();
                temp.rowID = dt.Rows[i]["RowID"].ToString();

                final.Add(temp);
            }
            return final;
        }

        [HttpPost]
        [Route("SD_getEmployee")]
        public List<employees> getEmployee(Client client)
        {
            DataTable DeptIds = new DataTable();
            DeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("DeptIds") });
            for (int i = 0; i < client.Department.Length; i++)
            {
                DeptIds.Rows.Add(client.Department[i]);
            }
            DataTable subDeptIds = new DataTable();
            subDeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("subDeptIds") });
            for (int i = 0; i < client.SubDepartment.Length; i++)
            {
                subDeptIds.Rows.Add(client.SubDepartment[i]);
            }
            Connection con = new Connection();
            DataTable dt = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = client.ClientID });
            con.myparameters.Add(new myParameters { ParameterName = "@DeptIds", mytype = SqlDbType.Structured, Value = DeptIds });
            con.myparameters.Add(new myParameters { ParameterName = "@subDeptIds", mytype = SqlDbType.Structured, Value = subDeptIds });
            dt = con.GetDataTable("sp_GetAllEmpFullName");
            List<employees> final = new List<employees>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                employees temp = new employees();
                temp.empName = dt.Rows[i]["FullName"].ToString();
                temp.empUID = dt.Rows[i]["UserID"].ToString();

                final.Add(temp);
            }
            return final;
        }
        [HttpPost]
        [Route("saveUserInfo")]
        public string saveCreateUser(createUserInfo para)
        {
            try
            {
                DataTable emptype = new DataTable();
                emptype.Columns.AddRange(new DataColumn[1] { new DataColumn("EMPID") });
                for (int i = 0; i < para.usertype.Length; i++)
                {
                    emptype.Rows.Add(para.usertype[i]);
                }
                Connection con = new Connection();
                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@empName", mytype = SqlDbType.NVarChar, Value = para.empname });
                con.myparameters.Add(new myParameters { ParameterName = "@department", mytype = SqlDbType.NVarChar, Value = para.deptname });
                con.myparameters.Add(new myParameters { ParameterName = "@subDepartment", mytype = SqlDbType.NVarChar, Value = para.subdeptname });
                con.myparameters.Add(new myParameters { ParameterName = "@email", mytype = SqlDbType.NVarChar, Value = para.email });
                con.myparameters.Add(new myParameters { ParameterName = "@PhoneNum", mytype = SqlDbType.NVarChar, Value = para.phonenum });
                con.myparameters.Add(new myParameters { ParameterName = "@userType", mytype = SqlDbType.Structured, Value = emptype });
                con.myparameters.Add(new myParameters { ParameterName = "@userStatus", mytype = SqlDbType.NVarChar, Value = para.empstat });
                con.myparameters.Add(new myParameters { ParameterName = "@FName", mytype = SqlDbType.NVarChar, Value = para.empFName });
                con.myparameters.Add(new myParameters { ParameterName = "@MName", mytype = SqlDbType.NVarChar, Value = para.empMName });
                con.myparameters.Add(new myParameters { ParameterName = "@LName", mytype = SqlDbType.NVarChar, Value = para.empLName });
                con.myparameters.Add(new myParameters { ParameterName = "@editEmpID", mytype = SqlDbType.NVarChar, Value = para.editEmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = para.ClientID });
                con.ExecuteNonQuery("sp_SD_InsertCreateUser");
                return "success";
            }
            catch (Exception e)
            {
                return "error0o0" + para.empname + para.deptname + para.subdeptname + para.email + para.email + "0o0" + e;
            }
        }
        [HttpPost]
        [Route("CheckExistEmpID")]
        public string CheckExistEmpID(CheckEmpID eid)
        {
            try
            {
                Connection Connection = new Connection();

                Connection.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = eid.PClientID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = eid.EmpID });
                return Connection.ExecuteScalar("sp_SD_CheckExistEmpID").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }

        [HttpPost]
        [Route("CheckExistEmpIDPulong")]
        public string CheckExistEmpIDPulong(CheckEmpIDpul eid)
        {
            try
            {
                Connection Connection = new Connection();
                Connection.myparameters.Add(new myParameters { ParameterName = "@CLIENT_ID", mytype = SqlDbType.NVarChar, Value = eid.PClientID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@SUBCLIENT_ID", mytype = SqlDbType.NVarChar, Value = eid.SubClientID });
                Connection.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = eid.EmpID });
                return Connection.ExecuteScalar("sp_SP_validateUserID").ToString();
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("SD_saveUserInfoV2")]
        public string saveCreateUserV2(createUserInfoV2 para)
        {
            try
            {
                DataTable emptype = new DataTable();
                emptype.Columns.AddRange(new DataColumn[1] { new DataColumn("EMPID") });
                foreach (string item in para.usertype)
                {
                    emptype.Rows.Add(item);
                }
                Connection con = new Connection();
                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@empName", mytype = SqlDbType.NVarChar, Value = para.empname });
                con.myparameters.Add(new myParameters { ParameterName = "@department", mytype = SqlDbType.NVarChar, Value = para.deptname });
                con.myparameters.Add(new myParameters { ParameterName = "@subDepartment", mytype = SqlDbType.NVarChar, Value = para.subdeptname });
                con.myparameters.Add(new myParameters { ParameterName = "@email", mytype = SqlDbType.NVarChar, Value = para.email });
                con.myparameters.Add(new myParameters { ParameterName = "@PhoneNum", mytype = SqlDbType.NVarChar, Value = para.phonenum });
                con.myparameters.Add(new myParameters { ParameterName = "@userType", mytype = SqlDbType.Structured, Value = emptype });
                con.myparameters.Add(new myParameters { ParameterName = "@userStatus", mytype = SqlDbType.NVarChar, Value = para.empstat });
                con.myparameters.Add(new myParameters { ParameterName = "@FName", mytype = SqlDbType.NVarChar, Value = para.empFName });
                con.myparameters.Add(new myParameters { ParameterName = "@MName", mytype = SqlDbType.NVarChar, Value = para.empMName });
                con.myparameters.Add(new myParameters { ParameterName = "@LName", mytype = SqlDbType.NVarChar, Value = para.empLName });
                con.myparameters.Add(new myParameters { ParameterName = "@EmpID", mytype = SqlDbType.NVarChar, Value = para.EmpID });
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = para.ClientID });
                con.myparameters.Add(new myParameters { ParameterName = "@multiEdit", mytype = SqlDbType.NVarChar, Value = para.multiEdit });
                con.ExecuteNonQuery("sp_SD_InsertCreateUserV2");
                return "success";
            }
            catch (Exception e)
            {
                return "error0o0" + para.empname + para.deptname + para.subdeptname + para.email + para.email + "0o0" + e;
            }
        }
        [HttpPost]
        [Route("updateStatus")]
        public string updateStatus(updateStatusParam edit)
        {
            Connection con1 = new Connection();
            DataTable EMPID = new DataTable();
            EMPID.Columns.AddRange(new DataColumn[1] { new DataColumn("EMPID") });
            for (int i = 0; i < edit.uID.Length; i++)
            {
                EMPID.Rows.Add(edit.uID[i]);
            }
            con1.myparameters.Add(new myParameters { ParameterName = "@userID", mytype = SqlDbType.Structured, Value = EMPID });
            con1.myparameters.Add(new myParameters { ParameterName = "@status", mytype = SqlDbType.NVarChar, Value = edit.status });
            con1.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = edit.ClientID });
            con1.ExecuteNonQuery("sp_SD_UpdateUserStatus");
            return "true";
        }
        [HttpPost]
        [Route("getcreateempid")]
        public string getcreateempid()
        {
            Connection con1 = new Connection();
            return con1.ExecuteScalar("sp_SD_getEmpID");
        }
        [HttpPost]
        [Route("SD_SchedulerPortalSubjects")]
        public subjects SchedulerSubjects(Client client)
        {
            try
            {
                Connection con = new Connection();
                subjects subjects = new subjects();
                List<subjectlist> list = new List<subjectlist>();
                //DataSet ds = new DataSet();
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = client.ClientID });
                DataTable dt = con.GetDataTable("sp_SD_ShowSubjects");

                foreach (DataRow row in dt.Rows)
                {
                    subjectlist param = new subjectlist()
                    {
                        _SID = row["SID"].ToString(),
                        _Subject = row["Subject"].ToString(),
                        _Status = Convert.ToBoolean(row["Status"].ToString()),
                        _DefaultName = row["DefaultName"].ToString(),
                        _ModifiedBy = row["ModifiedBy"].ToString(),
                        _ModifiedDate = row["ModifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subjects.subjectlist = list;
                subjects._myreturn = "Success";
                return subjects;
            }
            catch (Exception e)
            {
                subjects subjects = new subjects();
                List<subjectlist> list = new List<subjectlist>();
                subjects.subjectlist = list;
                subjects._myreturn = e.ToString();
                return subjects;
            }
        }
        //Not sure kung ano ito same name pero may nakahide na parameter check ko na lang if magka problem
        //[HttpPost]
        //[Route("SD_SchedulerGetSubSubjects")] //Listahan ng SubSubjects sa labels, pasahan ng value 0 yung parameter para magdisplay lahat ng subsubjects else selected subsubjects lang lalabas
        //public subsubjectslistforlabel GetSubSubjects(subsubjectslistforlabel subject)
        //{
        //    subsubjectslistforlabel subsubjects = new subsubjectslistforlabel();
        //    try
        //    {
        //        Connection con = new Connection();
        //        List<subsubjectlistlabel> list = new List<subsubjectlistlabel>();
        //        //DataSet ds = new DataSet();
        //        //con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subject._SID });
        //        //con.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.NVarChar, Value = subject.ClientID });
        //        DataTable dt = con.GetDataTable("sp_SD_SelectShowSubSubjects");

        //        foreach (DataRow row in dt.Rows)
        //        {
        //            subsubjectlistlabel param = new subsubjectlistlabel()
        //            {
        //                _RowID = row["RowID"].ToString(),
        //                _SID = row["SID"].ToString(),
        //                _Subject = row["SubSubject"].ToString(),
        //                _Status = Convert.ToBoolean(row["Status"].ToString()),
        //                _ModifiedBy = row["ModifiedBy"].ToString(),
        //                _ModifiedDate = row["ModifiedDate"].ToString(),
        //                _Edit = false
        //            };
        //            list.Add(param);
        //        }
        //        subsubjects.subsubjectlist = list;
        //        subsubjects._myreturn = "Success";
        //        return subsubjects;
        //    }
        //    catch (Exception e)
        //    {
        //        //List<subsubjectlist> list = new List<subsubjectlist>();
        //        //subsubjects.subsubjectlist = list;
        //        subsubjects._myreturn = e.ToString();
        //        return subsubjects;
        //    }
        //}
        [HttpPost]
        [Route("SD_SchedulerPortalSubSubjectStatus")] //Toggle ng checkbox for SubSubject = Active/Inactive
        public string SubSubjectStatusMigrated(subsubjectlistlabel subsubjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._RowID });
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = subsubjectlist.ClientID });
                con.ExecuteNonQuery("sp_SD_ActiveInactiveSubSubjectList");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("SD_SchedulerGetSubSubjects")] //Listahan ng SubSubjects sa labels, pasahan ng value 0 yung parameter para magdisplay lahat ng subsubjects else selected subsubjects lang lalabas
        public subsubjects GetSubSubjects(subsubjectlistlabel subject)
        {
            subsubjects subsubjects = new subsubjects();
            try
            {
                Connection con = new Connection();
                List<subsubjectlist> list = new List<subsubjectlist>();
                //DataSet ds = new DataSet();
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subject._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.NVarChar, Value = subject.ClientID });
                DataTable dt = con.GetDataTable("sp_SD_SelectShowSubSubjects");

                foreach (DataRow row in dt.Rows)
                {
                    subsubjectlist param = new subsubjectlist()
                    {
                        _RowID = row["RowID"].ToString(),
                        _SID = row["SID"].ToString(),
                        _Subject = row["SubSubject"].ToString(),
                        _Status = Convert.ToBoolean(row["Status"].ToString()),
                        _ModifiedBy = row["ModifiedBy"].ToString(),
                        _ModifiedDate = row["ModifiedDate"].ToString(),
                        _Edit = false
                    };
                    list.Add(param);
                }
                subsubjects.subsubjectlist = list;
                subsubjects._myreturn = "Success";
                return subsubjects;
            }
            catch (Exception e)
            {
                //List<subsubjectlist> list = new List<subsubjectlist>();
                //subsubjects.subsubjectlist = list;
                subsubjects._myreturn = e.ToString();
                return subsubjects;
            }
        }
        [HttpPost]
        [Route("SD_SchedulerPortalUpdateSubject")] // insert and update ng Subject. pag pinasahan ng 0 yung @SUBJECTID magiinsert siya ng panibago else iuupdate niya. yung @DEFAULTNAME original na name nung Subject
        public string UpdateSubjectMigrated(subjectlistUpdate subjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectlist._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTNAME", mytype = SqlDbType.NVarChar, Value = subjectlist._Subject });
                con.myparameters.Add(new myParameters { ParameterName = "@DEFAULTNAME", mytype = SqlDbType.NVarChar, Value = subjectlist._DefaultName });
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = subjectlist.ClientID });
                con.ExecuteScalar("sp_SD_InsertUpdateSubject");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("SD_SchedulerPortalSubjectStatus")] //Toggle ng checkbox for Subject = Active/Inactive
        public string SubjectStatusMigrated(subjectlist subjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subjectlist._SID });
                con.ExecuteNonQuery("sp_SD_ActiveInactiveSubject");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("SD_SchedulerPortalUpdateSubSubject")] //Insert and Update ng SubSubjects, pasahan ng 0 yung @SUBSUBJECTID para mag insert ng panibagong subsubject else iuupdate niya yung selected subsubject
        public string UpdateSubSubjectMigrated(subsubjectlistlabel subsubjectlist)
        {
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._ModifiedBy });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._SID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTID", mytype = SqlDbType.NVarChar, Value = subsubjectlist._RowID });
                con.myparameters.Add(new myParameters { ParameterName = "@SUBSUBJECTNAME", mytype = SqlDbType.NVarChar, Value = subsubjectlist._Subject });
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = subsubjectlist.ClientID });
                con.ExecuteScalar("sp_SD_InsertUpdateSubSubjectList");
                return "Success";
            }
            catch (Exception e)
            {
                return e.ToString();
            }
        }
        [HttpPost]
        [Route("GetDDItems")]
        public List<MultiSelectItems> MultiSelectItems(MultiSelectItems MSI)
        {
            List<MultiSelectItems> ListReturned = new List<MultiSelectItems>();
            DataTable DT = new DataTable();
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@ITEMTYPE", mytype = SqlDbType.NVarChar, Value = MSI.ItemType });
                DT = con.GetDataTable("sp_SD_GetDDItems");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    MultiSelectItems MS = new MultiSelectItems();
                    MS.ItemID = DT.Rows[i]["ItemID"].ToString();
                    MS.ItemName = DT.Rows[i]["ItemName"].ToString();
                    MS.Message = "Success";
                    ListReturned.Add(MS);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                MultiSelectItems MS = new MultiSelectItems();
                MS.Message = e.Message;
                ListReturned.Add(MS);
                return ListReturned;
            }
        }
        [HttpPost]
        [Route("GetRegion")]
        public LocationList Locations(Location Loc)
        {
            LocationList final2 = new LocationList();
            List<Location> final = new List<Location>();
            DataTable DT = new DataTable();
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@COUNTRY", mytype = SqlDbType.VarChar, Value = Loc.Loc });
                DT = con.GetDataTable("sp_Vue_GetRegion");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Location temp = new Location();
                    temp.Loc = DT.Rows[i]["regDesc"].ToString();
                    final.Add(temp);
                }
                final2.Locations = final;
            }
            catch { }
            return final2;
        }
        [HttpPost]
        [Route("SD_getAllProvinces")]
        public List<MultiSelectItems> Provinces(regItems reg)
        {
            try
            {
                Connection con = new Connection();
                DataTable dt = new DataTable();
                DataTable selregion = new DataTable();
                selregion.Columns.AddRange(new DataColumn[1] { new DataColumn("Region") });
                for (int i = 0; i < reg.regsel.Length; i++)
                {
                    selregion.Rows.Add(reg.regsel[i]);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@region", mytype = SqlDbType.Structured, Value = selregion });
                dt = con.GetDataTable("sp_Scheduler_MCGetAllProvince");
                List<MultiSelectItems> ListReturned = new List<MultiSelectItems>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MultiSelectItems MS = new MultiSelectItems();
                    MS.ItemID = dt.Rows[i]["ProvID"].ToString();
                    MS.ItemName = dt.Rows[i]["ItemName"].ToString();
                    MS.Message = "Success";
                    ListReturned.Add(MS);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                List<MultiSelectItems> ListReturned = new List<MultiSelectItems>();
                MultiSelectItems MS = new MultiSelectItems();
                MS.Message = e.ToString();
                return ListReturned;
            }
        }
        [HttpPost]
        [Route("getAllCity1")]
        public List<MultiSelectItems> AllCities1(province prov)
        {
            try
            {
                Connection con = new Connection();
                DataTable dt = new DataTable();
                DataTable selprov = new DataTable();
                selprov.Columns.AddRange(new DataColumn[1] { new DataColumn("ProvinceID") });
                for (int i = 0; i < prov.provinceID.Length; i++)
                {
                    selprov.Rows.Add(prov.provinceID[i]);
                }
                con.myparameters.Add(new myParameters { ParameterName = "@ProvinceTable", mytype = SqlDbType.Structured, Value = selprov });
                dt = con.GetDataTable("sp_Scheduler_MCGetAllCity");
                List<MultiSelectItems> ListReturned = new List<MultiSelectItems>();
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    MultiSelectItems MS = new MultiSelectItems();
                    MS.ItemName = dt.Rows[i]["ItemName"].ToString();
                    MS.Message = "Success";
                    ListReturned.Add(MS);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                List<MultiSelectItems> ListReturned = new List<MultiSelectItems>();
                MultiSelectItems MS = new MultiSelectItems();
                MS.Message = e.ToString();
                ListReturned.Add(MS);
                return ListReturned;
            }
        }
        [HttpPost]
        [Route("getAllProducts")]
        public List<MultiSelectItems> Products(MCItem para)
        {
            List<MultiSelectItems> ListReturned = new List<MultiSelectItems>();
            DataTable DT = new DataTable();
            try
            {
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = para.ClientID });
                DT = con.GetDataTable("sp_SD_GetAllProducts");
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    MultiSelectItems MS = new MultiSelectItems();
                    MS.ItemName = DT.Rows[i]["ProductName"].ToString();
                    MS.Message = "Success";
                    ListReturned.Add(MS);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                MultiSelectItems MS = new MultiSelectItems();
                MS.Message = e.Message;
                ListReturned.Add(MS);
                return ListReturned;
            }
        }
        [HttpPost]
        [Route("saveMasterControl")]
        public string saveMasterControl(MasterControlItems mcitem)
        {
            Connection con = new Connection();
            try
            {
                //con.ExecuteNonQuery("sp_SD_MasterControlDisable");
                con.myparameters.Add(new myParameters { ParameterName = "@CID", mytype = SqlDbType.VarChar, Value = mcitem.id });
                con.myparameters.Add(new myParameters { ParameterName = "@Emp1", mytype = SqlDbType.VarChar, Value = mcitem.EmployeeCount1 });
                con.myparameters.Add(new myParameters { ParameterName = "@Emp2", mytype = SqlDbType.VarChar, Value = mcitem.EmployeeCount2 });
                con.ExecuteNonQuery("sp_SD_saveMasterControlEmp");
                return "success";
            }
            catch (Exception ex)
            {
                return "Error: " + ex;
            }
        }
        [HttpPost]
        [Route("SD_updateMasterControlItem")]
        public string updateMasterControl(MCItem para)
        {
            try
            {
                DataTable TermData = new DataTable();
                TermData.Columns.AddRange(new DataColumn[2] { new DataColumn("ControlID"), new DataColumn("EmpLevel") });
                for (int i = 0; i < para.itemarr.Length; i++)
                {
                    TermData.Rows.Add(para.controlID, para.itemarr[i]);
                }
                Connection con = new Connection();
                con.myparameters.Clear();
                con.myparameters.Add(new myParameters { ParameterName = "@CID", mytype = SqlDbType.VarChar, Value = para.controlID });
                con.myparameters.Add(new myParameters { ParameterName = "@ItemList", mytype = SqlDbType.Structured, Value = TermData });
                con.myparameters.Add(new myParameters { ParameterName = "@target", mytype = SqlDbType.VarChar, Value = para.target });
                con.myparameters.Add(new myParameters { ParameterName = "@PClient_ID", mytype = SqlDbType.VarChar, Value = para.ClientID });
                con.ExecuteNonQuery("sp_SD_UpdateMasterControl");
                return "success";
            }
            catch (Exception e)
            {
                return "error0o0" + para.itemarr.Length + "0o0" + e;
            }
        }
        [HttpPost]
        [Route("SD_getSavedDetails")]
        public List<GetDetails> getsavedindustry(ctrlID cid)
        {
            ctrlID param = new ctrlID();
            param = cid;
            if (param.column == "industry")
            {
                try
                {
                    DataTable dt = new DataTable();
                    List<GetDetails> lstind = new List<GetDetails>();
                    Connection con = new Connection();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@controlID", mytype = SqlDbType.VarChar, Value = cid.ctid });
                    dt = con.GetDataTable("sp_SD_MCGetIndustry");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GetDetails temp = new GetDetails();
                        temp.ID = dt.Rows[i][0].ToString();
                        temp.resDetail = dt.Rows[i][1].ToString();
                        lstind.Add(temp);
                    }

                    return lstind;
                }
                catch
                {
                    return null;
                }
            }
            else if (param.column == "region")
            {
                try
                {
                    DataTable dt = new DataTable();
                    List<GetDetails> lstind = new List<GetDetails>();
                    Connection con = new Connection();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@controlID", mytype = SqlDbType.VarChar, Value = cid.ctid });
                    dt = con.GetDataTable("sp_SD_MCGetRegion");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GetDetails temp = new GetDetails();
                        temp.ID = dt.Rows[i][0].ToString();
                        temp.resDetail = dt.Rows[i][1].ToString();
                        lstind.Add(temp);
                    }

                    return lstind;
                }
                catch
                {
                    return null;
                }
            }
            else if (param.column == "province")
            {
                try
                {
                    DataTable dt = new DataTable();
                    List<GetDetails> lstind = new List<GetDetails>();
                    Connection con = new Connection();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@controlID", mytype = SqlDbType.VarChar, Value = cid.ctid });
                    dt = con.GetDataTable("sp_SD_MCGetProvince");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GetDetails temp = new GetDetails();
                        temp.ID = dt.Rows[i][0].ToString();
                        temp.resDetail = dt.Rows[i][1].ToString();
                        lstind.Add(temp);
                    }

                    return lstind;
                }
                catch
                {
                    return null;
                }
            }
            else if (param.column == "city")
            {
                try
                {
                    DataTable dt = new DataTable();
                    List<GetDetails> lstind = new List<GetDetails>();
                    Connection con = new Connection();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@controlID", mytype = SqlDbType.VarChar, Value = cid.ctid });
                    dt = con.GetDataTable("sp_SD_MCGetCity");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GetDetails temp = new GetDetails();
                        temp.ID = dt.Rows[i][0].ToString();
                        temp.resDetail = dt.Rows[i][1].ToString();
                        lstind.Add(temp);
                    }

                    return lstind;
                }
                catch
                {
                    return null;
                }
            }
            else if (param.column == "product")
            {
                try
                {
                    DataTable dt = new DataTable();
                    List<GetDetails> lstind = new List<GetDetails>();
                    Connection con = new Connection();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@controlID", mytype = SqlDbType.VarChar, Value = cid.ctid });
                    dt = con.GetDataTable("sp_SD_MCGetProduct");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GetDetails temp = new GetDetails();
                        temp.ID = dt.Rows[i][0].ToString();
                        temp.resDetail = dt.Rows[i][1].ToString();
                        lstind.Add(temp);
                    }

                    return lstind;
                }
                catch
                {
                    return null;
                }
            }
            else if (param.column == "agent")
            {
                try
                {
                    DataTable dt = new DataTable();
                    List<GetDetails> lstind = new List<GetDetails>();
                    Connection con = new Connection();
                    con.myparameters.Clear();
                    con.myparameters.Add(new myParameters { ParameterName = "@controlID", mytype = SqlDbType.VarChar, Value = cid.ctid });
                    dt = con.GetDataTable("sp_SD_MCGetAgent");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        GetDetails temp = new GetDetails();
                        temp.ID = dt.Rows[i][0].ToString();
                        temp.resDetail = dt.Rows[i][1].ToString();
                        lstind.Add(temp);
                    }

                    return lstind;
                }
                catch
                {
                    return null;
                }
            }
            else
            {
                return null;
            }
        }
        [HttpPost]
        [Route("disableMC")]
        public string disableMC()
        {
            Connection con = new Connection();
            try
            {
                con.ExecuteNonQuery("sp_SD_MasterControlDisable");
                return "success";
            }
            catch (Exception ex)
            {
                return "Error: " + ex;
            }
        }
        [HttpPost]
        [Route("DeleteMCItem")]
        public string DeleteMCItem(DeleteMC mcitem)
        {
            Connection con = new Connection();
            try
            {
                //con.ExecuteNonQuery("sp_SD_MasterControlDisable");
                con.myparameters.Add(new myParameters { ParameterName = "@CID", mytype = SqlDbType.VarChar, Value = mcitem.ctlID });
                con.ExecuteNonQuery("sp_SD_DeleteMasterControl");
                return "success";
            }
            catch (Exception ex)
            {
                return "Error: " + ex.ToString();
            }
        }
        [HttpPost]
        [Route("getMaxMCID")]
        public string getMaxMCID()
        {
            Connection con = new Connection();
            try
            {
                return con.GetDataTable("sp_SD_getMasterControlID").Rows[0][0].ToString();
            }
            catch (Exception ex)
            {
                return "Error: " + ex.ToString();
            }
        }
        [HttpPost]
        [Route("getUserPass")]
        public List<upass> retrieveUserPass(employeeID edit)
        {
            List<upass> eu = new List<upass>();
            DataTable aa = new DataTable();
            try
            {
                Connection con1 = new Connection();
                con1.myparameters.Add(new myParameters { ParameterName = "@userID", mytype = SqlDbType.NVarChar, Value = edit.uID });
                con1.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = edit.ClientID });
                aa = con1.GetDataTable("sp_getCurrentPassword");
                for (int i = 0; i < aa.Rows.Count; i++)
                {
                    upass MS = new upass();
                    MS.userpass = aa.Rows[0]["UserPass"].ToString();
                    MS.message = "Success";
                    eu.Add(MS);
                }
                return eu;
            }
            catch (Exception e)
            {
                upass MS = new upass();
                MS.message = e.Message;
                eu.Add(MS);
                return eu;
            }

        }
        [HttpPost]
        [Route("updatePassword")]
        public string updateCurPassword(updatePass edit)
        {
            try
            {
                Connection con1 = new Connection();
                con1.myparameters.Clear();
                con1.myparameters.Add(new myParameters { ParameterName = "@userID", mytype = SqlDbType.NVarChar, Value = edit.uID });
                con1.myparameters.Add(new myParameters { ParameterName = "@NEWPASS", mytype = SqlDbType.NVarChar, Value = edit.newpass });
                con1.myparameters.Add(new myParameters { ParameterName = "@CLIENTID", mytype = SqlDbType.NVarChar, Value = edit.ClientID });
                con1.ExecuteNonQuery("sp_updateCurrentPassword");
                return "true";
            }
            catch (Exception e)
            {
                return "error0o0" + edit.uID + edit.newpass + edit.ClientID + e;
            }
        }
        static DataTable userinfo = new DataTable();
        [HttpPost]
        [Route("getUserInfo")]
        public List<info> retrieveUserInfo(userinfo search)
        {
            Connection con = new Connection();
            DataTable dt = new DataTable();
            DataTable EMPID = new DataTable();
            EMPID.Columns.AddRange(new DataColumn[1] { new DataColumn("EMPID") });
            for (int i = 0; i < search.EID.Length; i++)
            {
                EMPID.Rows.Add(search.EID[i]);
            }
            DataTable DeptIds = new DataTable();
            DeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("DeptIds") });
            for (int i = 0; i < search.Department.Length; i++)
            {
                DeptIds.Rows.Add(search.Department[i]);
            }
            DataTable subDeptIds = new DataTable();
            subDeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("subDeptIds") });
            for (int i = 0; i < search.SubDepartment.Length; i++)
            {
                subDeptIds.Rows.Add(search.SubDepartment[i]);
            }
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.Structured, Value = EMPID });
            con.myparameters.Add(new myParameters { ParameterName = "@DeptIds", mytype = SqlDbType.Structured, Value = DeptIds });
            con.myparameters.Add(new myParameters { ParameterName = "@subDeptIds", mytype = SqlDbType.Structured, Value = subDeptIds });
            con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = search.ClientID });
            dt = con.GetDataTable("sp_SD_SearchEmployeeInfo");
            List<info> final = new List<info>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                info temp = new info();
                temp.empUserID = dt.Rows[i]["UserID"].ToString();
                temp.Company = dt.Rows[i]["Company"].ToString();
                temp.empName = dt.Rows[i]["EmployeeName"].ToString();
                temp.deptname = dt.Rows[i]["Department"].ToString();
                temp.subdeptname = dt.Rows[i]["SubDepartment"].ToString();
                temp.empEmail = dt.Rows[i]["Email"].ToString();
                temp.empPnum = dt.Rows[i]["Phone_Number"].ToString();
                temp.empUtype = dt.Rows[i]["User_Type"].ToString();
                temp.empStat = dt.Rows[i]["Status"].ToString();
                temp.FName = dt.Rows[i]["FName"].ToString();
                temp.MName = dt.Rows[i]["MName"].ToString();
                temp.LName = dt.Rows[i]["LName"].ToString();
                temp.userEmailID = dt.Rows[i]["UserEmailID"].ToString();
                temp.DeptID = dt.Rows[i]["DepartmentID"].ToString();
                temp.SubDeptID = dt.Rows[i]["SubDepartmentID"].ToString();
                final.Add(temp);
            }
            return final;
        }
        [HttpPost]
        [Route("getUserInfoV2")]
        public List<info> retrieveUserInfoV2(userinfoV2 search)
        {
            Connection con = new Connection();
            DataTable dt = new DataTable();
            DataTable EMPID = new DataTable();
            EMPID.Columns.AddRange(new DataColumn[1] { new DataColumn("EMPID") });
            for (int i = 0; i < search.EID.Length; i++)
            {
                EMPID.Rows.Add(search.EID[i]);
            }
            DataTable DeptIds = new DataTable();
            DeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("DeptIds") });
            for (int i = 0; i < search.Department.Length; i++)
            {
                DeptIds.Rows.Add(search.Department[i]);
            }
            DataTable subDeptIds = new DataTable();
            subDeptIds.Columns.AddRange(new DataColumn[1] { new DataColumn("subDeptIds") });
            for (int i = 0; i < search.SubDepartment.Length; i++)
            {
                subDeptIds.Rows.Add(search.SubDepartment[i]);
            }
            con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.Structured, Value = EMPID });
            con.myparameters.Add(new myParameters { ParameterName = "@DeptIds", mytype = SqlDbType.Structured, Value = DeptIds });
            con.myparameters.Add(new myParameters { ParameterName = "@subDeptIds", mytype = SqlDbType.Structured, Value = subDeptIds });
            con.myparameters.Add(new myParameters { ParameterName = "@Status", mytype = SqlDbType.NVarChar, Value = search.Status });
            con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = search.ClientID });
            dt = con.GetDataTable("sp_SD_SearchEmployeeInfoV2");
            List<info> final = new List<info>();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                info temp = new info();
                temp.empUserID = dt.Rows[i]["UserID"].ToString();
                temp.Company = dt.Rows[i]["Company"].ToString();
                temp.empName = dt.Rows[i]["EmployeeName"].ToString();
                temp.deptname = dt.Rows[i]["Department"].ToString();
                temp.subdeptname = dt.Rows[i]["SubDepartment"].ToString();
                temp.empEmail = dt.Rows[i]["Email"].ToString();
                temp.empPnum = dt.Rows[i]["Phone_Number"].ToString();
                temp.empUtype = dt.Rows[i]["User_Type"].ToString();
                temp.empStat = dt.Rows[i]["Status"].ToString();
                temp.FName = dt.Rows[i]["FName"].ToString();
                temp.MName = dt.Rows[i]["MName"].ToString();
                temp.LName = dt.Rows[i]["LName"].ToString();
                temp.userEmailID = dt.Rows[i]["UserEmailID"].ToString();
                temp.DeptID = dt.Rows[i]["DepartmentID"].ToString();
                temp.SubDeptID = dt.Rows[i]["SubDepartmentID"].ToString();
                final.Add(temp);
            }
            return final;
        }
        [HttpPost]
        [Route("updateSched")]
        public string updateSched(updateField update)
        {
            try
            {
                DataTable empIds = new DataTable();
                empIds.Columns.AddRange(new DataColumn[1] { new DataColumn("EmpID") });
                for (int i = 0; i < update.ID.Length; i++)
                {
                    empIds.Rows.Add(update.ID[i]);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@SCHEDULE", mytype = SqlDbType.NVarChar, Value = update.SCHEDULE });
                con.myparameters.Add(new myParameters { ParameterName = "@START", mytype = SqlDbType.NVarChar, Value = update.START });
                con.myparameters.Add(new myParameters { ParameterName = "@FIRSTBREAK", mytype = SqlDbType.NVarChar, Value = update.FIRSTBREAK });
                con.myparameters.Add(new myParameters { ParameterName = "@LUNCH", mytype = SqlDbType.NVarChar, Value = update.LUNCH });
                con.myparameters.Add(new myParameters { ParameterName = "@SECONDBREAK", mytype = SqlDbType.NVarChar, Value = update.SECONDBREAK });
                con.myparameters.Add(new myParameters { ParameterName = "@ENDBREAK", mytype = SqlDbType.NVarChar, Value = update.ENDBREAK });
                con.myparameters.Add(new myParameters { ParameterName = "@ENDOFSHIFT", mytype = SqlDbType.NVarChar, Value = update.ENDOFSHIFT });
                con.myparameters.Add(new myParameters { ParameterName = "@serID", mytype = SqlDbType.Structured, Value = empIds });
                con.myparameters.Add(new myParameters { ParameterName = "@FIRSTBREAKDSP", mytype = SqlDbType.NVarChar, Value = update.FIRSTBREAKDSP });
                con.myparameters.Add(new myParameters { ParameterName = "@SECONDBREAKDSP", mytype = SqlDbType.NVarChar, Value = update.SECONDBREAKDSP });
                con.myparameters.Add(new myParameters { ParameterName = "@ENDBREAKDSP", mytype = SqlDbType.NVarChar, Value = update.ENDBREAKDSP });
                con.myparameters.Add(new myParameters { ParameterName = "@LUNCHBREAKEND", mytype = SqlDbType.NVarChar, Value = update.LUNCHBREAKEND });
                con.myparameters.Add(new myParameters { ParameterName = "@RESTDAY1", mytype = SqlDbType.NVarChar, Value = update.RESTDAY1 });
                con.myparameters.Add(new myParameters { ParameterName = "@RESTDAY2", mytype = SqlDbType.NVarChar, Value = update.RESTDAY2 });
                con.myparameters.Add(new myParameters { ParameterName = "@RESTDAY3", mytype = SqlDbType.NVarChar, Value = update.RESTDAY3 });
                con.myparameters.Add(new myParameters { ParameterName = "@FBREAKTIMEMINUTES", mytype = SqlDbType.NVarChar, Value = update.FBREAKTIMEMINUTES });
                con.myparameters.Add(new myParameters { ParameterName = "@SBREAKTIMEMINUTES", mytype = SqlDbType.NVarChar, Value = update.SBREAKTIMEMINUTES });
                con.myparameters.Add(new myParameters { ParameterName = "@EBREAKTIMEMINUTES", mytype = SqlDbType.NVarChar, Value = update.EBREAKTIMEMINUTES });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENTID", mytype = SqlDbType.NVarChar, Value = update.CLIENTID });
                con.myparameters.Add(new myParameters { ParameterName = "@CLIENTNAME", mytype = SqlDbType.NVarChar, Value = update.CLIENTNAME });
                con.myparameters.Add(new myParameters { ParameterName = "@PCLient_ID", mytype = SqlDbType.NVarChar, Value = update.PClientID });
                con.myparameters.Add(new myParameters { ParameterName = "@BREAKTYPE", mytype = SqlDbType.NVarChar, Value = update.BreakType });
                con.ExecuteNonQuery("sp_UpdateSched");
                return "true";
            }
            catch (Exception e)
            {
                return "error" + e;
            }
        }
        #endregion
        ///Webservice for mobile 2 Migrated version by Brandon 8/13/2019
        #region Brandon Migrate Api for Pulong Mobile 
        //Retrieve Demo  Schedule
        [HttpPost]
        [Route("GetDemoSchedule")]
        public List<Appointment> GetDemoSchedule(Appointment GetApp)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@USEREMAIL", mytype = SqlDbType.NVarChar, Value = GetApp.EMAIL });
            con.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = GetApp.STARTDATE });
            con.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = GetApp.ENDDATE });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetDemoScheduleV2");
            List<Appointment> ListReturned = new List<Appointment>();

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Appointment GetAppAvail = new Appointment();
                    //Company Appointment Information 
                    GetAppAvail.CompanyID = DT.Rows[i][0].ToString();
                    GetAppAvail.STARTDATE = DT.Rows[i][1].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i][1]).ToString("yyyy-MM-dd");
                    GetAppAvail.STARTTIME = DT.Rows[i][2].ToString();
                    GetAppAvail.COMPNAME = DT.Rows[i][3].ToString();
                    GetAppAvail.CITY = DT.Rows[i][4].ToString();
                    GetAppAvail.SERIESID = DT.Rows[i][5].ToString();
                    GetAppAvail.TABLEFIELD = DT.Rows[i][6].ToString();
                    GetAppAvail.BARANGAY = DT.Rows[i][5].ToString();
                    GetAppAvail.VILLAGE = DT.Rows[i][6].ToString();
                    ListReturned.Add(GetAppAvail);
                }
            }
            else
            {
                Appointment GetAppAvail = new Appointment();
                //Company Appointment Information 
                GetAppAvail.CompanyID = string.Empty;
                GetAppAvail.STARTDATE = string.Empty;
                GetAppAvail.STARTTIME = string.Empty;
                GetAppAvail.COMPNAME = string.Empty;
                GetAppAvail.CITY = string.Empty;
                GetAppAvail.SERIESID = string.Empty;
                GetAppAvail.TABLEFIELD = string.Empty;
                GetAppAvail.BARANGAY = string.Empty;
                GetAppAvail.VILLAGE = string.Empty;
                ListReturned.Add(GetAppAvail);
            }

            return ListReturned;
        }

        //Update Appointment Demo schedule  -Status column in(tbl_EmpAppointmentSchedule,tbl_CompanyAppointment)

        [HttpPost]
        [Route("GetCompanySchedule")]
        public List<Appointment> GetCompanySchedule(Appointment GetApp)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@UserID", mytype = SqlDbType.NVarChar, Value = GetApp.USERID });//added
            con.myparameters.Add(new myParameters { ParameterName = "@SubClientID", mytype = SqlDbType.NVarChar, Value = GetApp.SubClientID });//added
            con.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = GetApp.EMPSTARTDATE });
            con.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = GetApp.EMPENDDATE });
            con.myparameters.Add(new myParameters { ParameterName = "@COMPANYID", mytype = SqlDbType.NVarChar, Value = GetApp.SERIESID });
            con.myparameters.Add(new myParameters { ParameterName = "@STATS", mytype = SqlDbType.NVarChar, Value = GetApp.STATUS });
            con.myparameters.Add(new myParameters { ParameterName = "@PCLIENTID", mytype = SqlDbType.NVarChar, Value = GetApp.PClient_ID });

            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetCompanyScheduleV3");
            List<Appointment> ListReturned = new List<Appointment>();
            string StatSelect = GetApp.STATUS;


            if (StatSelect == "SingleData")
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {

                    Appointment GetAppAvail = new Appointment();
                    //Company Appointment Information       
                    GetAppAvail.CompanyID = DT.Rows[i][0].ToString();

                    GetAppAvail.APPOINTMENTFOR = DT.Rows[i][1].ToString();
                    GetAppAvail.APPONTMENTREASON = DT.Rows[i][2].ToString();
                    GetAppAvail.COMPNAME = DT.Rows[i][3].ToString();
                    GetAppAvail.INDUSTRY = DT.Rows[i][4].ToString();
                    GetAppAvail.NUMOFEMPLOYEES = DT.Rows[i][5].ToString();

                    GetAppAvail.HRIS = DT.Rows[i][6].ToString();
                    GetAppAvail.ZIPCODE = DT.Rows[i][7].ToString();
                    GetAppAvail.UNITBLDG = DT.Rows[i][8].ToString();
                    GetAppAvail.STREET = DT.Rows[i][9].ToString();

                    GetAppAvail.CREATEDBY = DT.Rows[i][10].ToString();
                    GetAppAvail.CREATEDDATE = DT.Rows[i][11].ToString();

                    GetAppAvail.STATUS = DT.Rows[i][12].ToString();

                    if (DT.Rows[i][12].ToString() == "Pending")
                    {
                        GetAppAvail.BGCOLOR = "<i class=\"fas fa-question-circle\"></i>";
                    }
                    else if (DT.Rows[i][12].ToString() == "Denied")
                    {
                        GetAppAvail.BGCOLOR = "<i class=\"fas fa-times-circle\"></i>";
                    }
                    else if (DT.Rows[i][12].ToString() == "Approved")
                    {
                        GetAppAvail.BGCOLOR = "<i class=\"fas fa-check - circle\"></i>";
                    }

                    GetAppAvail.SERIESID = DT.Rows[i][13].ToString();

                    GetAppAvail.STARTDATE = DT.Rows[i][14].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i][14]).ToString("yyyy-MM-dd");
                    GetAppAvail.STARTTIME = DT.Rows[i][15].ToString();
                    GetAppAvail.ENDDATE = DT.Rows[i][16].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i][16]).ToString("yyyy-MM-dd");
                    GetAppAvail.ENDTIME = DT.Rows[i][17].ToString();

                    GetAppAvail.REQUESTDATE = DT.Rows[i][18].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i][18]).ToString("yyyy-MM-dd");


                    GetAppAvail.BARANGAY = DT.Rows[i][19].ToString();
                    GetAppAvail.VILLAGE = DT.Rows[i][20].ToString();
                    GetAppAvail.APPOINTEMPLOYEE = DT.Rows[i][21].ToString();
                    ListReturned.Add(GetAppAvail);

                }

            }
            else if (StatSelect == "MultipleData")
            {

                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Appointment GetAppAvail = new Appointment();
                    //Company Appointment Information       
                    GetAppAvail.COMPNAME = DT.Rows[i][0].ToString();
                    GetAppAvail.STATUS = DT.Rows[i][1].ToString();
                    GetAppAvail.SERIESID = DT.Rows[i][2].ToString();
                    GetAppAvail.STARTDATE = DT.Rows[i][3].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i][3]).ToString("yyyy-MM-dd");
                    if (DT.Rows[i][1].ToString() == "Pending")
                    {
                        GetAppAvail.BGCOLOR = "background-color: #ffff80";
                    }
                    else if (DT.Rows[i][1].ToString() == "Denied")
                    {
                        GetAppAvail.BGCOLOR = "background-color:#ff6666";
                    }
                    else if (DT.Rows[i][1].ToString() == "Approved")
                    {
                        GetAppAvail.BGCOLOR = "background-color: #80ff80";
                    }
                    GetAppAvail.CompanyID = DT.Rows[i][4].ToString();
                    GetAppAvail.STARTTIME = DT.Rows[i][5].ToString();
                    ListReturned.Add(GetAppAvail);
                }
            }
            else if (StatSelect == "ALL")
            {

                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Appointment GetAppAvail = new Appointment();
                    //Company Appointment Information      
                    GetAppAvail.CompanyID = DT.Rows[i][0].ToString();
                    GetAppAvail.COMPNAME = DT.Rows[i][1].ToString();
                    GetAppAvail.REQUESTDATE = DT.Rows[i][2].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i][2]).ToString("yyyy-MM-dd");
                    GetAppAvail.STATUS = DT.Rows[i][3].ToString();
                    GetAppAvail.SERIESID = DT.Rows[i][4].ToString();
                    ListReturned.Add(GetAppAvail);
                }
            }
            else
            {
                //none data
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("GetIndustry")]

        public List<ZipCode> GetIndustry(ZipCode GetIndustry)
        {
            Connection con = new Connection();
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetIndustry");
            List<ZipCode> ListReturned = new List<ZipCode>();

            for (int i = 0; i < DT.Rows.Count; i++)
            {
                ZipCode GetInd = new ZipCode();
                GetInd.IndustryID = DT.Rows[i][0].ToString();
                GetInd.IndustryInfo = DT.Rows[i][1].ToString();
                ListReturned.Add(GetInd);

            }

            return ListReturned;
        }
        [HttpPost]
        [Route("GetZipCode")]

        public List<ZipCode> GetZipCode(ZipCode GetZip)
        {
            Connection con = new Connection();
            DataTable DT = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@ZIPCODE", mytype = SqlDbType.NVarChar, Value = GetZip.ZIPCODE });

            DT = con.GetDataTable("sp_GetZipCode");
            List<ZipCode> ListReturned = new List<ZipCode>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                ZipCode GetZ = new ZipCode();
                GetZ.REGION = DT.Rows[i][0].ToString();
                GetZ.PROVINCE = DT.Rows[i][1].ToString();
                GetZ.CITY = DT.Rows[i][2].ToString();
                ListReturned.Add(GetZ);
            }
            return ListReturned;
        }
        [HttpPost]
        [Route("GetBrgy")]

        public List<BrgyList> GetBrgy(BrgyList GetZip)
        {
            Connection con = new Connection();
            DataTable DT = new DataTable();
            con.myparameters.Add(new myParameters { ParameterName = "@ZIPCODE", mytype = SqlDbType.NVarChar, Value = GetZip.ZIPCODE });

            DT = con.GetDataTable("sp_GetBrgyCode");
            List<BrgyList> ListReturned = new List<BrgyList>();
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                BrgyList GetZ = new BrgyList();
                GetZ.BRGYID = DT.Rows[i]["brgyCode"].ToString();
                GetZ.BRGYNAME = DT.Rows[i]["brgyDesc"].ToString();

                ListReturned.Add(GetZ);
            }
            return ListReturned;
        }
        [Route("AddRequestAppointmentv2")]
        public string AddRequestAppointmentv2(Appointment AddApp)
        {
            try
            {
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@APPOINTMENTFOR", mytype = SqlDbType.NVarChar, Value = AddApp.APPOINTMENTFOR });
                Con.myparameters.Add(new myParameters { ParameterName = "@APPOINTMENTTYPE", mytype = SqlDbType.NVarChar, Value = AddApp.APPONTMENTREASON });
                Con.myparameters.Add(new myParameters { ParameterName = "@COMPANYNAME", mytype = SqlDbType.NVarChar, Value = AddApp.COMPNAME });
                Con.myparameters.Add(new myParameters { ParameterName = "@INDUSTRY", mytype = SqlDbType.NVarChar, Value = AddApp.INDUSTRY });
                Con.myparameters.Add(new myParameters { ParameterName = "@NOEMPLOYEES", mytype = SqlDbType.NVarChar, Value = AddApp.NUMOFEMPLOYEES });
                Con.myparameters.Add(new myParameters { ParameterName = "@HRIS", mytype = SqlDbType.NVarChar, Value = AddApp.HRIS });
                Con.myparameters.Add(new myParameters { ParameterName = "@ZIPCODE", mytype = SqlDbType.NVarChar, Value = AddApp.ZIPCODE });
                Con.myparameters.Add(new myParameters { ParameterName = "@UNITBLDG", mytype = SqlDbType.NVarChar, Value = AddApp.UNITBLDG });
                Con.myparameters.Add(new myParameters { ParameterName = "@STREET", mytype = SqlDbType.NVarChar, Value = AddApp.STREET });
                Con.myparameters.Add(new myParameters { ParameterName = "@VILLAGE", mytype = SqlDbType.NVarChar, Value = AddApp.VILLAGE });
                Con.myparameters.Add(new myParameters { ParameterName = "@BRGY", mytype = SqlDbType.NVarChar, Value = AddApp.BARANGAY });
                Con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = AddApp.USERID });
                Con.myparameters.Add(new myParameters { ParameterName = "@CITY", mytype = SqlDbType.NVarChar, Value = AddApp.CITY });
                Con.ExecuteNonQuery("sp_AddRequestAppointmentv2");



                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        [HttpPost]
        [Route("AddRequestCompanyContact")]
        //Company Contact 
        public string AddRequestCompanyContact(Appointment AddApp)
        {
            try
            {
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@CONTACTNAME", mytype = SqlDbType.NVarChar, Value = AddApp.CONTACTNAME });
                Con.myparameters.Add(new myParameters { ParameterName = "@TITLE", mytype = SqlDbType.NVarChar, Value = AddApp.TITLE });
                Con.myparameters.Add(new myParameters { ParameterName = "@CONTACTNUMBER", mytype = SqlDbType.NVarChar, Value = AddApp.CONTACTNUMBER });
                Con.myparameters.Add(new myParameters { ParameterName = "@EMAILADDRESS", mytype = SqlDbType.NVarChar, Value = AddApp.EMAIL });
                Con.myparameters.Add(new myParameters { ParameterName = "@PCLIENTID", mytype = SqlDbType.NVarChar, Value = AddApp.PClient_ID });
                Con.ExecuteNonQuery("sp_AddRequestCompanyContact");

                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        //Retrieve Demo  Schedule
        [HttpPost]
        [Route("GetBookDemo")]
        public List<Appointment> GetBookDemo(Appointment GetApp)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = GetApp.PClient_ID });
            con.myparameters.Add(new myParameters { ParameterName = "@SETDATE", mytype = SqlDbType.NVarChar, Value = GetApp.STARTDATE });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetBookDemo4");
            List<Appointment> ListReturned = new List<Appointment>();
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Appointment GetAppAvail = new Appointment();
                    //Company Book Demo     
                    //string iDate = DT.Rows[i][1].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i][1]).ToString("yyyy-MM-dd");
                    //DateTime oDate = Convert.ToDateTime(iDate);
                    //GetAppAvail.STARTDATE = oDate.ToString("MMM dd,ddd");
                    GetAppAvail.STARTDATE = DT.Rows[i][1].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i][1]).ToString("yyyy-MM-dd");
                    GetAppAvail.STARTTIME = DT.Rows[i][2].ToString();
                    GetAppAvail.ENDTIME = DT.Rows[i][3].ToString();
                    GetAppAvail.UID = DT.Rows[i][4].ToString();
                    GetAppAvail.USERID = DT.Rows[i][5].ToString();
                    ListReturned.Add(GetAppAvail);
                }
            }
            else
            {
                Appointment GetAppAvail = new Appointment();
                //Company Book Demo             
                GetAppAvail.STARTDATE = string.Empty;
                GetAppAvail.STARTTIME = string.Empty;
                GetAppAvail.ENDTIME = string.Empty;
                GetAppAvail.USERID = string.Empty;
                ListReturned.Add(GetAppAvail);
            }

            return ListReturned;
        }
        //Retrieve  Google Api
        [HttpPost]
        [Route("GetGoogleApi")]
        public List<Appointment> GetGoogleApi(Appointment GetApp)
        {
            Connection con = new Connection();
            //con.myparameters.Add(new myParameters { ParameterName = "@COMPANYID", mytype = SqlDbType.NVarChar, Value = GetApp.GAppiCompanyID });
            //con.myparameters.Add(new myParameters { ParameterName = "@ALLAVAILID", mytype = SqlDbType.NVarChar, Value = GetApp.ALLVAILID });


            con.myparameters.Add(new myParameters { ParameterName = "@COMPANYID", mytype = SqlDbType.NVarChar, Value = GetApp.GAppiCompanyID });
            con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = GetApp.USERID });
            con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = GetApp.STARTDATE });
            con.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = GetApp.STARTTIME });
            con.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = GetApp.ENDTIME });

            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetGoogleApiV3");

            List<Appointment> ListReturned = new List<Appointment>();
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Appointment Api = new Appointment();
                    //Company Book Demo                   
                    Api.ORIGIN = DT.Rows[i][0].ToString();
                    Api.LOCATION = DT.Rows[i][1].ToString();
                    ListReturned.Add(Api);
                }
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("GetAPIDuration")]
        public List<Appointment> GetAPIDuration(Appointment GetApp)
        {

            Connection Con = new Connection();
            //Con.myparameters.Add(new myParameters { ParameterName = "@DURAINTRAFFIC", mytype = SqlDbType.NVarChar, Value = GetApp.DURATION });
            //Con.myparameters.Add(new myParameters { ParameterName = "@COMPANYID", mytype = SqlDbType.NVarChar, Value = GetApp.CompanyID });
            //Con.myparameters.Add(new myParameters { ParameterName = "@ALLAVAILID", mytype = SqlDbType.NVarChar, Value = GetApp.ALLVAILID });

            Con.myparameters.Add(new myParameters { ParameterName = "@DURAINTRAFFIC", mytype = SqlDbType.NVarChar, Value = GetApp.DURATION });
            Con.myparameters.Add(new myParameters { ParameterName = "@COMPANYID", mytype = SqlDbType.NVarChar, Value = GetApp.CompanyID });
            Con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = GetApp.USERID });
            Con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = GetApp.STARTDATE });
            Con.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = GetApp.STARTTIME });
            Con.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = GetApp.ENDTIME });

            DataTable DT = new DataTable();
            DT = Con.GetDataTable("sp_GetAPIDuration");

            List<Appointment> ListReturned = new List<Appointment>();
            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Appointment Api = new Appointment();

                    Api.AVAILSTARTTIME = DT.Rows[i][0].ToString();
                    Api.AVAILENDTIME = DT.Rows[i][1].ToString();
                    Api.AVAILSTATUS = DT.Rows[i][2].ToString();
                    ListReturned.Add(Api);
                }
            }
            return ListReturned;

        }
        //Retrieve Demo  Schedule
        [HttpPost]
        [Route("MDCGetBookDemo")]
        public List<MDCBOOKDEMO> MDCGetBookDemo(MDCBOOKDEMOSEND GetApp)
        {
            List<MDCBOOKDEMO> ListReturned = new List<MDCBOOKDEMO>();
            try
            {
                DataTable DataCorr = new DataTable();
                DataCorr.Clear();
                string all = "";
                if (DataCorr.Columns.Count < 1)
                {
                    DataCorr.Columns.Add("companyID", typeof(string));
                    DataCorr.Columns.Add("userID", typeof(string));
                    DataCorr.Columns.Add("SchedDate", typeof(string));
                    DataCorr.Columns.Add("StartTime", typeof(string));
                    DataCorr.Columns.Add("EndTime", typeof(string));
                }
                for (int oo = 0; oo < GetApp.BOOKDATA.Count; oo++)
                {
                    DataRow colof = DataCorr.NewRow();
                    colof["companyID"] = GetApp.BOOKDATA[oo].companyID.ToString();
                    colof["userID"] = GetApp.BOOKDATA[oo].userID.ToString();
                    colof["SchedDate"] = GetApp.BOOKDATA[oo].SchedDate.ToString();
                    colof["StartTime"] = GetApp.BOOKDATA[oo].StartTime.ToString();
                    colof["EndTime"] = GetApp.BOOKDATA[oo].EndTime.ToString();
                    all += GetApp.BOOKDATA[oo].companyID.ToString() + "-";
                    all += GetApp.BOOKDATA[oo].userID.ToString() + "-";
                    all += GetApp.BOOKDATA[oo].SchedDate.ToString() + "-";
                    all += GetApp.BOOKDATA[oo].StartTime.ToString() + "-";
                    all += GetApp.BOOKDATA[oo].EndTime.ToString() + "-";
                    DataCorr.Rows.Add(colof);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@BOOK", mytype = SqlDbType.Structured, Value = DataCorr });
                DataTable DT = con.GetDataTable("sp_MDCGetBookDemo");
                foreach (DataRow row in DT.Rows)
                {
                    MDCBOOKDEMO GetAppAvail = new MDCBOOKDEMO()
                    {
                        companyID = row["companyID"].ToString(),
                        SchedDate = row["SchedDate"].ToString(),
                        StartTime = row["StartTime"].ToString(),
                        EndTime = row["EndTime"].ToString(),
                        userID = row["userID"].ToString()
                    };
                    ListReturned.Add(GetAppAvail);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                return ListReturned;
            }
        }
        //Add Demo  Schedule
        [HttpPost]
        [Route("MDCAddBookDemo")]
        public List<MDCBOOKDEMO> MDCAddBookDemo(MDCBOOKDEMOSEND GetApp)
        {
            //DataTable DTSI = new DataTable();
            //DTSI.Columns.AddRange(new DataColumn[1] { new DataColumn("StartingID") });
            //for (int i = 0; i < SF.StartingIDs.Length; i++)
            //{
            //    DTSI.Rows.Add(SF.StartingIDs[i]);
            //}
            List<MDCBOOKDEMO> ListReturned = new List<MDCBOOKDEMO>();
            try
            {
                DataTable DataCorr = new DataTable();
                DataCorr.Clear();
                string all = "";
                if (DataCorr.Columns.Count < 1)
                {
                    DataCorr.Columns.Add("companyID", typeof(string));
                    DataCorr.Columns.Add("userID", typeof(string));
                    DataCorr.Columns.Add("SchedDate", typeof(string));
                    DataCorr.Columns.Add("StartTime", typeof(string));
                    DataCorr.Columns.Add("EndTime", typeof(string));
                }
                for (int oo = 0; oo < GetApp.BOOKDATA.Count; oo++)
                {
                    DataRow colof = DataCorr.NewRow();
                    colof["companyID"] = GetApp.BOOKDATA[oo].companyID.ToString();
                    colof["userID"] = GetApp.BOOKDATA[oo].userID.ToString();
                    colof["SchedDate"] = GetApp.BOOKDATA[oo].SchedDate.ToString();
                    colof["StartTime"] = GetApp.BOOKDATA[oo].StartTime.ToString();
                    colof["EndTime"] = GetApp.BOOKDATA[oo].EndTime.ToString();
                    all += GetApp.BOOKDATA[oo].companyID.ToString() + "-";
                    all += GetApp.BOOKDATA[oo].userID.ToString() + "-";
                    all += GetApp.BOOKDATA[oo].SchedDate.ToString() + "-";
                    all += GetApp.BOOKDATA[oo].StartTime.ToString() + "-";
                    all += GetApp.BOOKDATA[oo].EndTime.ToString() + "-";
                    DataCorr.Rows.Add(colof);
                }
                Connection con = new Connection();
                con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.VarChar, Value = GetApp.PClientID });
                con.myparameters.Add(new myParameters { ParameterName = "@BOOK", mytype = SqlDbType.Structured, Value = DataCorr });
                con.myparameters.Add(new myParameters { ParameterName = "@StartTime", mytype = SqlDbType.DateTime, Value = GetApp.StartTime });
                con.myparameters.Add(new myParameters { ParameterName = "@EndTime", mytype = SqlDbType.DateTime, Value = GetApp.EndTime });
                con.myparameters.Add(new myParameters { ParameterName = "@CompanyID", mytype = SqlDbType.VarChar, Value = GetApp.CompanyID });
                DataTable dt2 = con.GetDataTable("sp_MDCAddBookDemo2");
                foreach (DataRow row in dt2.Rows)
                {
                    MDCBOOKDEMO UserTokenID = new MDCBOOKDEMO()
                    {
                        TokenID = row["TokenID"].ToString(),
                        userID = row["UserID"].ToString()
                    };
                    ListReturned.Add(UserTokenID);
                }
                return ListReturned;
            }
            catch (Exception e)
            {
                return ListReturned;
            }
        }
        //Update Company Start time and End time 
        [HttpPost]
        [Route("UpdateAppointSchedule")]
        public string UpdateAppointSchedule(Appointment UpdateApp)
        {
            try
            {

                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = UpdateApp.PClient_ID });
                Con.myparameters.Add(new myParameters { ParameterName = "@SERIESID", mytype = SqlDbType.NVarChar, Value = UpdateApp.SERIESID });
                Con.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = UpdateApp.STARTDATE });
                Con.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = UpdateApp.STARTTIME });
                Con.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = UpdateApp.ENDDATE });
                Con.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = UpdateApp.ENDTIME });
                Con.myparameters.Add(new myParameters { ParameterName = "@REQUESTTO", mytype = SqlDbType.NVarChar, Value = UpdateApp.REQUESTTO });
                // 8=====D
                Con.ExecuteNonQuery("sp_UpdateAppointSchedulev2");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        //[HttpPost]
        //[Route("MDCAddBookDemo")]
        //public string MDCAddBookDemo(MDCBOOKDEMOSEND GetApp)
        //{
        //    //DataTable DTSI = new DataTable();
        //    //DTSI.Columns.AddRange(new DataColumn[1] { new DataColumn("StartingID") });
        //    //for (int i = 0; i < SF.StartingIDs.Length; i++)
        //    //{
        //    //    DTSI.Rows.Add(SF.StartingIDs[i]);
        //    //}
        //    List<MDCBOOKDEMO> ListReturned = new List<MDCBOOKDEMO>();
        //    try
        //    {
        //        DataTable DataCorr = new DataTable();
        //        DataCorr.Clear();
        //        string all = "";
        //        if (DataCorr.Columns.Count < 1)
        //        {
        //            DataCorr.Columns.Add("companyID", typeof(string));
        //            DataCorr.Columns.Add("userID", typeof(string));
        //            DataCorr.Columns.Add("SchedDate", typeof(string));
        //            DataCorr.Columns.Add("StartTime", typeof(string));
        //            DataCorr.Columns.Add("EndTime", typeof(string));
        //        }
        //        for (int oo = 0; oo < GetApp.BOOKDATA.Count; oo++)
        //        {
        //            DataRow colof = DataCorr.NewRow();
        //            colof["companyID"] = GetApp.BOOKDATA[oo].companyID.ToString();
        //            colof["userID"] = GetApp.BOOKDATA[oo].userID.ToString();
        //            colof["SchedDate"] = GetApp.BOOKDATA[oo].SchedDate.ToString();
        //            colof["StartTime"] = GetApp.BOOKDATA[oo].StartTime.ToString();
        //            colof["EndTime"] = GetApp.BOOKDATA[oo].EndTime.ToString();
        //            all += GetApp.BOOKDATA[oo].companyID.ToString() + "-";
        //            all += GetApp.BOOKDATA[oo].userID.ToString() + "-";
        //            all += GetApp.BOOKDATA[oo].SchedDate.ToString() + "-";
        //            all += GetApp.BOOKDATA[oo].StartTime.ToString() + "-";
        //            all += GetApp.BOOKDATA[oo].EndTime.ToString() + "-";
        //            DataCorr.Rows.Add(colof);
        //        }
        //        Connection con = new Connection();
        //        con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.VarChar, Value = GetApp.PClientID });
        //        con.myparameters.Add(new myParameters { ParameterName = "@BOOK", mytype = SqlDbType.Structured, Value = DataCorr });
        //        con.myparameters.Add(new myParameters { ParameterName = "@StartTime", mytype = SqlDbType.DateTime, Value = GetApp.StartTime });
        //        con.myparameters.Add(new myParameters { ParameterName = "@EndTime", mytype = SqlDbType.DateTime, Value = GetApp.EndTime });
        //        con.myparameters.Add(new myParameters { ParameterName = "@CompanyID", mytype = SqlDbType.VarChar, Value = GetApp.CompanyID });
        //        var dr = con.ExecuteScalar("sp_MDCAddBookDemo2");

        //        return dr.ToString();
        //    }
        //    catch (Exception e)
        //    {
        //        return e.ToString();
        //    }
        //}
        //Update Company Start time and End time 
        //[HttpPost]
        //[Route("UpdateAppointSchedule")]
        //public string UpdateAppointSchedule(Appointment UpdateApp)
        //{
        //    try
        //    {

        //        Connection Con = new Connection();
        //        Con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = UpdateApp.PClient_ID });
        //        Con.myparameters.Add(new myParameters { ParameterName = "@SERIESID", mytype = SqlDbType.NVarChar, Value = UpdateApp.SERIESID });
        //        Con.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = UpdateApp.STARTDATE });
        //        Con.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = UpdateApp.STARTTIME });
        //        Con.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = UpdateApp.ENDDATE });
        //        Con.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = UpdateApp.ENDTIME });
        //        // 8=====D
        //        Con.ExecuteNonQuery("sp_UpdateAppointSchedulev2");
        //        return "success";
        //    }
        //    catch (Exception e)
        //    {
        //        return "failed" + e;
        //    }
        //}
        [HttpPost]
        [Route("AddEmailNotification")]
        public List<Appointment> AddEmailNotification(Appointment Notif)
        {

            Connection Con = new Connection();
            Con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = Notif.PClient_ID });
            Con.myparameters.Add(new myParameters { ParameterName = "@USERID", mytype = SqlDbType.NVarChar, Value = Notif.USERID });
            Con.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = Notif.STARTTIME });
            Con.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = Notif.ENDTIME });
            Con.myparameters.Add(new myParameters { ParameterName = "@SCHEDDATE", mytype = SqlDbType.NVarChar, Value = Notif.STARTDATE });
            Con.myparameters.Add(new myParameters { ParameterName = "@COMPANYID", mytype = SqlDbType.NVarChar, Value = Notif.CompanyID });

            DataTable DT = new DataTable();
            DT = Con.GetDataTable("sp_AddEmailNotificationv2");

            List<Appointment> ListReturned = new List<Appointment>();
            if (DT.Rows.Count > 0)
            {

                Appointment Api = new Appointment();
                Api.EMAIL = DT.Rows[0][0].ToString();
                Api.COMPNAME = DT.Rows[0][1].ToString();
                Api.LOCATION = DT.Rows[0][2].ToString();
                ListReturned.Add(Api);

                string iDate = Convert.ToDateTime(Notif.STARTDATE).ToString("yyyy-MM-dd");
                DateTime oDate = Convert.ToDateTime(iDate);
                string SCHEDDATE = oDate.ToString("MMM dd,ddd");
                DateTime timeValue = Convert.ToDateTime(Notif.ENDTIME);
                //  Console.WriteLine();

                try
                {

                    var message = new MimeMessage();
                    message.From.Add(new MailboxAddress("notifications@illimitado.com"));
                    message.To.Add(new MailboxAddress("" + Api.EMAIL + ""));
                    message.Subject = "Schedule Demo " + Api.COMPNAME;
                    message.Body = new TextPart("plain")
                    {
                        Text = "SCHEDULE DEMO FOR: " + SCHEDDATE + " at " + Api.COMPNAME + " From " + Notif.STARTTIME + " to " + timeValue.ToString("hh:mm tt")

                    };

                    using (var client = new MailKit.Net.Smtp.SmtpClient())
                    {
                        client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                        client.Connect("mail.illimitado.com", 465, true);
                        client.Authenticate("notifications@illimitado.com", "Not1f@1230");
                        client.Send(message);
                        client.Disconnect(true);
                    }


                }
                catch (Exception e)
                {

                }

                //send mail 
                //    string UMailUser = "payroll.noreply@illimitado.com";
                //    string emailto = Api.EMAIL;
                //    string UMailPass = "LimitLess!1@";
                //    using (MailMessage mm = new MailMessage(UMailUser, emailto))
                //    {
                //        MailAddress aliasmail = new MailAddress("payroll.noreply@illimitado.com", "Pulong Scheduler");
                //        MailAddress aliasreplymail = new MailAddress("payroll.noreply@illimitado.com");
                //        mm.From = aliasmail;
                //        mm.Subject = "Schedule Demo " + Api.COMPNAME ;
                //        mm.Body ="SCHEDULE DEMO FOR: " + SCHEDDATE+ " at " + Api.COMPNAME + " From " + Notif.STARTTIME + " to "+ timeValue.ToString("hh:mm tt");
                //        mm.IsBodyHtml = false;
                //        SmtpClient smtp = new SmtpClient();
                //        smtp.Host = "box1256.bluehost.com";
                //        smtp.EnableSsl = true;
                //        NetworkCredential NetworkCred = new NetworkCredential(UMailUser, UMailPass);
                //        smtp.UseDefaultCredentials = true;
                //        smtp.Credentials = NetworkCred;
                //        smtp.Port = 587;
                //        smtp.Send(mm);
                //    }
            }


            return ListReturned;

        }
        //GetCompany Information
        [HttpPost]
        [Route("GetCompanyInformation")]
        public List<Appointment> GetCompanyInformation(Appointment GetApp)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@COMPANYID", mytype = SqlDbType.NVarChar, Value = GetApp.CompanyID });
            con.myparameters.Add(new myParameters { ParameterName = "@PCLIENTID", mytype = SqlDbType.NVarChar, Value = GetApp.PClient_ID });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetCompanyInformation");
            List<Appointment> ListReturned = new List<Appointment>();

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Appointment GetAppAvail = new Appointment();
                    //Company Appointment Information      
                    GetAppAvail.COMPNAME = DT.Rows[i][0].ToString();
                    GetAppAvail.SERIESID = DT.Rows[i][1].ToString();
                    GetAppAvail.CompanyID = DT.Rows[i][2].ToString();
                    GetAppAvail.APPOINTMENTFOR = DT.Rows[i][3].ToString();
                    GetAppAvail.APPONTMENTREASON = DT.Rows[i][4].ToString();
                    GetAppAvail.STARTDATE = DT.Rows[i][5].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i][5]).ToString("yyyy-MM-dd");
                    GetAppAvail.STARTTIME = DT.Rows[i][6].ToString();
                    GetAppAvail.ENDDATE = DT.Rows[i][7].ToString() == "" ? null : Convert.ToDateTime(DT.Rows[i][7]).ToString("yyyy-MM-dd");
                    GetAppAvail.ENDTIME = DT.Rows[i][8].ToString();
                    GetAppAvail.MUNICIPALITY = DT.Rows[i][9].ToString();
                    GetAppAvail.CITY = DT.Rows[i][10].ToString();
                    GetAppAvail.REGION = DT.Rows[i][11].ToString();
                    GetAppAvail.PROVINCE = DT.Rows[i][12].ToString();

                    GetAppAvail.STATUS = DT.Rows[i][13].ToString();
                    GetAppAvail.INDUSTRY = DT.Rows[i][14].ToString();
                    GetAppAvail.NUMOFEMPLOYEES = DT.Rows[i][15].ToString();
                    GetAppAvail.HRIS = DT.Rows[i][16].ToString();
                    GetAppAvail.ZIPCODE = DT.Rows[i][17].ToString();
                    GetAppAvail.UNITBLDG = DT.Rows[i][18].ToString();
                    GetAppAvail.STREET = DT.Rows[i][19].ToString();
                    GetAppAvail.BARANGAY = DT.Rows[i][20].ToString();
                    GetAppAvail.VILLAGE = DT.Rows[i][21].ToString();


                    ListReturned.Add(GetAppAvail);
                }
            }
            else
            {
                Appointment GetAppAvail = new Appointment();
                //Company Appointment Information      
                GetAppAvail.COMPNAME = string.Empty;
                GetAppAvail.SERIESID = string.Empty;
                GetAppAvail.CompanyID = string.Empty;
                GetAppAvail.APPOINTMENTFOR = string.Empty;
                GetAppAvail.APPONTMENTREASON = string.Empty;
                GetAppAvail.STARTDATE = string.Empty;
                GetAppAvail.STARTTIME = string.Empty;
                GetAppAvail.ENDDATE = string.Empty;
                GetAppAvail.ENDTIME = string.Empty;
                GetAppAvail.MUNICIPALITY = string.Empty;
                GetAppAvail.CITY = string.Empty;
                GetAppAvail.REGION = string.Empty;
                GetAppAvail.PROVINCE = string.Empty;

                GetAppAvail.STATUS = string.Empty;
                GetAppAvail.INDUSTRY = string.Empty;
                GetAppAvail.NUMOFEMPLOYEES = string.Empty;
                GetAppAvail.HRIS = string.Empty;
                GetAppAvail.ZIPCODE = string.Empty;
                GetAppAvail.UNITBLDG = string.Empty;
                GetAppAvail.STREET = string.Empty;
                GetAppAvail.BARANGAY = string.Empty;
                GetAppAvail.VILLAGE = string.Empty;

                ListReturned.Add(GetAppAvail);
            }
            return ListReturned;
        }
        //Get Company Contact
        [HttpPost]
        [Route("GetCompanyContact")]
        public List<Appointment> GetCompanyContact(Appointment GetApp)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = GetApp.PClient_ID });
            con.myparameters.Add(new myParameters { ParameterName = "@COMPANYID", mytype = SqlDbType.NVarChar, Value = GetApp.CompanyID });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetCompanyContactv2");
            List<Appointment> ListReturned = new List<Appointment>();

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Appointment GetAppAvail = new Appointment();
                    //Company Appointment Information      
                    GetAppAvail.CompanyID = DT.Rows[i][0].ToString();
                    GetAppAvail.CONTACTNAME = DT.Rows[i][1].ToString();
                    GetAppAvail.TITLE = DT.Rows[i][2].ToString();
                    GetAppAvail.CONTACTNUMBER = DT.Rows[i][3].ToString();
                    GetAppAvail.EMAIL = DT.Rows[i][4].ToString();
                    GetAppAvail.UID = DT.Rows[i][5].ToString();
                    ListReturned.Add(GetAppAvail);
                }
            }
            else
            {
                Appointment GetAppAvail = new Appointment();
                //Company Appointment Information 
                GetAppAvail.CompanyID = string.Empty;
                GetAppAvail.CONTACTNAME = string.Empty;
                GetAppAvail.TITLE = string.Empty;
                GetAppAvail.CONTACTNUMBER = string.Empty;
                GetAppAvail.EMAIL = string.Empty;
                GetAppAvail.UID = string.Empty;
                ListReturned.Add(GetAppAvail);
            }

            return ListReturned;
        }
        //update company appointment schedule
        [HttpPost]
        [Route("UpdateAppointmentStatus")]
        public string UpdateAppointmentStatus(Appointment UpdateApp)
        {
            try
            {

                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = UpdateApp.PClient_ID });
                Con.myparameters.Add(new myParameters { ParameterName = "@SERIESID", mytype = SqlDbType.NVarChar, Value = UpdateApp.SERIESID });
                Con.myparameters.Add(new myParameters { ParameterName = "@STATUS", mytype = SqlDbType.NVarChar, Value = UpdateApp.STATUS });

                Con.ExecuteNonQuery("sp_UpdateAppointmentStatusv2");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        [Route("CompanyInformationRecordv2")]
        public string CompanyInformationRecordv2(Appointment AddApp)
        {
            try
            {
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@SERIESID", mytype = SqlDbType.NVarChar, Value = AddApp.SERIESID });
                Con.myparameters.Add(new myParameters { ParameterName = "@APPOINTMENTFOR", mytype = SqlDbType.NVarChar, Value = AddApp.APPOINTMENTFOR });
                Con.myparameters.Add(new myParameters { ParameterName = "@APPOINTMENTREASON", mytype = SqlDbType.NVarChar, Value = AddApp.APPONTMENTREASON });
                Con.myparameters.Add(new myParameters { ParameterName = "@COMPANYNAME", mytype = SqlDbType.NVarChar, Value = AddApp.COMPNAME });
                Con.myparameters.Add(new myParameters { ParameterName = "@INDUSTRY", mytype = SqlDbType.NVarChar, Value = AddApp.INDUSTRY });
                Con.myparameters.Add(new myParameters { ParameterName = "@NUMEMPLOYEE", mytype = SqlDbType.NVarChar, Value = AddApp.NUMOFEMPLOYEES });
                Con.myparameters.Add(new myParameters { ParameterName = "@HRIS", mytype = SqlDbType.NVarChar, Value = AddApp.HRIS });
                Con.myparameters.Add(new myParameters { ParameterName = "@ZIPCODE", mytype = SqlDbType.NVarChar, Value = AddApp.ZIPCODE });
                Con.myparameters.Add(new myParameters { ParameterName = "@UNITBLDG", mytype = SqlDbType.NVarChar, Value = AddApp.UNITBLDG });
                Con.myparameters.Add(new myParameters { ParameterName = "@STREET", mytype = SqlDbType.NVarChar, Value = AddApp.STREET });
                Con.myparameters.Add(new myParameters { ParameterName = "@VILLAGE", mytype = SqlDbType.NVarChar, Value = AddApp.VILLAGE });
                Con.myparameters.Add(new myParameters { ParameterName = "@USERMAILID", mytype = SqlDbType.NVarChar, Value = AddApp.USERID });
                Con.myparameters.Add(new myParameters { ParameterName = "@BRGY", mytype = SqlDbType.NVarChar, Value = AddApp.BARANGAY });
                Con.myparameters.Add(new myParameters { ParameterName = "@CITY", mytype = SqlDbType.NVarChar, Value = AddApp.CITY });
                Con.ExecuteNonQuery("sp_CompanyInformationRecordv3");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        [HttpPost]
        [Route("CompanyContactRecord")]
        public string CompanyContactInformation(Appointment AddApp)
        {
            try
            {
                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@SERIESID", mytype = SqlDbType.NVarChar, Value = AddApp.SERIESID });
                Con.myparameters.Add(new myParameters { ParameterName = "@UID", mytype = SqlDbType.NVarChar, Value = AddApp.UID });
                Con.myparameters.Add(new myParameters { ParameterName = "@STATS", mytype = SqlDbType.NVarChar, Value = AddApp.STATUS });
                Con.myparameters.Add(new myParameters { ParameterName = "@CONTACTPERSON", mytype = SqlDbType.NVarChar, Value = AddApp.CONTACTNAME });
                Con.myparameters.Add(new myParameters { ParameterName = "@TITLE", mytype = SqlDbType.NVarChar, Value = AddApp.TITLE });
                Con.myparameters.Add(new myParameters { ParameterName = "@CONTACTNUMBER", mytype = SqlDbType.NVarChar, Value = AddApp.CONTACTNUMBER });
                Con.myparameters.Add(new myParameters { ParameterName = "@EMAILADD", mytype = SqlDbType.NVarChar, Value = AddApp.EMAIL });
                Con.myparameters.Add(new myParameters { ParameterName = "@PCLIENTID", mytype = SqlDbType.NVarChar, Value = AddApp.PClient_ID });
                Con.ExecuteNonQuery("sp_CompanyContactRecordv2");
                return "success";
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        //Retrieve Company Conctact Record
        [HttpPost]
        [Route("GetContactInformation")]
        public List<Appointment> GetContactInformation(Appointment GetApp)
        {
            Connection con = new Connection();
            con.myparameters.Add(new myParameters { ParameterName = "@PCLIENTID", mytype = SqlDbType.NVarChar, Value = GetApp.PClient_ID });
            con.myparameters.Add(new myParameters { ParameterName = "@UID", mytype = SqlDbType.NVarChar, Value = GetApp.UID });
            DataTable DT = new DataTable();
            DT = con.GetDataTable("sp_GetContactInformationv2");
            List<Appointment> ListReturned = new List<Appointment>();

            if (DT.Rows.Count > 0)
            {
                for (int i = 0; i < DT.Rows.Count; i++)
                {
                    Appointment GetAppAvail = new Appointment();
                    //Company Appointment Information    
                    GetAppAvail.COMPNAME = DT.Rows[i][0].ToString();
                    GetAppAvail.CompanyID = DT.Rows[i][1].ToString();
                    GetAppAvail.CONTACTNAME = DT.Rows[i][2].ToString();
                    GetAppAvail.TITLE = DT.Rows[i][3].ToString();
                    GetAppAvail.CONTACTNUMBER = DT.Rows[i][4].ToString();
                    GetAppAvail.EMAIL = DT.Rows[i][5].ToString();
                    GetAppAvail.UID = DT.Rows[i][6].ToString();
                    ListReturned.Add(GetAppAvail);
                }
            }
            else
            {
                Appointment GetAppAvail = new Appointment();
                //Company Appointment Information    
                GetAppAvail.COMPNAME = string.Empty;
                GetAppAvail.CompanyID = string.Empty;
                GetAppAvail.CONTACTNAME = string.Empty;
                GetAppAvail.TITLE = string.Empty;
                GetAppAvail.CONTACTNUMBER = string.Empty;
                GetAppAvail.EMAIL = string.Empty;
                GetAppAvail.UID = string.Empty;
                ListReturned.Add(GetAppAvail);
            }

            return ListReturned;
        }
        [HttpPost]
        [Route("AddBookDemoAvaailable")]
        public string AddBookDemoAvaailable(Appointment AvailableDemo)
        {
            try
            {

                Connection Con = new Connection();
                Con.myparameters.Add(new myParameters { ParameterName = "@PClientID", mytype = SqlDbType.NVarChar, Value = AvailableDemo.PClient_ID });
                Con.myparameters.Add(new myParameters { ParameterName = "@SELID", mytype = SqlDbType.NVarChar, Value = AvailableDemo.USERID });
                Con.myparameters.Add(new myParameters { ParameterName = "@STARTDATE", mytype = SqlDbType.NVarChar, Value = AvailableDemo.STARTDATE });
                Con.myparameters.Add(new myParameters { ParameterName = "@ENDDATE", mytype = SqlDbType.NVarChar, Value = AvailableDemo.ENDDATE });
                Con.myparameters.Add(new myParameters { ParameterName = "@STARTTIME", mytype = SqlDbType.NVarChar, Value = AvailableDemo.STARTTIME });
                Con.myparameters.Add(new myParameters { ParameterName = "@ENDTIME", mytype = SqlDbType.NVarChar, Value = AvailableDemo.ENDTIME });
                Con.myparameters.Add(new myParameters { ParameterName = "@COMPANYID", mytype = SqlDbType.NVarChar, Value = AvailableDemo.CompanyID });
                Con.myparameters.Add(new myParameters { ParameterName = "@DURATION", mytype = SqlDbType.NVarChar, Value = "" });
                Con.myparameters.Add(new myParameters { ParameterName = "@DISTANCE", mytype = SqlDbType.NVarChar, Value = "" });

                AvailableDemo.COUNTNOTIFY = Con.ExecuteScalar("sp_AddBookDemov2");
                return AvailableDemo.COUNTNOTIFY;
            }
            catch (Exception e)
            {
                return "failed" + e;
            }
        }
        //[HttpPost]
        //[Route("DHRDelAddTokenMobilePulong")]
        //public string DHRDelAddToken(Token T)
        //{
        //    GetDatabase GDB = new GetDatabase();
        //    GDB.ClientName = T.CN;
        //    SelfieRegistration2Controller.GetDB2(GDB);
        //    Connection Con = new Connection();
        //    Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = T.EMPID });
        //    Con.myparameters.Add(new myParameters { ParameterName = "@TOKENID", mytype = SqlDbType.NVarChar, Value = T.TOKENID });
        //    Con.ExecuteNonQuery("sp_UpdateTokenIDv1");
        //    return "success";
        //}
        //[HttpPost]
        //[Route("DHRDelTokenMobilePulong")]
        //public string DHRDelToken(Token T)
        //{
        //    GetDatabase GDB = new GetDatabase();
        //    GDB.ClientName = T.CN;
        //    SelfieRegistration2Controller.GetDB2(GDB);
        //    Connection Con = new Connection();
        //    Con.myparameters.Add(new myParameters { ParameterName = "@EMPID", mytype = SqlDbType.NVarChar, Value = T.EMPID });
        //    Con.ExecuteNonQuery("sp_DHRDelTokenv1");
        //    return "success";
        //}

        #endregion
    }
}
