﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace illimitadoWepAPI.Models
{
    public class UserProfile
    {
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string MIDDLENAME { get; set; }
        public string SUFFIX { get; set; }
        public string CARDNAME { get; set; }
        public string BIRTHDAY { get; set; }
        public string PLACEOFBIRTH { get; set; }
        public string NATIONALITY { get; set; }
        public string NATCOUNTRY { get; set; }
        public string GENDER { get; set; }
        public string MOBILE { get; set; }
        public string TELPHONE { get; set; }
        public string PRESHOUSENUMBER { get; set; }
        public string PRESHOUSEADDRESS { get; set; }
        public string PRESBRGY { get; set; }
        public string PRESMUNI { get; set; }
        public string PRESCOUNTRY { get; set; }
        public string PRESREGION { get; set; }
        public string PRESPROVINCE { get; set; }
        public string PRESZIPCODE { get; set; }
        public string PRESCITY { get; set; }
        public string PERMHOUSENUMBER { get; set; }
        public string PERMHOUSEADDRESS { get; set; }
        public string PERMBRGY { get; set; }
        public string PERMUNI { get; set; }
        public string PERMCOUNTRY { get; set; }
        public string PERMREGION { get; set; }
        public string PERMPROVINCE { get; set; }
        public string PERMZIPCODE { get; set; }
        public string PERMCITY { get; set; }
        public string EMAILADD { get; set; }
        public string EMPLOYERNAME { get; set; }
        public string CIVILSTATUS { get; set; }
        public string MOTHFIRSTNAME { get; set; }
        public string MOTHLASTNAME { get; set; }
        public string MOTHMIDDLENAME { get; set; }
        public string MOTHSUFFIX { get; set; }
        public string NUMBEROFPADS { get; set; }
        public string OTHERDETAILS { get; set; }
        public string FILENAME { get; set; }
        public string NTID { get; set; }
        public string TIN { get; set; }
        public string SCORE { get; set; }
        public string ProfileID { get; set; }
        public string Status { get; set; }
        public String LoginID { get; set; }
        public String Password { get; set; }
        public String FaceID { get; set; }
        public string PayYear { get; set; }
        public string PayMonth { get; set; }
        public string PayDay { get; set; }
        public string Access { get; set; }

        public string COMPANYNAME { get; set; }
        public string SPECIALIZATION { get; set; }
        public string ROLEPOSITIONTITLE { get; set; }
        public String POSITIONLEVEL { get; set; }
        public String JOINEDFROM { get; set; }
        public String JOINEDTO { get; set; }
        public string INDUSTRY { get; set; }
        public string COUNTRY { get; set; }
        public string EXPERIENCE { get; set; }
        public string PositionTitle { get; set; }
        public string JoinDate { get; set; }
        public string Marrieddate { get; set; }
        public string MaritalStatus { get; set; }
        public string Coordinates { get; set; }
        public string EmpStatus { get; set; }
    }

    public class UserProfile_Images
    {
        public string Signature1 { get; set; }
        public string Signature2 { get; set; }
        public string Signature3 { get; set; }
        public string Card1 { get; set; }
        public string Card2 { get; set; }
        public string EmpID { get; set; }
        public string ImageType { get; set; }

    }

    public class OR_Reimbursement
    {
        public string EmpID { get; set; }
        public string BenefitType { get; set; }
        public string ORDate { get; set; }
        public string ORTime { get; set; }
        public string ORDateTIme { get; set; }
        public string ORAmount { get; set; }
        public string VendorName { get; set; }
        public string VendorTin { get; set; }
        public string Base { get; set; }
        public string Description { get; set; }
        public string BenefitID { get; set; }
        public string CN { get; set; }

    }


    public class UpdateEmployeeConfiInfo
    {
        public string EmpID { get; set; }
        public string TaxStatus { get; set; }
        public string SSS { get; set; }
        public string TIN { get; set; }
        public string PhilHealth { get; set; }
        public string Pagibig { get; set; }
        public string DriversLicense { get; set; }
        public string Passport { get; set; }
        public string Unified { get; set; }
        public string Postal { get; set; }
        public string BirthCertificate { get; set; }
        public string SssImage { get; set; }
        public string TinImage { get; set; }
        public string PhilImage { get; set; }
        public string PagImage { get; set; }
        public string DriverImage { get; set; }
        public string PassportImage { get; set; }
        public string UmidImage { get; set; }
        public string PostalImage { get; set; }
        public string BirthImage { get; set; }
        public string DriversExpiry { get; set; }
        public string PassportExpiry { get; set; }
        public string PostalExpiry { get; set; }
        public string SAGSD { get; set; }
        public string SAGSDExpiry { get; set; }
        public string NTC { get; set; }
        public string NTCExpiry { get; set; }
        public string BrgyClearance { get; set; }
        public string BrgyClearanceExpiry { get; set; }
        public string PNPClearance { get; set; }
        public string PNPClearanceExpiry { get; set; }
        public string NBIClearance { get; set; }
        public string NBIClearanceExpiry { get; set; }
        public string NueroEvaluation { get; set; }
        public string NeuroEvaluationExpiry { get; set; }
        public string DrugTest { get; set; }
        public string DrugTestExpiry { get; set; }

    }



    public class UpdateEmployeeInfo
    {
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string Ntlogin { get; set; }
        public string Alias { get; set; }
        public string Deptcode { get; set; }
        public string Deptname { get; set; }
        public string BUnit { get; set; }
        public string BUnithead { get; set; }
        public string Workstation { get; set; }
        public string Supervisor { get; set; }
        public string DTransFrom { get; set; }
        public string DTransTo { get; set; }
        public string LocFrom { get; set; }
        public string LocTo { get; set; }
        public string CCChangeFrom { get; set; }
        public string CCChangeTo { get; set; }
        public string Email { get; set; }
        public string WorkEmail { get; set; }
        public string Extension { get; set; }
        public string JoinDate { get; set; }
        public string ProdStart { get; set; }
        public string Tenure { get; set; }
        public string TenureMnth { get; set; }
        public string Batch { get; set; }
        public string Class { get; set; }
        public string Skill { get; set; }
        public string JobDesc { get; set; }
        public string Promotion { get; set; }
        public string Status { get; set; }
        public string Emp_Name { get; set; }
        public string approved { get; set; }
        public string Employee_Level { get; set; }
        public string Employee_Category { get; set; }
        public string BusinessSegment { get; set; }
        public string IsApprove { get; set; }
        public string ResignedDate { get; set; }

    }


    public class UpdateEmployeePersonalInfo
    {
        public string EmpID { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Barangay { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public string BloodType { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Citizenship { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        public string ContactNo { get; set; }
        public string Province { get; set; }
        public string EmrgFname { get; set; }
        public string EmrgMname { get; set; }
        public string EmrgLname { get; set; }


    }
    public class Master_User_Info_Personal_Confi_Info
    {
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string NTID { get; set; }
        public string EmpName { get; set; }
        public string EmpType { get; set; }
        public string EmpLevel { get; set; }
        public string EmpStatus { get; set; }
        public string UserType { get; set; }
        public string Gender { get; set; }
        public string DateJoined { get; set; }
        public string ContractEndDate { get; set; }
        public string LeaveBal { get; set; }
        public string LeaveBalCTO { get; set; }
        public string Country { get; set; }
        public string JobDesc { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public string MngrID { get; set; }
        public string MngrName { get; set; }
        public string MngrNTID { get; set; }
        public string BusinessUnit { get; set; }
        public string BusinessUnitHead { get; set; }
        public string BusinessSegment { get; set; }
        public string AccessLevel { get; set; }
        public string DOB { get; set; }
        public string Alias { get; set; }
        public string ExtensionNumber { get; set; }
        public string Skill { get; set; }
        public string Batch { get; set; }
        public string Class { get; set; }
        public string Workstation { get; set; }
        public string ProductionStartDate { get; set; }
        public string ModifiedBy { get; set; }
        public string DateModified { get; set; }
        public string EmpPayHrsCat { get; set; }
        public string Salary { get; set; }
        public string DailyRate { get; set; }
        public string EcolaWageOrder { get; set; }
        public string EcolaRate { get; set; }
        public string Transportation { get; set; }
        public string Rice { get; set; }
        public string Communication { get; set; }
        public string EcolaRegion { get; set; }
        public string Branch { get; set; }
        public string Wallet { get; set; }
        public string FaceID { get; set; }
        public string DTransFrom { get; set; }
        public string DTransTo { get; set; }
        public string LocFrom { get; set; }
        public string LocTo { get; set; }
        public string CCChangeFrom { get; set; }
        public string CCChangeTo { get; set; }
        public string Email { get; set; }
        public string WorkEmail { get; set; }
        public string Extension { get; set; }
        public string JoinDate { get; set; }
        public string ProdStart { get; set; }
        public string Tenure { get; set; }
        public string TenureMnth { get; set; }
        public string Batch1 { get; set; }
        public string Class1 { get; set; }
        public string Skill1 { get; set; }
        public string JobDesc1 { get; set; }
        public string Promotion { get; set; }
        public string Status { get; set; }
        public string Emp_Name { get; set; }
        public string approved { get; set; }
        public string Employee_Level { get; set; }
        public string Employee_Category { get; set; }
        public string BusinessSegment1 { get; set; }
        public string IsApprove { get; set; }
        public string ResignedDate { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Barangay { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public string BloodType { get; set; }
        public string Gender1 { get; set; }
        public string DOB1 { get; set; }
        public string Citizenship { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }
        //public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        //public string ContactNo { get; set; }
        public string Province { get; set; }
        public string TaxStatus { get; set; }
        public string SSS { get; set; }
        public string TIN { get; set; }
        public string PhilHealth { get; set; }
        public string Pagibig { get; set; }
        public string DriversLicense { get; set; }
        public string Passport { get; set; }
        public string Unified { get; set; }
        public string Postal { get; set; }
        public string BirthCertificate { get; set; }
        public string SssImage { get; set; }
        public string TinImage { get; set; }
        public string PhilImage { get; set; }
        public string PagImage { get; set; }
        public string DriverImage { get; set; }
        public string PassportImage { get; set; }
        public string UmidImage { get; set; }
        public string PostalImage { get; set; }
        public string BirthImage { get; set; }
        public string DriversExpiry { get; set; }
        public string PassportExpiry { get; set; }
        public string PostalExpiry { get; set; }
        public string SAGSD { get; set; }
        public string SAGSDExpiry { get; set; }
        public string NTC { get; set; }
        public string NTCExpiry { get; set; }
        public string BrgyClearance { get; set; }
        public string BrgyClearanceExpiry { get; set; }
        public string PNPClearance { get; set; }
        public string PNPClearanceExpiry { get; set; }
        public string NBIClearance { get; set; }
        public string NBIClearanceExpiry { get; set; }
        public string NueroEvaluation { get; set; }
        public string NeuroEvaluationExpiry { get; set; }
        public string DrugTest { get; set; }
        public string DrugTestExpiry { get; set; }
        public string PlaceofBirth { get; set; }

        public List<PersonalEmail> PersonalEmail;

        public List<ContactNo> ContactNo;
    }
    public class PersonalEmail
    {
        public string Email { get; set; }
        public string EmailType { get; set; }
       
    }
    public class ContactNo
    {
        public string Contact { get; set; }
        public string ContactType { get; set; }

    }
    public class Information
    {
        public List<Master_User_Info_Personal_Confi_Info> CompleteProfile;
        public string myreturn;
    }
    public class Informationv2
    {
        public List<Master_User_Info_Personal_Confi_Infov3> CompleteProfile;
        public string myreturn;
    }
    public class Government
    {
        public string Contact { get; set; }
        public string ContactType { get; set; }

    }
    public class GovernmentLicenses
    {
        public string Contact { get; set; }
        public string ContactType { get; set; }

    }




    public class EmployerHistory
    {
        public string Company_Name { get; set; }
        public string Work_From { get; set; }
        public string Work_To { get; set; }
    }

    public class EmployerList
    {
        public List<EmployerHistory> EmployerListing;
        public string myreturn;
    }


    public class DependentList
    {
        public List<DependentHistory> DependentListing;
        public string myreturn;
    }

    public class DependentHistory
    {
        public string FullName { get; set; }
        public string DOB { get; set; }
        public string Relationship { get; set; }
        public string EmpID { get; set; }
        public string First { get; set; }
        public string Mid { get; set; }
        public string Lastname { get; set; }
        public string Extension { get; set; }
        public string Birth { get; set; }
        public string Comments { get; set; }
        public string DocFilename { get; set; }


    }


    public class GetCount
    {
        public string AdFilename { get; set; }

    }

    public class updateReimburse
    {
        public string EmpID { get; set; }
        public string Image { get; set; }
        public byte[] Byte { get; set; }
    }

    public class ReimburseList
    {
        public List<ReimburseGet> ListReimburse;
        public string myreturn;
    }

    public class ReimburseGet
    {
        public string Name { get; set; }
        public string Required { get; set; }


    }
    public class SalaryList
    {
        public string Salary { get; set; }
        public string Days { get; set; }
        public string DailyRate { get; set; }
        public string HourlyRate { get; set; }
        public string PayrollScheme { get; set; }
        public string EcolaWageOrder { get; set; }
        public string EcolaRate { get; set; }

        public string BankName { get; set; }
        public string BranchName { get; set; }
        public string AccountNumber { get; set; }



    }
    public class GetCompanyProfileView
    {
        public string BranchID { get; set; }
        public string OfficeType { get; set; }
        public string OfficeName { get; set; }
        public string TradingName { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public string DateOfRegistration { get; set; }
        public string AddUnitFloor { get; set; }
        public string AddBldg { get; set; }

        public string AddStreet { get; set; }
        public string AddBarangay { get; set; }
        public string AddMunicipality { get; set; }
        public string AddCity { get; set; }
        public string AddRegion { get; set; }
        public string AddCountry { get; set; }
        public string AddZipCode { get; set; }
        public string Sss { get; set; }
        public string Philhealth { get; set; }
        public string Tin { get; set; }
        public string RDOCode { get; set; }
        public string Pagibig { get; set; }
        public string PagibigCode { get; set; }



    }

    public class CompanyProfileList
    {
        public List<GetCompanyProfileView> CompanyProfile;
        public string myreturn;
    }

    public class GetCompanyProfileContactView
    {
        public string PhoneType { get; set; }
        public string ContactNumber { get; set; }



    }

    public class CompanyProfileContactList
    {
        public List<GetCompanyProfileContactView> GetCompanyProfileContactView;
        public string myreturn;
    }

    public class GetCompanyProfileEmailView
    {
        public string EmailType { get; set; }
        public string Emailtxt { get; set; }
    }

    public class CompanyProfileEmailList
    {
        public List<GetCompanyProfileEmailView> GetCompanyProfileEmailView;
        public string myreturn;
    }

    public class CarDueDetails
    {
        public string EMPID { get; set; }
        public string CARBRANDID { get; set; }
        public string CARMODELID { get; set; }
        public string CARVARIANTID { get; set; }
        public string CARMAKEYEAR { get; set; }
        public string CARPLATENO { get; set; }
        public string MONTHPURCHASED { get; set; }
        public string YEARPURCHASED { get; set; }
        public string ISINSURED { get; set; }
        public string INSURANCEEXPIRY { get; set; }
        public string INSURANCEAMOUNT { get; set; }
        public string ISREMIND { get; set; }
        public string INSURANCEREMINDER { get; set; }
        public string ISCARLOAN { get; set; }
        public string PAYMENTDUEDATE { get; set; }
        public string MONTHLYAMORRIZATION { get; set; }
        public string MONTHLOANEND { get; set; }
        public string YEARLOANEND { get; set; }
    }

    public class HomeTypeDetails
    {
        public string EMPID { get; set; }
        public string HOMESTATUS { get; set; }
        public string HOUSETYPEID { get; set; }
        public string PAYMENTTYPEID { get; set; }
        public string PAYMENTMONTH { get; set; }
        public string PAYMENTDUEDATE { get; set; }
        public string RENTAMOUNT { get; set; }
        public string ISHOAREMIND { get; set; }
        public string HOMETYPE { get; set; }
        public string HOAPAYMENTTYPEID { get; set; }
        public string HOAPAYMENTMONTH { get; set; }
        public string HOAPAYMENTDUEDATE { get; set; }
        public string HOAFEE { get; set; }
        public string BANKID { get; set; }
        public string LOANTERMNUMBER { get; set; }
        public string LOANTERMID { get; set; }
        public string LOANSTARTMONTH { get; set; }
        public string LOANSTARTYEAR { get; set; }
        public string MORTPAYMENTTYPEID { get; set; }
        public string MORTPAYMENTMONTH { get; set; }
        public string MORTPAYMENTDUEDATE { get; set; }
        public string MORTPAYMENTAMOUNT { get; set; }
        public string ISPAYINGFEE { get; set; }
        public string SCHOOLNAME { get; set; }
        public string DOWNPAYMENTAMOUNT { get; set; }
        public string FSTQUARTERAMOUNT { get; set; }
        public string FSTQUARTERDUEDATE { get; set; }
        public string SNDQUARTERAMOUNT { get; set; }
        public string SNDQUARTERDUEDATE { get; set; }
        public string TRDQUARTERAMOUNT { get; set; }
        public string TRDQUARTERDUEDATE { get; set; }
        public string FTHQUARTERAMOUNT { get; set; }
        public string FTHQUARTERDUEDATE { get; set; }
    }

    public class LoanAndServiceTypeDetails
    {
        public string EMPID { get; set; }
        public string ISOTHERLOAN { get; set; }
        public string BANKID { get; set; }
        public string LOANTERMNUMBER { get; set; }
        public string LOANTERMID { get; set; }
        public string LOANSTARTMONTH { get; set; }
        public string LOANSTARTYEAR { get; set; }
        public string PAYMENTTYPEID { get; set; }
        public string PAYMENTMONTH { get; set; }
        public string PAYMENTDUEDATE { get; set; }
        public string PAYMENTAMOUNT { get; set; }
        public string SERVICETYPE { get; set; }
        public string SPID { get; set; }
        public string ACCOUNTNO { get; set; }
        public string MONTHLYFEE { get; set; }
        public string DUEDATE { get; set; }
    }
    public class form1601C
    {
        public string empid { get; set; }
        public string path { get; set; }
        public string yr { get; set; }
        public string mon { get; set; }

    }

    //---------------------------------------------------------------Revised Change DB Start------------------------------------------------------------------------
    public class LoginLuxandModels
    {
        public string LoginID { get; set; }
        public string Password { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string MIDDLENAME { get; set; }
        public string NTID { get; set; }
        public string FaceID { get; set; }
        public string Access { get; set; }
        public string JoinDate { get; set; }
        public string BIRTHDAY { get; set; }
        public string CIVILSTATUS { get; set; }
        public string Marrieddate { get; set; }
        public string MaritalStatus { get; set; }
        public string Coordinates { get; set; }
        public string EmpStatus { get; set; }
        public string LuxandKey { get; set; }
    }
    public class UserProfilev2
    {
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string MIDDLENAME { get; set; }
        public string SUFFIX { get; set; }
        public string CARDNAME { get; set; }
        public string BIRTHDAY { get; set; }
        public string PLACEOFBIRTH { get; set; }
        public string NATIONALITY { get; set; }
        public string NATCOUNTRY { get; set; }
        public string GENDER { get; set; }
        public string MOBILE { get; set; }
        public string TELPHONE { get; set; }
        public string PRESHOUSENUMBER { get; set; }
        public string PRESHOUSEADDRESS { get; set; }
        public string PRESBRGY { get; set; }
        public string PRESMUNI { get; set; }
        public string PRESCOUNTRY { get; set; }
        public string PRESREGION { get; set; }
        public string PRESPROVINCE { get; set; }
        public string PRESZIPCODE { get; set; }
        public string PRESCITY { get; set; }
        public string PERMHOUSENUMBER { get; set; }
        public string PERMHOUSEADDRESS { get; set; }
        public string PERMBRGY { get; set; }
        public string PERMUNI { get; set; }
        public string PERMCOUNTRY { get; set; }
        public string PERMREGION { get; set; }
        public string PERMPROVINCE { get; set; }
        public string PERMZIPCODE { get; set; }
        public string PERMCITY { get; set; }
        public string EMAILADD { get; set; }
        public string EMPLOYERNAME { get; set; }
        public string CIVILSTATUS { get; set; }
        public string MOTHFIRSTNAME { get; set; }
        public string MOTHLASTNAME { get; set; }
        public string MOTHMIDDLENAME { get; set; }
        public string MOTHSUFFIX { get; set; }
        public string NUMBEROFPADS { get; set; }
        public string OTHERDETAILS { get; set; }
        public string FILENAME { get; set; }
        public string NTID { get; set; }
        public string TIN { get; set; }
        public string SCORE { get; set; }
        public string ProfileID { get; set; }
        public string Status { get; set; }
        public String LoginID { get; set; }
        public String Password { get; set; }
        public String FaceID { get; set; }
        public string PayYear { get; set; }
        public string PayMonth { get; set; }
        public string PayDay { get; set; }
        public string Access { get; set; }

        public string COMPANYNAME { get; set; }
        public string SPECIALIZATION { get; set; }
        public string ROLEPOSITIONTITLE { get; set; }
        public String POSITIONLEVEL { get; set; }
        public String JOINEDFROM { get; set; }
        public String JOINEDTO { get; set; }
        public string INDUSTRY { get; set; }
        public string COUNTRY { get; set; }
        public string EXPERIENCE { get; set; }
        public string PositionTitle { get; set; }
        public string JoinDate { get; set; }
        public string Marrieddate { get; set; }
        public string MaritalStatus { get; set; }
        public string Coordinates { get; set; }
        public string EmpStatus { get; set; }
        public string CN { get; set; }
        public string onBoardstatus { get; set; }
      
    }

    public class UserProfile_Imagesv2
    {
        public string Signature1 { get; set; }
        public string Signature2 { get; set; }
        public string Signature3 { get; set; }
        public string Card1 { get; set; }
        public string Card2 { get; set; }
        public string EmpID { get; set; }
        public string ImageType { get; set; }
        public string URL { get; set; }
        public string CN { get; set; }
    }


    public class DependentHistoryv2
    {
        public string FullName { get; set; }
        public string DOB { get; set; }
        public string Relationship { get; set; }
        public string EmpID { get; set; }
        public string First { get; set; }
        public string Mid { get; set; }
        public string Lastname { get; set; }
        public string Extension { get; set; }
        public string Birth { get; set; }
        public string Comments { get; set; }
        public string DocFilename { get; set; }
        public string CN { get; set; }
    }

    public class Master_User_Info_Personal_Confi_Infov2
    {
        public string CN { get; set; }
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string NTID { get; set; }
        public string EmpName { get; set; }
        public string EmpType { get; set; }
        public string EmpLevel { get; set; }
        public string EmpStatus { get; set; }
        public string UserType { get; set; }
        public string Gender { get; set; }
        public string DateJoined { get; set; }
        public string ContractEndDate { get; set; }
        public string LeaveBal { get; set; }
        public string LeaveBalCTO { get; set; }
        public string Country { get; set; }
        public string JobDesc { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public string MngrID { get; set; }
        public string MngrName { get; set; }
        public string MngrNTID { get; set; }
        public string BusinessUnit { get; set; }
        public string BusinessUnitHead { get; set; }
        public string BusinessSegment { get; set; }
        public string AccessLevel { get; set; }
        public string DOB { get; set; }
        public string Alias { get; set; }
        public string ExtensionNumber { get; set; }
        public string Skill { get; set; }
        public string Batch { get; set; }
        public string Class { get; set; }
        public string Workstation { get; set; }
        public string ProductionStartDate { get; set; }
        public string ModifiedBy { get; set; }
        public string DateModified { get; set; }
        public string EmpPayHrsCat { get; set; }
        public string Salary { get; set; }
        public string DailyRate { get; set; }
        public string EcolaWageOrder { get; set; }
        public string EcolaRate { get; set; }
        public string Transportation { get; set; }
        public string Rice { get; set; }
        public string Communication { get; set; }
        public string EcolaRegion { get; set; }
        public string Branch { get; set; }
        public string Wallet { get; set; }
        public string FaceID { get; set; }
        public string DTransFrom { get; set; }
        public string DTransTo { get; set; }
        public string LocFrom { get; set; }
        public string LocTo { get; set; }
        public string CCChangeFrom { get; set; }
        public string CCChangeTo { get; set; }
        public string Email { get; set; }
        public string WorkEmail { get; set; }
        public string Extension { get; set; }
        public string JoinDate { get; set; }
        public string ProdStart { get; set; }
        public string Tenure { get; set; }
        public string TenureMnth { get; set; }
        public string Batch1 { get; set; }
        public string Class1 { get; set; }
        public string Skill1 { get; set; }
        public string JobDesc1 { get; set; }
        public string Promotion { get; set; }
        public string Status { get; set; }
        public string Emp_Name { get; set; }
        public string approved { get; set; }
        public string Employee_Level { get; set; }
        public string Employee_Category { get; set; }
        public string BusinessSegment1 { get; set; }
        public string IsApprove { get; set; }
        public string ResignedDate { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Barangay { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public string BloodType { get; set; }
        public string Gender1 { get; set; }
        public string DOB1 { get; set; }
        public string Citizenship { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }
        //public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        //public string ContactNo { get; set; }
        public string Province { get; set; }
        public string TaxStatus { get; set; }
        public string SSS { get; set; }
        public string TIN { get; set; }
        public string PhilHealth { get; set; }
        public string Pagibig { get; set; }
        public string DriversLicense { get; set; }
        public string Passport { get; set; }
        public string Unified { get; set; }
        public string Postal { get; set; }
        public string BirthCertificate { get; set; }
        public string SssImage { get; set; }
        public string TinImage { get; set; }
        public string PhilImage { get; set; }
        public string PagImage { get; set; }
        public string DriverImage { get; set; }
        public string PassportImage { get; set; }
        public string UmidImage { get; set; }
        public string PostalImage { get; set; }
        public string BirthImage { get; set; }
        public string DriversExpiry { get; set; }
        public string PassportExpiry { get; set; }
        public string PostalExpiry { get; set; }
        public string SAGSD { get; set; }
        public string SAGSDExpiry { get; set; }
        public string NTC { get; set; }
        public string NTCExpiry { get; set; }
        public string BrgyClearance { get; set; }
        public string BrgyClearanceExpiry { get; set; }
        public string PNPClearance { get; set; }
        public string PNPClearanceExpiry { get; set; }
        public string NBIClearance { get; set; }
        public string NBIClearanceExpiry { get; set; }
        public string NueroEvaluation { get; set; }
        public string NeuroEvaluationExpiry { get; set; }
        public string DrugTest { get; set; }
        public string DrugTestExpiry { get; set; }
        public string PlaceofBirth { get; set; }

        public List<PersonalEmail> PersonalEmail;

        public List<ContactNo> ContactNo;
    }

    public class UpdateEmployeePersonalInfov2
    {
        public string EmpID { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Barangay { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public string BloodType { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Citizenship { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        public string ContactNo { get; set; }
        public string Province { get; set; }
        public string EmrgFname { get; set; }
        public string EmrgMname { get; set; }
        public string EmrgLname { get; set; }
        public string CN { get; set; }
    }

    public class UpdateEmployeeInfov2
    {
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string Ntlogin { get; set; }
        public string Alias { get; set; }
        public string Deptcode { get; set; }
        public string Deptname { get; set; }
        public string BUnit { get; set; }
        public string BUnithead { get; set; }
        public string Workstation { get; set; }
        public string Supervisor { get; set; }
        public string DTransFrom { get; set; }
        public string DTransTo { get; set; }
        public string LocFrom { get; set; }
        public string LocTo { get; set; }
        public string CCChangeFrom { get; set; }
        public string CCChangeTo { get; set; }
        public string Email { get; set; }
        public string WorkEmail { get; set; }
        public string Extension { get; set; }
        public string JoinDate { get; set; }
        public string ProdStart { get; set; }
        public string Tenure { get; set; }
        public string TenureMnth { get; set; }
        public string Batch { get; set; }
        public string Class { get; set; }
        public string Skill { get; set; }
        public string JobDesc { get; set; }
        public string Promotion { get; set; }
        public string Status { get; set; }
        public string Emp_Name { get; set; }
        public string approved { get; set; }
        public string Employee_Level { get; set; }
        public string Employee_Category { get; set; }
        public string BusinessSegment { get; set; }
        public string IsApprove { get; set; }
        public string ResignedDate { get; set; }
        public string CN { get; set; }

        public string Ref1FNAME { get; set; }
        public string Ref1LNAME { get; set; }
        public string Ref1Contact { get; set; }
        public string Ref2FNAME { get; set; }
        public string Ref2LNAME { get; set; }
        public string Ref2Contact { get; set; }
    }

    public class NewEmployee
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string Name { get; set; }
        public string EmpType { get; set; }
        public string EmpLevel { get; set; }
        public string NTID { get; set; }
        public string UserType { get; set; }
        public string LeaveBal { get; set; }
        public string LeaveBalCTO { get; set; }
        public string Alias { get; set; }
        public string Badge { get; set; }
        public string Extensionnumber { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public string MngrID { get; set; }
        public string MngrName { get; set; }
        public string MngrNTID { get; set; }
        public string BusinessUnit { get; set; }
        public string BusinessUnitHead { get; set; }
        public string BusinessSegment { get; set; }
        public string WorkStation { get; set; }
        public string DateJoined { get; set; }
        public string ProductionStartDate { get; set; }
        public string Batch { get; set; }
        public string Class { get; set; }
        public string Skill { get; set; }
        public string JobDesc { get; set; }
        public string EmpStatus { get; set; }
        public string EmpPayHrsCat { get; set; }
        public string Branch { get; set; }
        public string Wallet { get; set; }
        public string Tenure { get; set; }
        public string TenureMonth { get; set; }
        public string Industry { get; set; }
        public string Salary { get; set; }
        public string WorkingDays { get; set; }
        public string DailyRate { get; set; }
        public string HourlyRate { get; set; }
        public string PayrollScheme { get; set; }
        public string Preffix { get; set; }
        public string Suffix { get; set; }
        public string Country { get; set; }
        public string ContractStatus { get; set; }
        public string ContractNo { get; set; }
        public string ContractEnd { get; set; }
        public string Promotion { get; set; }
        public string Role { get; set; }

    }

    public class UpdateEmployeeConfiInfov2
    {
        public string EmpID { get; set; }
        public string TaxStatus { get; set; }
        public string SSS { get; set; }
        public string TIN { get; set; }
        public string PhilHealth { get; set; }
        public string Pagibig { get; set; }
        public string DriversLicense { get; set; }
        public string Passport { get; set; }
        public string Unified { get; set; }
        public string Postal { get; set; }
        public string BirthCertificate { get; set; }
        public string SssImage { get; set; }
        public string TinImage { get; set; }
        public string PhilImage { get; set; }
        public string PagImage { get; set; }
        public string DriverImage { get; set; }
        public string PassportImage { get; set; }
        public string UmidImage { get; set; }
        public string PostalImage { get; set; }
        public string BirthImage { get; set; }
        public string DriversExpiry { get; set; }
        public string PassportExpiry { get; set; }
        public string PostalExpiry { get; set; }
        public string SAGSD { get; set; }
        public string SAGSDExpiry { get; set; }
        public string NTC { get; set; }
        public string NTCExpiry { get; set; }
        public string BrgyClearance { get; set; }
        public string BrgyClearanceExpiry { get; set; }
        public string PNPClearance { get; set; }
        public string PNPClearanceExpiry { get; set; }
        public string NBIClearance { get; set; }
        public string NBIClearanceExpiry { get; set; }
        public string NueroEvaluation { get; set; }
        public string NeuroEvaluationExpiry { get; set; }
        public string DrugTest { get; set; }
        public string DrugTestExpiry { get; set; }
        public string CN { get; set; }
    }
    public class GetCompanyProfileViewv2
    {
        public string BranchID { get; set; }
        public string OfficeType { get; set; }
        public string OfficeName { get; set; }
        public string TradingName { get; set; }
        public string CompanyRegistrationNo { get; set; }
        public string DateOfRegistration { get; set; }
        public string AddUnitFloor { get; set; }
        public string AddBldg { get; set; }

        public string AddStreet { get; set; }
        public string AddBarangay { get; set; }
        public string AddMunicipality { get; set; }
        public string AddCity { get; set; }
        public string AddRegion { get; set; }
        public string AddCountry { get; set; }
        public string AddZipCode { get; set; }
        public string Sss { get; set; }
        public string Philhealth { get; set; }
        public string Tin { get; set; }
        public string RDOCode { get; set; }
        public string Pagibig { get; set; }
        public string PagibigCode { get; set; }
        public string CN { get; set; }
    }
    public class Master_User_Info_Personal_Confi_Infov3
    {
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string NTID { get; set; }
        public string EmpName { get; set; }
        public string EmpType { get; set; }
        public string EmpLevel { get; set; }
        public string EmpStatus { get; set; }
        public string UserType { get; set; }
        public string Gender { get; set; }
        public string DateJoined { get; set; }
        public string ContractEndDate { get; set; }
        public string LeaveBal { get; set; }
        public string LeaveBalCTO { get; set; }
        public string Country { get; set; }
        public string JobDesc { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public string MngrID { get; set; }
        public string MngrName { get; set; }
        public string MngrNTID { get; set; }
        public string BusinessUnit { get; set; }
        public string BusinessUnitHead { get; set; }
        public string BusinessSegment { get; set; }
        public string AccessLevel { get; set; }
        public string DOB { get; set; }
        public string Alias { get; set; }
        public string ExtensionNumber { get; set; }
        public string Skill { get; set; }
        public string Batch { get; set; }
        public string Class { get; set; }
        public string Workstation { get; set; }
        public string ProductionStartDate { get; set; }
        public string ModifiedBy { get; set; }
        public string DateModified { get; set; }
        public string EmpPayHrsCat { get; set; }
        public string Salary { get; set; }
        public string DailyRate { get; set; }
        public string EcolaWageOrder { get; set; }
        public string EcolaRate { get; set; }
        public string Transportation { get; set; }
        public string Rice { get; set; }
        public string Communication { get; set; }
        public string EcolaRegion { get; set; }
        public string Branch { get; set; }
        public string Wallet { get; set; }
        public string FaceID { get; set; }
        public string DTransFrom { get; set; }
        public string DTransTo { get; set; }
        public string LocFrom { get; set; }
        public string LocTo { get; set; }
        public string CCChangeFrom { get; set; }
        public string CCChangeTo { get; set; }
        public string Email { get; set; }
        public string WorkEmail { get; set; }
        public string Extension { get; set; }
        public string JoinDate { get; set; }
        public string ProdStart { get; set; }
        public string Tenure { get; set; }
        public string TenureMnth { get; set; }
        public string Batch1 { get; set; }
        public string Class1 { get; set; }
        public string Skill1 { get; set; }
        public string JobDesc1 { get; set; }
        public string Promotion { get; set; }
        public string Status { get; set; }
        public string Emp_Name { get; set; }
        public string approved { get; set; }
        public string Employee_Level { get; set; }
        public string Employee_Category { get; set; }
        public string BusinessSegment1 { get; set; }
        public string IsApprove { get; set; }
        public string ResignedDate { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Barangay { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public string BloodType { get; set; }
        public string Gender1 { get; set; }
        public string DOB1 { get; set; }
        public string Citizenship { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }
        //public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        //public string ContactNo { get; set; }
        public string Province { get; set; }
        public string TaxStatus { get; set; }
        public string SSS { get; set; }
        public string TIN { get; set; }
        public string PhilHealth { get; set; }
        public string Pagibig { get; set; }
        public string DriversLicense { get; set; }
        public string Passport { get; set; }
        public string Unified { get; set; }
        public string Postal { get; set; }
        public string BirthCertificate { get; set; }
        public string SssImage { get; set; }
        public string TinImage { get; set; }
        public string PhilImage { get; set; }
        public string PagImage { get; set; }
        public string DriverImage { get; set; }
        public string PassportImage { get; set; }
        public string UmidImage { get; set; }
        public string PostalImage { get; set; }
        public string BirthImage { get; set; }
        public string DriversExpiry { get; set; }
        public string PassportExpiry { get; set; }
        public string PostalExpiry { get; set; }
        public string SAGSD { get; set; }
        public string SAGSDExpiry { get; set; }
        public string NTC { get; set; }
        public string NTCExpiry { get; set; }
        public string BrgyClearance { get; set; }
        public string BrgyClearanceExpiry { get; set; }
        public string PNPClearance { get; set; }
        public string PNPClearanceExpiry { get; set; }
        public string NBIClearance { get; set; }
        public string NBIClearanceExpiry { get; set; }
        public string NueroEvaluation { get; set; }
        public string NeuroEvaluationExpiry { get; set; }
        public string DrugTest { get; set; }
        public string DrugTestExpiry { get; set; }
        public string PlaceofBirth { get; set; }
        public string Religion { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string MothersName { get; set; }
        public string FathersName { get; set; }


        public List<PersonalEmail> PersonalEmail;

        public List<ContactNo> ContactNo;
    }
    public class UserProfilev3
    {
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string MIDDLENAME { get; set; }
        public string SUFFIX { get; set; }
        public string CARDNAME { get; set; }
        public string BIRTHDAY { get; set; }
        public string PLACEOFBIRTH { get; set; }
        public string NATIONALITY { get; set; }
        public string NATCOUNTRY { get; set; }
        public string GENDER { get; set; }
        public string MOBILE { get; set; }
        public string TELPHONE { get; set; }
        public string PRESHOUSENUMBER { get; set; }
        public string PRESHOUSEADDRESS { get; set; }
        public string PRESBRGY { get; set; }
        public string PRESMUNI { get; set; }
        public string PRESCOUNTRY { get; set; }
        public string PRESREGION { get; set; }
        public string PRESPROVINCE { get; set; }
        public string PRESZIPCODE { get; set; }
        public string PRESCITY { get; set; }
        public string PERMHOUSENUMBER { get; set; }
        public string PERMHOUSEADDRESS { get; set; }
        public string PERMBRGY { get; set; }
        public string PERMUNI { get; set; }
        public string PERMCOUNTRY { get; set; }
        public string PERMREGION { get; set; }
        public string PERMPROVINCE { get; set; }
        public string PERMZIPCODE { get; set; }
        public string PERMCITY { get; set; }
        public string EMAILADD { get; set; }
        public string EMPLOYERNAME { get; set; }
        public string CIVILSTATUS { get; set; }
        public string MOTHFIRSTNAME { get; set; }
        public string MOTHLASTNAME { get; set; }
        public string MOTHMIDDLENAME { get; set; }
        public string MOTHSUFFIX { get; set; }
        public string NUMBEROFPADS { get; set; }
        public string OTHERDETAILS { get; set; }
        public string FILENAME { get; set; }
        public string NTID { get; set; }
        public string TIN { get; set; }
        public string SCORE { get; set; }
        public string ProfileID { get; set; }
        public string Status { get; set; }
        public String LoginID { get; set; }
        public String Password { get; set; }
        public String FaceID { get; set; }
        public string PayYear { get; set; }
        public string PayMonth { get; set; }
        public string PayDay { get; set; }
        public string Access { get; set; }

        public string COMPANYNAME { get; set; }
        public string SPECIALIZATION { get; set; }
        public string ROLEPOSITIONTITLE { get; set; }
        public String POSITIONLEVEL { get; set; }
        public String JOINEDFROM { get; set; }
        public String JOINEDTO { get; set; }
        public string INDUSTRY { get; set; }
        public string COUNTRY { get; set; }
        public string EXPERIENCE { get; set; }
        public string PositionTitle { get; set; }
        public string JoinDate { get; set; }
        public string Marrieddate { get; set; }
        public string MaritalStatus { get; set; }
        public string Coordinates { get; set; }
        public string EmpStatus { get; set; }
        public string CN { get; set; }

        public string BLOODTYPE { get; set; }
        public string RELIGION { get; set; }
        public string TOWN { get; set; }
        public string HEIGHT { get; set; }
        public string WEIGHT { get; set; }
    }


    #region Marvic 
    public class CompanyCE
    {
        public string LoginID { get; set; }
        public string CN { get; set; }
        public string ContactNumber { get; set; }
        public string ContactTypes { get; set; }
        public string Email { get; set; }
        public string EmailTypes { get; set; }
    }

    //DEPENDENT DETAILS

    public class DHRDEPv1
    {
        public string FullName { get; set; }
        public string DOB { get; set; }
        public string Relationship { get; set; }
        public string EmpID { get; set; }
        public string First { get; set; }
        public string Mid { get; set; }
        public string Lastname { get; set; }
        public string Extension { get; set; }
        public string Birth { get; set; }
        public string Comments { get; set; }
        public string DocFilename { get; set; }
        public string CN { get; set; }
        public string Age { get; set; }

        public string Suffix { get; set; }
        public string LoginID { get; set; }

        public string BASED64IMAGE { get; set; }
        public string CERTIFICATETYPE { get; set; }
        public string URLLINK { get; set; }

        public string dependentID { get; set;}
        public string FileName { get; set; }

    }
    //EMP HISTORY 

    public class DHREMPHv1
    {
        public string CompanyName { get; set; }
        public string Specialization { get; set; }
        public string Role { get; set; }
        public string PositionTitle { get; set; }
        public string Positionlevel { get; set; }
        public string JoinedFrom { get; set; }
        public string JoinedTo { get; set; }
        public string Country { get; set; }
        public string Description { get; set; }
        public string CN { get; set; }
        public string LoginID { get; set; }
        public string Industry { get; set; }
        public string ID { get; set; }
    }

    public class TxtInfoList
    {
        public List<ImpactTxtInfo> TxtInfoListing;
        public string myreturn;
    }

    public class ImpactTxtInfo
    {
        public string Number { get; set; }
        public string TxtMsg { get; set; }
    }
    public class UserProfilev4
    {
        public string PREFFIX { get; set; }
        public string FATHERNANME { get; set; }
        public string MOTHERNAME { get; set; }

        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string MIDDLENAME { get; set; }
        public string SUFFIX { get; set; }
        public string CARDNAME { get; set; }
        public string BIRTHDAY { get; set; }
        public string PLACEOFBIRTH { get; set; }
        public string NATIONALITY { get; set; }
        public string NATCOUNTRY { get; set; }
        public string GENDER { get; set; }
        public string MOBILE { get; set; }
        public string TELPHONE { get; set; }
        public string PRESHOUSENUMBER { get; set; }
        public string PRESHOUSEADDRESS { get; set; }
        public string PRESBRGY { get; set; }
        public string PRESMUNI { get; set; }
        public string PRESCOUNTRY { get; set; }
        public string PRESREGION { get; set; }
        public string PRESPROVINCE { get; set; }
        public string PRESZIPCODE { get; set; }
        public string PRESCITY { get; set; }
        public string PERMHOUSENUMBER { get; set; }
        public string PERMHOUSEADDRESS { get; set; }
        public string PERMBRGY { get; set; }
        public string PERMUNI { get; set; }
        public string PERMCOUNTRY { get; set; }
        public string PERMREGION { get; set; }
        public string PERMPROVINCE { get; set; }
        public string PERMZIPCODE { get; set; }
        public string PERMCITY { get; set; }
        public string EMAILADD { get; set; }
        public string EMPLOYERNAME { get; set; }
        public string CIVILSTATUS { get; set; }
        public string MOTHFIRSTNAME { get; set; }
        public string MOTHLASTNAME { get; set; }
        public string MOTHMIDDLENAME { get; set; }
        public string MOTHSUFFIX { get; set; }
        public string NUMBEROFPADS { get; set; }
        public string OTHERDETAILS { get; set; }
        public string FILENAME { get; set; }
        public string NTID { get; set; }
        public string TIN { get; set; }
        public string SCORE { get; set; }
        public string ProfileID { get; set; }
        public string Status { get; set; }
        public String LoginID { get; set; }
        public String Password { get; set; }
        public String FaceID { get; set; }
        public string PayYear { get; set; }
        public string PayMonth { get; set; }
        public string PayDay { get; set; }
        public string Access { get; set; }

        public string COMPANYNAME { get; set; }
        public string SPECIALIZATION { get; set; }
        public string ROLEPOSITIONTITLE { get; set; }
        public String POSITIONLEVEL { get; set; }
        public String JOINEDFROM { get; set; }
        public String JOINEDTO { get; set; }
        public string INDUSTRY { get; set; }
        public string COUNTRY { get; set; }
        public string EXPERIENCE { get; set; }
        public string PositionTitle { get; set; }
        public string JoinDate { get; set; }
        public string Marrieddate { get; set; }
        public string MaritalStatus { get; set; }
        public string Coordinates { get; set; }
        public string EmpStatus { get; set; }
        public string CN { get; set; }

        public string BLOODTYPE { get; set; }
        public string RELIGION { get; set; }
        public string TOWN { get; set; }
        public string HEIGHT { get; set; }
        public string WEIGHT { get; set; }

        // Complete Address
        public string PREPROPTYPE { get; set; }
        public string PREBLDGNO { get; set; }
        public string PREPLACE { get; set; }
        public string PREWING { get; set; }
        public string PREUNITNO { get; set; }
        public string PREFLOORNO { get; set; }
        public string PREVILLAGE { get; set; }
        public string PREPHASENO { get; set; }
        public string PRESUBDIVISION { get; set; }
        public string PREROOMNO { get; set; }
        public string PREBLKNO { get; set; }
        public string PRELOTNO { get; set; }
        public string PRESTREET { get; set; }
        public string PREBRGYNAME { get; set; }
        public string PRE_REGION { get; set; }
        public string PRE_PROVINCE { get; set; }
        public string PRE_CITYMUNI { get; set; }
        public string PRE_ZIPCODE { get; set; }
        public string ISSAME { get; set; }
        public string PRE_COUNTRY { get; set; }

        public string PERPROPTYPE { get; set; }
        public string PERBLDGNO { get; set; }
        public string PERPLACE { get; set; }
        public string PERWING { get; set; }
        public string PERUNITNO { get; set; }
        public string PERFLOORNO { get; set; }
        public string PERVILLAGE { get; set; }
        public string PERPHASENO { get; set; }
        public string PERSUBDIVISION { get; set; }
        public string PERROOMNO { get; set; }
        public string PERBLKNO { get; set; }
        public string PERLOTNO { get; set; }
        public string PERSTREET { get; set; }
        public string PERBRGYNAME { get; set; }
        public string PER_REGION { get; set; }
        public string PER_PROVINCE { get; set; }
        public string PER_CITYMUNI { get; set; }
        public string PER_ZIPCODE { get; set; }
        public string PER_COUNTRY { get; set; }

        public string Ref1FNAME { get; set; }
        public string Ref1LNAME { get; set; }
        public string Ref1Contact { get; set; }
        public string Ref2FNAME { get; set; }
        public string Ref2LNAME { get; set; }
        public string Ref2Contact { get; set; }

        public string DateJoin { get; set; }
        public string PositionLvl { get; set; }

    }



    public class Master_User_Info_Personal_Confi_Infov4
    {
        public string SeriesID { get; set; }
        public string EmpID { get; set; }
        public string First_Name { get; set; }
        public string Middle_Name { get; set; }
        public string Last_Name { get; set; }
        public string NTID { get; set; }
        public string EmpName { get; set; }
        public string EmpType { get; set; }
        public string EmpLevel { get; set; }
        public string EmpStatus { get; set; }
        public string UserType { get; set; }
        public string Gender { get; set; }
        public string DateJoined { get; set; }
        public string ContractEndDate { get; set; }
        public string LeaveBal { get; set; }
        public string LeaveBalCTO { get; set; }
        public string Country { get; set; }
        public string JobDesc { get; set; }
        public string DeptCode { get; set; }
        public string DeptName { get; set; }
        public string MngrID { get; set; }
        public string MngrName { get; set; }
        public string MngrNTID { get; set; }
        public string BusinessUnit { get; set; }
        public string BusinessUnitHead { get; set; }
        public string BusinessSegment { get; set; }
        public string AccessLevel { get; set; }
        public string DOB { get; set; }
        public string Alias { get; set; }
        public string ExtensionNumber { get; set; }
        public string Skill { get; set; }
        public string Batch { get; set; }
        public string Class { get; set; }
        public string Workstation { get; set; }
        public string ProductionStartDate { get; set; }
        public string ModifiedBy { get; set; }
        public string DateModified { get; set; }
        public string EmpPayHrsCat { get; set; }
        public string Salary { get; set; }
        public string DailyRate { get; set; }
        public string EcolaWageOrder { get; set; }
        public string EcolaRate { get; set; }
        public string Transportation { get; set; }
        public string Rice { get; set; }
        public string Communication { get; set; }
        public string EcolaRegion { get; set; }
        public string Branch { get; set; }
        public string Wallet { get; set; }
        public string FaceID { get; set; }
        public string DTransFrom { get; set; }
        public string DTransTo { get; set; }
        public string LocFrom { get; set; }
        public string LocTo { get; set; }
        public string CCChangeFrom { get; set; }
        public string CCChangeTo { get; set; }
        public string Email { get; set; }
        public string WorkEmail { get; set; }
        public string Extension { get; set; }
        public string JoinDate { get; set; }
        public string ProdStart { get; set; }
        public string Tenure { get; set; }
        public string TenureMnth { get; set; }
        public string Batch1 { get; set; }
        public string Class1 { get; set; }
        public string Skill1 { get; set; }
        public string JobDesc1 { get; set; }
        public string Promotion { get; set; }
        public string Status { get; set; }
        public string Emp_Name { get; set; }
        public string approved { get; set; }
        public string Employee_Level { get; set; }
        public string Employee_Category { get; set; }
        public string BusinessSegment1 { get; set; }
        public string IsApprove { get; set; }
        public string ResignedDate { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Barangay { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public string BloodType { get; set; }
        public string Gender1 { get; set; }
        public string DOB1 { get; set; }
        public string Citizenship { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }

        public string RegDate { get; set; }
        // COMPLETE ADDRESS
        public string PrePropertyType { get; set; }
        public string PreBldgNo { get; set; }
        public string PrePlace { get; set; }
        public string PreWing { get; set; }
        public string PreUnitNo { get; set; }
        public string PreFloorNo { get; set; }
        public string PreVillage { get; set; }
        public string PrePhaseNo { get; set; }
        public string PreSubdivision { get; set; }
        public string PreRoomNo { get; set; }
        public string PreBlkNo { get; set; }
        public string PreLotNo { get; set; }
        public string isSame { get; set; }
        public string PreStreet { get; set; }
        public string PreBrgy { get; set; }
        public string PreRegion { get; set; }
        public string PreProvince{ get; set; }
        public string PreCityMuni { get; set; }
        public string PreZipcode { get; set; }
        public string PreCountry { get; set; }
        public string PerPropertyType { get; set; }
        public string PerBldgNo { get; set; }
        public string PerPlace { get; set; }
        public string PerWing { get; set; }
        public string PerUnitNo { get; set; }
        public string PerFloorNo { get; set; }
        public string PerVillage { get; set; }
        public string PerPhaseNo { get; set; }
        public string PerSubdivision { get; set; }
        public string PerRoomNo { get; set; }
        public string PerBlkNo { get; set; }
        public string PerLotNo { get; set; }
        public string PerStreet { get; set; }
        public string PerBrgy { get; set; }
        public string PerRegion { get; set; }
        public string PerProvince { get; set; }
        public string PerCityMuni { get; set; }
        public string PerZipcode { get; set; }
        public string PerCountry { get; set; }
        // END COMPLETE ADDRESS
        public string Ref1FNAME { get; set; }
        public string Ref1LNAME { get; set; }
        public string Ref1Contact { get; set; }
        public string Ref2FNAME { get; set; }
        public string Ref2LNAME { get; set; }
        public string Ref2Contact { get; set; }


        //public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        //public string ContactNo { get; set; }
        public string Province { get; set; }
        public string TaxStatus { get; set; }
        public string SSS { get; set; }
        public string TIN { get; set; }
        public string PhilHealth { get; set; }
        public string Pagibig { get; set; }
        public string DriversLicense { get; set; }
        public string Passport { get; set; }
        public string Unified { get; set; }
        public string Postal { get; set; }
        public string BirthCertificate { get; set; }
        public string SssImage { get; set; }
        public string TinImage { get; set; }
        public string PhilImage { get; set; }
        public string PagImage { get; set; }
        public string DriverImage { get; set; }
        public string PassportImage { get; set; }
        public string UmidImage { get; set; }
        public string PostalImage { get; set; }
        public string BirthImage { get; set; }
        public string DriversExpiry { get; set; }
        public string PassportExpiry { get; set; }
        public string PostalExpiry { get; set; }
        public string SAGSD { get; set; }
        public string SAGSDExpiry { get; set; }
        public string NTC { get; set; }
        public string NTCExpiry { get; set; }
        public string BrgyClearance { get; set; }
        public string BrgyClearanceExpiry { get; set; }
        public string PNPClearance { get; set; }
        public string PNPClearanceExpiry { get; set; }
        public string NBIClearance { get; set; }
        public string NBIClearanceExpiry { get; set; }
        public string NueroEvaluation { get; set; }
        public string NeuroEvaluationExpiry { get; set; }
        public string DrugTest { get; set; }
        public string DrugTestExpiry { get; set; }
        public string PlaceofBirth { get; set; }
        public string Religion { get; set; }
        public string Height { get; set; }
        public string Weight { get; set; }
        public string MothersName { get; set; }
        public string FathersName { get; set; }

        public string BranchName { get; set; }
        public string BadgeNo { get; set; }
        public string EmpRole { get; set; }
        public string ContractStat { get; set; }
        public string ContractNo { get; set; }
        public string ContractEnd { get; set; }


        public string INDUSTRY { get; set; }


        public string MARITALSTATUS { get; set; }
        public string MARRIEDDATE { get; set; }
        public string CONTACTNUMBER { get; set; }


        public List<PersonalEmail> PersonalEmail;

        public List<ContactNo> ContactNo;
        public List<CompanyEmail> CompanyEmail;
        public List<CompanyContact> CompanyContact;

        public string SUFFIX { get; set; }
        public string PREFFIX { get; set; }

    }

    public class Informationv4
    {
        public List<Master_User_Info_Personal_Confi_Infov4> CompleteProfile;
        public string myreturn;
    }

    public class GeneratedCodev1
    {
        public List<GeneratedCodeData> DisplayGeneratedCode;
        public string myreturn;
    }
    public class GeneratedCodeData
    {
        public string GeneratedCode { get; set; }
    }

    public class GeneratedParameter
    {
        public String EMAIL { get; set; }
        public String CN { get; set; }
    }
    public class CompanyEmail
    {
        public string EmailData { get; set; }
        public string EmailType { get; set; }
        public string ID { get; set; }

    }
    public class CompanyContact
    {
        public string PhoneType { get; set; }
        public string ContactNumber { get; set; }
        public string ID { get; set; }
    }
    #endregion


    #region marvic 2018-09-02 

    public class GovIDLicensesList
    {
        public string CN { get; set; }
        public string LoginID { get; set; }
        public string ID { get; set; }
        public string CompanIDTypes { get; set; }
        public string IDIssued { get; set; }
        public string IDCode { get; set; }
        public string IDExpiryDate { get; set; }
        public string IDBased64Image { get; set; }

        public string IDisRequired { get; set; }
        public string UserTaxStatus { get; set; }
        public string GovTypes { get; set; }
        public string URLLINK { get; set; }
    }

    public class GovIDLicenses
    {
        public string CN { get; set; }
        public string LoginID { get; set; }
        public string BASED64IMAGE { get; set; }
        public string INPUTEDDATA { get; set; }
        public string UID { get; set; }
        public string IDLICENSESTYPE { get; set; }
        public string EXPIRYDATE { get; set; }
        public string IDISSUED { get; set; }

    }

    public class UpdateEmployeePersonalInfov4
    {
        public string EmpID { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Barangay { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public string BloodType { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Citizenship { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        public string ContactNo { get; set; }
        public string Province { get; set; }
        public string EmrgFname { get; set; }
        public string EmrgMname { get; set; }
        public string EmrgLname { get; set; }
        public string CN { get; set; }

        public string FatherName { get; set; }
        public string MotherName { get; set; }


    }

    public class UpdateEmployeePersonalInfov5
    {
        public string EmpID { get; set; }
        public string HouseNumber { get; set; }
        public string StreetName { get; set; }
        public string Barangay { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string ZipCode { get; set; }
        public string BloodType { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Citizenship { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        public string ContactNo { get; set; }
        public string Province { get; set; }
        public string EmrgFname { get; set; }
        public string EmrgMname { get; set; }
        public string EmrgLname { get; set; }
        public string CN { get; set; }

        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string MaritalStatus { get; set; }
        public string MarriedDate { get; set; }

        public string HEIGHT { get; set; }
        public string WEIGHT { get; set; }
        public string PEMAIL { get; set; }

        public string PCONTACT { get; set; }  

        public string Religion { get; set; }

        public string PlaceofBirth { get; set; }
        


    }
    public class UpdateEmployeePersonalInfov6
    {
        public string EmpID { get; set; }
        public string BloodType { get; set; }
        public string Gender { get; set; }
        public string DOB { get; set; }
        public string Citizenship { get; set; }
        public string NCountry { get; set; }
        public string NameofOrganization { get; set; }
        public string MobileAreaCode { get; set; }
        public string MobileNumber { get; set; }
        public string HomeAreaCode { get; set; }
        public string HomeNumber { get; set; }
        public string PersonalEmail { get; set; }
        public string EmrgName { get; set; }
        public string EmrgNumber { get; set; }
        public string EmrgRelationship { get; set; }
        public string OptionalEmail { get; set; }
        public string ContactNo { get; set; }
        public string Province { get; set; }
        public string EmrgFname { get; set; }
        public string EmrgMname { get; set; }
        public string EmrgLname { get; set; }
        public string CN { get; set; }

        public string FatherName { get; set; }
        public string MotherName { get; set; }
        public string MaritalStatus { get; set; }
        public string MarriedDate { get; set; }

        public string HEIGHT { get; set; }
        public string WEIGHT { get; set; }
        public string PEMAIL { get; set; }

        public string PCONTACT { get; set; }

        public string Religion { get; set; }

        public string PlaceofBirth { get; set; }

        public string PREPropertyType { get; set; }
        public string PREBldgNo { get; set; }
        public string PREPlace { get; set; }
        public string PREWing { get; set; }
        public string PREUnitNo { get; set; }
        public string PREFloorNo { get; set; }
        public string PREVillage { get; set; }
        public string PREPhaseNo { get; set; }
        public string PRESubdivision { get; set; }
        public string PRERoomNo { get; set; }
        public string PREBlkNo { get; set; }
        public string PRELotNo { get; set; }
        public string PREStreet { get; set; }
        public string PREBrgyname { get; set; }
        public string PRERegion { get; set; }
        public string PREProvince { get; set; }
        public string PRECityMuni { get; set; }

        public string PREZipcode { get; set; }
        public string PRECountry { get; set; }
        public string ISSAME { get; set; }

        public string PERMAPropertyType { get; set; }
        public string PERMABldgNo { get; set; }
        public string PERMAPlace { get; set; }
        public string PERMAWing { get; set; }
        public string PERMAUnitNo { get; set; }
        public string PERMAFloorNo { get; set; }
        public string PERMAVillage { get; set; }
        public string PERMAPhaseNo { get; set; }
        public string PERMASubdivision { get; set; }
        public string PERMARoomNo { get; set; }
        public string PERMABlkNo { get; set; }
        public string PERMALotNo { get; set; }
        public string PERMAStreet { get; set; }
        public string PERMABrgyname { get; set; }
        public string PERMARegion { get; set; }
        public string PERMAProvince { get; set; }
        public string PERMACityMuni { get; set; }

        public string PERMAZipcode { get; set; }
        public string PERMACountry { get; set; }

        public string Ref1FNAME { get; set; }
        public string Ref1LNAME { get; set; }
        public string Ref1Contact { get; set; }
        public string Ref2FNAME { get; set; }
        public string Ref2LNAME { get; set; }
        public string Ref2Contact { get; set; }

    }
    #endregion

    #region 2018-09-10
    public class AuditTrailList
    {
        public string CN { get; set; }
        public string ID { get; set; }
        public string MODIFIEDBYNAME { get; set; }
        public string MODIFIEDBYEMPID { get; set; }
        public string CHANGEFROM { get; set; }
        public string CHANGETO { get; set; }
        public string MODIFIEDDATE { get; set; }
        public string CATEGORY { get; set; }
        public string DATEFROM { get; set; }
        public string DATETO { get; set; }
        public string MODIFIEDTIME { get; set; }
        


    }
    public class AuditTrailListv2
    {
        public string CN { get; set; }
        public string ID { get; set; }
        public string EMPID { get; set; }
        public string MODIFIEDBYNAME { get; set; }
        public string MODIFIEDBYEMPID { get; set; }
        public string CHANGEFROM { get; set; }
        public string CHANGETO { get; set; }
        public string MODIFIEDDATE { get; set; }
        public string CATEGORY { get; set; }
        public string DATEFROM { get; set; }
        public string DATETO { get; set; }
        public string MODIFIEDTIME { get; set; }



    }
    #endregion
    #region 2018-09-26
    public class GetDependentResult
    {
        public List<DHRDEPv1> Dependent;
        public string myreturn;
    }
    #endregion
    #region 2018-09-27
    public class Token
    {
        public string CN { get; set; }
        public string EMPID { get; set; }
        public string TOKENID { get; set; }
    }
    public class GetEmpHistory
    {
        public List<DHREMPHv1> EmployeeHistory;
        public string myreturn;
    }
    #endregion
    #region 2010-10-05
    public class GetRequestForm
    {
        public List<DHRRequestv1> RequestFormList;
        public string myreturn;
    }


    public class DHRRequestv1
    {
        public string CN { get; set; }
        public string LOGINID { get; set; }

        public string FORMID { get; set; }
        public string FORMNAME { get; set; }
        public string CODENAME { get; set; }
        public string PDFFILE { get; set; }
        public string FORMTYPES { get; set; }
        public string DATECREATED { get; set; }

    }
    #endregion

    #region 2010-10-08
    public class GetPSv1
    {
        public List<DHRPSv1> ProfileSignatoryList;
        public string myreturn;
    }


    public class DHRPSv1
    {
        public string CN { get; set; }
        public string LOGINID { get; set; }

        public string FORMID { get; set; }
        public string FORMNAME { get; set; }
        public string CODENAME { get; set; }
        public string PDFFILE { get; set; }
        public string FORMTYPES { get; set; }
        public string DATECREATED { get; set; }
        public string ISCHECKED { get; set; }
        public string AUTOSIGNED { get; set; }
        public string SIGNWITHPERMIT { get; set; }
        public string PRINTTOSIGN { get; set; }

    }
    #endregion
    #region 2018-10-16
    public class GetGovIDLicensesNotif
    {
        public List<IDNotifDetails> GovIDLicensesNotifList;
        public string myreturn;
    }
    public class IDNotifDetails
    {
        public string type { get; set; }
        public string reminddate { get; set; }
        public string remindmessage { get; set; }
        public string renewdate { get; set; }
        public string renewmessage { get; set; }
        public string ExpireDate { get; set; }
        public string ID { get; set; }
        public string remindtype { get; set; }
        public string CN { get; set; }
        public string LoginID { get; set; }
    }


    #endregion

    #region 2018-10-17
    public class Reemburse
    {
        public string CN { get; set; }
        public string NTID { get; set; }
        public string B64Image { get; set; }

    }
    public class GetGreetingResult
    {
        public List<Greetings> Greeting;
        public string myreturn;
    }
    public class DHRDEPRelationv1
    {

        public string Relationship { get; set; }
        public string CN { get; set; }

    }
    public class GetRelationResult
    {
        public List<DHRDEPRelationv1> Relationship;
        public string myreturn;
    }
    #endregion
    //---------------------------------------------------------------Revised Change DB End------------------------------------------------------------------------

    public class EmpList
    {
        public string EmpID { get; set; }
        public string EmpName { get; set; }
    }

    public class EmployeeList
    {
        public List<EmpList> EmpList;
        public string _myreturn { get; set; }
    }

    public class DocumentId
    {
        public string docId { get; set; }
        public string imgType { get; set; }
        public string imgFilename { get; set; }
        public string id { get; set; }
        public string _return { get; set; }
    }

    public class EmpIdForIDs
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string _return { get; set; }
    }

    public class DocList
    {
        public List<DocumentId> DocumentId;
        public string _myreturn { get; set; }
    }
    public class DocumentId2
    {
        public string docId { get; set; }
        public string imgType { get; set; }
        public string imgFilename { get; set; }
        public string id { get; set; }
        public string source { get; set; }
        public string _return { get; set; }
    }
    public class EmpIdForIDs2
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string _return { get; set; }
    }

    public class DocList2
    {
        public List<DocumentId2> DocumentId2;
        public string _myreturn { get; set; }
    }

    public class baseID
    {
        public string CN { get; set; }
        public string imageID { get; set; }
        public string source { get; set; }
        public string _return { get; set; }
    }

    public class idImage
    {
        public string IDimg { get; set; }
        public string imgType { get; set; }
    }

    public class imageList
    {
        public List<idImage> idImage;
        public string _return { get; set; }
    }

    public class updateImgFilename
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string imgID { get; set; }
        public string filename { get; set; }
        public string _return { get; set; }
        public string base64 { get; set; }
    }
    public class updateImgFilenamev2
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string imgID { get; set; }
        public string filename { get; set; }
        public string _return { get; set; }
        public string base64 { get; set; }
        public string userid { get; set; }
    }
    public class insertBase64
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string filename { get; set; }
        public string base64 { get; set; }
        public string _return { get; set; }
    }
    public class insertImg
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string imgType { get; set; }
        public string filename { get; set; }
        public string _return { get; set; }
    }
    public class insertImgv2
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string imgType { get; set; }
        public string filename { get; set; }
        public string _return { get; set; }
        public string userid { get; set; }
    }
    //Brandon Start
    public class BrandonUserProfiletesting
    {
        public string FIRSTNAMEMO { get; set; }
        public string LASTNAMEMO { get; set; }
        public string MIDDLENAMEMO { get; set; }
    }
    //Brandon End

    public class GetNationality
    {
        public string CN { get; set; }
    }
    public class GetEmpAccess
    {
        public string _EMPID { get; set; }
        public string CN { get; set; }
    }

    public class DeleteImage
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string imageID { get; set; }
        public string filename { get; set; }
        public string _return { get; set; }
    }

    public class DelImage
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string filename { get; set; }
        public string base64 { get; set; }
        public string newfilename { get; set; }

    }

    public class getnewimg
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string Type { get; set; }
        public string _return { get; set; }
    }

    public class newImgList
    {
        public List<newImg> newImg;
        public string _myreturn { get; set; }
    }

    public class newImg
    {
        public string ID { get; set; }
        public string ImgType { get; set; }
        public string Filename { get; set; }
        public string _return { get; set; }
    }

    public class InsertNewImg
    {
        public string CN { get; set; }
        public string empID { get; set; }
        public string Filename { get; set; }
        public string ImgType { get; set; }
    }

    public class delNewImg
    {
        public string CN { get; set; }
        public string ID { get; set; }
    }

    public class delallnewimg
    {
        public string CN { get; set; }
        public string empID { get; set; }
    }
    public class updateidslicenses
    {
        public string CN { get; set; }
        public string EmpID { get; set; }
        public string base64 { get; set; }
        public string FileName { get; set; }
    }

    #region 2018-11-20
    public class GetRequestVarv1
    {
        public List<GetRequestv1> GetRequestInfo;
        public string myreturn;
    }

    public class GetRequestv1
    {

        public string ISMODALVIEW { get; set; }
        public string CN { get; set; }
        public string LOGINID { get; set; }
        public string APPROVERID { get; set; }
        public int FORMID { get; set; }

    }


    public class GetTodoRequestVarv1
    {
        public List<GetTodoRequestv1> GetTodoRequestInfo;
        public string myreturn;
    }

    public class GetTodoRequestv1
    {
        public string LOGINID { get; set; }
        public string CN { get; set; }
        public string ID { get; set; }
        public string DATEREQUESTED { get; set; }
        public string FORMCODE { get; set; }
        public string REQUESTEDNAMEBY { get; set; }
        public string APPROVED { get; set; }
        public string FORMID { get; set; }
        public string FORMTYPES { get; set; }
        public string DATEAPPROVED { get; set; }
        public string REQUESTEMPID { get; set; }

    }




    public class GetTodoRequestAuditVarv1
    {
        public List<GetTodoRequestAuditv1> GetTodoRequestAuditInfo;
        public string myreturn;
    }

    public class GetTodoRequestAuditv1
    {
        public string LOGINID { get; set; }
        public string CN { get; set; }
        public string DATEREQUESTED { get; set; }
        public string EMPNAME { get; set; }
        public string FORMID { get; set; }
        public string APPROVERID { get; set; }
        public string FORMCODE { get; set; }
        public string REQUESTEDNAMEBY { get; set; }
        public string APPROVED { get; set; }
        public string FORMTYPES { get; set; }
        public string DATEAPPROVED { get; set; }

    }


    public class GetTodoRequestApprovedDenied
    {
        public string LOGINID { get; set; }
        public string CN { get; set; }
        public int ID { get; set; }
        public int FORMID { get; set; }
        public string EMPREQ { get; set; }
        public string DATEREQ { get; set; }
        public string EMPID { get; set; }
        public string STATUS1 { get; set; }
        public string STATUS2 { get; set; }
        public string ISCHECKED { get; set; }

    }
    #endregion

    public class reimbursementStatusList
    {
        public List<reimbursementStatus> reimStatus;
        public string _myreturn { get; set; }
    }

    public class reimbursementStatus
    {
        public string ID { get; set; }
        public string EmpID { get; set; }
        public string EmpName { get; set; }
        public string BenefitType { get; set; }
        public string VendorName { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionTime { get; set; }
        public string VendorTIN { get; set; }
        public string Amount { get; set; }
        public string Status { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
    }

    public class reimbursementDetailsresult
    {
        public List<reimbursementDetails> reimDetails;
        public string _myreturn { get; set; }
    }

    public class reimbursementDetails
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Required { get; set; }
        public string Status { get; set; }
        public string ModifiedBy { get; set; }
        public string ModifiedDate { get; set; }
    }
    public class EmpLevelList
    {
        public string Level { get; set; }
    }

    public class EmpLevelListResult
    {
        public List<EmpLevelList> empLevelList;
        public string myreturn { get; set; }
    }

    public class AllEmployeeList
    {
        public string EmpID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Image { get; set; }
    }

    public class AllEmployeeListResult
    {
        public List<AllEmployeeList> empLevelList;
        public string myreturn { get; set; }
    }
    public class ExitProcessAuditTrailMod
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string TerminationType { get; set; }
        public string TerminationReason { get; set; }
        public string EffectiveDate { get; set; }
        public string ImmediateSupervisor { get; set; }
        public string ModifiedBy { get; set; }
        public string Department { get; set; }
        public string ResignationReason { get; set; }
        public string CN { get; set; }
    }

    public class ExitProcessAuditTrailResult
    {
        public List<ExitProcessAuditTrailMod> Dependent;
        public string myreturn;
    }
}